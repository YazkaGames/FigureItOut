package com.yazka.figureitout_alpha.Objects;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.preference.PreferenceManager;

/**
 * Represents a right-angled triangle wall with a hypotenuse at south west (SW), south east (SE),
 * north west (NW) or north east (NE)
 * Copyright 2017 Roger Kalliomäki
 *
 * @author Roger Kalliomäki
 */
public class WallTriangle {

//----------- Instance variables ---------------
    private Paint mPaint;
    private Path mPath;
    private Vector mCorner1,mCorner2,mCorner3,mHNormal,mTemp;
    private String mType;
    private float mNormalAngle,mXmax,mYmax;
//----------- Constructor --------------------

    /**
     * Constructs a WallTriangle specified by a right-angle corner (x,y) and the corners
     * ((x2,y),(x,y2) for SE), ((x,y2),(x2,y) for SW), ((x,y2),(x2,y) for NE), ((x2,y),(x,y2) for NW)
     *
     * @param x - x-position of right-angle corner
     * @param y - y-position of right-angle corner
     * @param x2 - x-position of corner not shared by right-angle corner
     * @param y2 - y-position of corner not shared by right-angle corner
     * @param context - Context with global information about the application environment
     */
    public WallTriangle(float x, float y, float x2, float y2, Context context) {

        mCorner1 = new Vector(x,y);
        mHNormal = new Vector(0,0);
        mTemp = new Vector(0,0);
        if(x>x2 && y<y2) {
            mType = "SW";
            mCorner2 = new Vector(x,y2);
            mCorner3 = new Vector(x2,y);
        }
        if(x<x2 && y<y2) {
            mType = "SE";
            mCorner2 = new Vector(x2,y);
            mCorner3 = new Vector(x,y2);
        }
        if(x<x2 && y>y2) {
            mType = "NE";
            mCorner2 = new Vector(x,y2);
            mCorner3 = new Vector(x2,y);
        }
        if(x>x2 && y>y2) {
            mType = "NW";
            mCorner2 = new Vector(x2,y);
            mCorner3 = new Vector(x,y2);
        }
        //set hypotenuse as a vector pointing from corner2 to corner3
        Vector hypotenuse = new Vector(0,0);
        hypotenuse.copy(mCorner3);
        hypotenuse.sub(mCorner2);
        //normal (ñ) of hypotenuse-vector (pointing inwards triangle)
        mHNormal.copy(hypotenuse);
        mHNormal.setToNormal();
        Vector xAxis = new Vector(1,0);
        mNormalAngle = mHNormal.getAngle(xAxis);
        //get screen size for device
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        mXmax = sharedPref.getFloat("xMax", 0f);
        mYmax = sharedPref.getFloat("yMax", 0f);
        //define one pixel unit used for drawing
        float pixelUnit = mXmax/1500;
        mPaint = new Paint();
        mPaint.setColor(Color.WHITE);
        mPaint.setStrokeWidth(5f*pixelUnit);
        mPath = new Path();
        mPath.moveTo(x,y);
        mPath.lineTo(mCorner2.getX(),mCorner2.getY());
        mPath.lineTo(mCorner3.getX(),mCorner3.getY());
        mPath.lineTo(x,y);

    }

//------------------ Methods ---------------------

    /**
     * Check for collisions with Ball object and redirect Ball if a collision occurs
     *
     * @param b - The ball to be checked (and possibly redirected)
     */
    public boolean checkForCollision(Ball b) {
        //if ball has moved inside wall
        if(isInside(b)) {

            float r = b.getRadii();
            float x = b.getX();
            float y = b.getY();
            float dx = b.getDx();
            float dy = b.getDy();
            float oldX = b.getPosition().getOldX();
            float oldY = b.getPosition().getOldY();
            float collisionPointX = (float)(x+r*Math.cos(mNormalAngle));

            //if a (stationary) ball was created inside the wall, move it to a new random position
            if (dx == 0f && dy == 0f) {
                float randomX = (float)Math.random()*mXmax;
                float randomY = (float)Math.random()*mYmax;
                b.setPosition(randomX,randomY);
                return true;
            }

            //switch cases for the four different wall types
            switch (mType) {
                case "SW":
                    //if ball crosses the right side of the wall
                    if (mCorner1.getY()<y && y<mCorner2.getY() && oldX - r >= mCorner1.getX()) {
                        //move ball back outside wall
                        moveBack(b);
                        //direct collision horizontally flip sign of x
                        b.getVelocity().flipSignX();
                        b.updateRects();
                        return true;
                    }
                    //if ball crosses the top of the wall
                    if (mCorner3.getX()<x && x<mCorner1.getX() && oldY + r <= mCorner1.getY()) {
                        //move ball back outside wall
                        moveBack(b);
                        //direct collision vertically flip sign of y
                        b.getVelocity().flipSignY();
                        b.updateRects();
                        return true;
                    }
                    //collision with corner1
                    if (b.getDistance(mCorner1.getX(),mCorner1.getY())<=r) {
                        cornerCollision(mCorner1,b);
                        return true;
                    }
                    //collision with corner2
                    if (b.getDistance(mCorner2.getX(),mCorner2.getY())<=r) {
                        cornerCollision(mCorner2,b);
                        return true;
                    }
                    //collision with corner3
                    if (b.getDistance(mCorner3.getX(),mCorner3.getY())<=r) {
                        cornerCollision(mCorner3,b);
                        return true;
                    }
                    //if ball is just next to a corner but outside wall
                    if (mCorner1.getX()<x || mCorner1.getY()>y || mCorner2.getX()<x || mCorner3.getY()>y)
                        return false;
                    //collision with hypotenuse
                    if(mCorner3.getX()<collisionPointX && collisionPointX<mCorner2.getX()) {
                        hypCollision(b);
                        return true;
                    }
                    break;

                case "SE":
                    //if ball crosses the top of the wall
                    if (mCorner1.getX()<x && x<mCorner2.getX() && oldY + r <= mCorner1.getY()) {
                        //move ball back outside wall
                        moveBack(b);
                        //direct collision vertically flip sign of y
                        b.getVelocity().flipSignY();
                        b.updateRects();
                        return true;
                    }
                    //if ball crosses the left side of the wall
                    if (mCorner1.getY()<y && y<mCorner3.getY() && oldX + r <= mCorner1.getX()) {
                        //move ball back outside wall
                        moveBack(b);
                        //direct collision horizontally flip sign of x
                        b.getVelocity().flipSignX();
                        b.updateRects();
                        return true;
                    }
                    //collision with corner1
                    if (b.getDistance(mCorner1.getX(),mCorner1.getY())<=r) {
                        cornerCollision(mCorner1,b);
                        return true;
                    }
                    //collision with corner2
                    if (b.getDistance(mCorner2.getX(),mCorner2.getY())<=r) {
                        cornerCollision(mCorner2,b);
                        return true;
                    }
                    //collision with corner3
                    if (b.getDistance(mCorner3.getX(),mCorner3.getY())<=r) {
                        cornerCollision(mCorner3,b);
                        return true;
                    }
                    //if ball is just next to a corner but outside wall
                    if (mCorner1.getX()>x || mCorner1.getY()>y || mCorner2.getY()>y || mCorner3.getX()>x)
                        return false;
                    //collision with hypotenuse
                    if(mCorner3.getX()<collisionPointX && collisionPointX<mCorner2.getX()) {
                        hypCollision(b);
                        return true;
                    }
                    break;

                case "NE":
                    //if ball crosses the left side of the wall
                    if (mCorner2.getY()<y && y<mCorner1.getY() && oldX + r <= mCorner1.getX()) {
                        //move ball back outside wall
                        moveBack(b);
                        //direct collision horizontally flip sign of x
                        b.getVelocity().flipSignX();
                        b.updateRects();
                        return true;
                    }
                    //if ball crosses the bottom of the wall
                    if (mCorner1.getX()<x && x<mCorner3.getX() && oldY - r >= mCorner1.getY()) {
                        //move ball back outside wall
                        moveBack(b);
                        //direct collision vertically flip sign of y
                        b.getVelocity().flipSignY();
                        b.updateRects();
                        return true;
                    }
                    //collision with corner1
                    if (b.getDistance(mCorner1.getX(),mCorner1.getY())<=r) {
                        cornerCollision(mCorner1,b);
                        return true;
                    }
                    //collision with corner2
                    if (b.getDistance(mCorner2.getX(),mCorner2.getY())<=r) {
                        cornerCollision(mCorner2,b);
                        return true;
                    }
                    //collision with corner3
                    if (b.getDistance(mCorner3.getX(),mCorner3.getY())<=r) {
                        cornerCollision(mCorner3,b);
                        return true;
                    }
                    //if ball is just next to a corner but outside wall
                    if (mCorner1.getX()>x || mCorner1.getY()<y || mCorner2.getX()>x || mCorner3.getY()<y)
                        return false;
                    //collision with hypotenuse
                    if(mCorner2.getX()<collisionPointX && collisionPointX<mCorner3.getX()) {
                        hypCollision(b);
                        return true;
                    }
                    break;

                case "NW":
                    //if ball crosses the right side of the wall
                    if (mCorner3.getY()<y && y<mCorner1.getY() && oldX - r >= mCorner1.getX()) {
                        //move ball back outside wall
                        moveBack(b);
                        //direct collision horizontally flip sign of x
                        b.getVelocity().flipSignX();
                        b.updateRects();
                        return true;
                    }
                    //if ball crosses the bottom of the wall
                    if (mCorner2.getX()<x && x<mCorner1.getX() && oldY - r >= mCorner1.getY()) {
                        //move ball back outside wall
                        moveBack(b);
                        //direct collision vertically flip sign of y
                        b.getVelocity().flipSignY();
                        b.updateRects();
                        return true;
                    }
                    //collision with corner1
                    if (b.getDistance(mCorner1.getX(),mCorner1.getY())<=r) {
                        cornerCollision(mCorner1,b);
                        return true;
                    }
                    //collision with corner2
                    if (b.getDistance(mCorner2.getX(),mCorner2.getY())<=r) {
                        cornerCollision(mCorner2,b);
                        return true;
                    }
                    //collision with corner3
                    if (b.getDistance(mCorner3.getX(),mCorner3.getY())<=r) {
                        cornerCollision(mCorner3,b);
                        return true;
                    }
                    //if ball is just next to a corner but outside wall
                    if (mCorner1.getX()<x || mCorner1.getY()<y || mCorner2.getY()<y || mCorner3.getX()<x)
                        return false;
                    //collision with hypotenuse
                    if(mCorner2.getX()<collisionPointX && collisionPointX<mCorner3.getX()) {
                        hypCollision(b);
                        return true;
                    }
                    break;
            }
        }
        return false;
    }

    /**
     * Reflection against (imaginary) line between ball and corner
     *
     * @param corner - collision corner
     * @param b - The ball to collide against the corner
     */
    private void cornerCollision(Vector corner, Ball b) {
        //move ball back outside wall
        moveBack(b);
        //set n to normal vector of the boundary line between ball and corner (pointing towards ball)
        Vector n = b.getTemp();
        n.set(b.getX()-corner.getX(),b.getY()-corner.getY());
        n.normalize();
        n.scale(b.getVelocity().length());
        //change direction of velocity to the normal
        b.setVelocity(n);
        b.updateRects();
    }

    /**
     * Draws the region to the specified canvas
     *
     * @param canvas - The canvas to draw on
     */
    public void drawWall(Canvas canvas){ canvas.drawPath(mPath,mPaint); }

    /**
     * Returns the value (y) of the hypotenuse between corner2 and corner3, at (argument) x
     *
     * @param x - x-position
     * @return - value (y) of the hypotenuse at (argument) x
     */
    private float getLineValue(float x) {
        //y1=m*(x1-x2)+y2
        float m = (mCorner2.getY()-mCorner3.getY()) / (mCorner2.getX()-mCorner3.getX());
        return m*(x-mCorner3.getX()) + mCorner3.getY();
    }

    /**
     * Reflection against hypotenuse of the wall
     *
     * @param b - The ball to collide against the hypotenuse
     */
    private void hypCollision(Ball b) {
        //move ball back outside wall
        moveBack(b);
        //move back little extra in case ball gets stuck in wall (it will work itself out of the wall)
        mTemp.copy(mHNormal);
        mTemp.scale(0.1f);
        b.getPosition().sub(mTemp);
        //reflection v_new = v_old - (2*(v_old·ñ))*ñ
        float scale = 2f * b.getVelocity().dot(mHNormal);
        mTemp.copy(mHNormal);
        mTemp.scale(scale);
        b.getVelocity().sub(mTemp);
        b.updateRects();
    }

    /**
     * Checks if argument ball is inside a square defined by the edges
     *
     * @param b - The ball to be checked
     * @return - true if ball is inside wall, else false
     */
    private boolean isInside(Ball b) {
        float x = b.getX();
        float y = b.getY();
        float r = b.getRadii();
        float collisionPointX,collisionPointY;

        switch (mType) {

            case "SW":
                //the point on the ball which will hit the wall (depends on wall angle)
                collisionPointX = (float)(x+r*Math.cos(mNormalAngle));
                collisionPointY = (float)(y-r*Math.sin(mNormalAngle));
                //if ball has moved inside the wall
                if(x-r<mCorner1.getX() && y+r>mCorner1.getY() && x+r>mCorner3.getX() && y-r<mCorner2.getY())
                    return (collisionPointY<getLineValue(collisionPointX));
                break;

            case "SE":
                //the point on the ball which will hit the wall (depends on wall angle)
                collisionPointX = (float)(x+r*Math.cos(mNormalAngle));
                collisionPointY = (float)(y-r*Math.sin(mNormalAngle));
                //if ball has moved inside the wall
                if(x+r>mCorner1.getX() && y+r>mCorner1.getY() && x-r<mCorner2.getX() && y-r<mCorner3.getY())
                    return (collisionPointY<getLineValue(collisionPointX));
                break;

            case "NE":
                //the point on the ball which will hit the wall (depends on wall angle)
                collisionPointX = (float)(x+r*Math.cos(mNormalAngle));
                collisionPointY = (float)(y+r*Math.sin(mNormalAngle));
                //if ball has moved inside the wall
                if(x+r>mCorner1.getX() && y+r>mCorner2.getY() && x-r<mCorner3.getX() && y-r<mCorner3.getY())
                    return (collisionPointY>getLineValue(collisionPointX));
                break;

            case "NW":
                //the point on the ball which will hit the wall (depends on wall angle)
                collisionPointX = (float)(x+r*Math.cos(mNormalAngle));
                collisionPointY = (float)(y+r*Math.sin(mNormalAngle));
                //if ball has moved inside the wall
                if(x+r>mCorner2.getX() && y+r>mCorner3.getY() && x-r<mCorner1.getX() && y-r<mCorner1.getY())
                    return (collisionPointY>getLineValue(collisionPointX));
                break;

        }
        return false;
    }

    /**
     * Move ball back one time step and set old position to two time steps back
     *
     * @param b - The ball to move back
     */
    private void moveBack(Ball b) {
        //move back ball one time step (to get ball outside wall again)
        b.getPosition().sub(b.getVelocity());
        //set old position "two steps" back (one more step back)
        Vector backStep = b.getTemp();
        backStep.copy(b.getPosition());
        backStep.sub(b.getVelocity());
        b.getPosition().setOld(backStep.getX(), backStep.getY());
    }
}

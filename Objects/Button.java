package com.yazka.figureitout_alpha.Objects;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

/**
 * Represents a button for a level in the game.
 * Copyright 2017 Roger Kalliomäki
 *
 * @author Roger Kalliomäki
 */
public class Button {

//----------- Instance variables --------------
    private Paint mArcPaint,mBorderPaint,mBitmapPaint;
    private RectF mRectF;
    private Rect mRect;
    private int mArcLength;
    private float mXPos,mYPos,mRadii;
    private Bitmap mBitmap = null;
//----------- Constructors --------------------

    /**
     * Constructs a Button object with specified radii, position and color
     *
     * @param x - x-position of button
     * @param y - y-position of button
     * @param r - Radii of button
     * @param arcColor - Inside color of button
     * @param borderColor - Border color of button
     */
    public Button(float x, float y, float r, String arcColor, String borderColor) {
        float strokeWidth = r/20f;
        mRectF = new RectF(x-r,y-r,x+r,y+r);
        mXPos = x;
        mYPos = y;
        mRadii = r;
        mArcLength = 0;
        mArcPaint = new Paint();
        mBorderPaint = new Paint();
        mArcPaint.setColor(Color.parseColor(arcColor));
        mBorderPaint.setColor(Color.parseColor(borderColor));
        mArcPaint.setStyle(Paint.Style.FILL);
        mBorderPaint.setStyle(Paint.Style.STROKE);
        mBorderPaint.setStrokeWidth(strokeWidth);
    }

    /**
     * Constructs a Button object with specified radii, position and color
     * with a bitmap in the middle
     *
     * @param x - x-position of button
     * @param y - y-position of button
     * @param r - Radii of button
     * @param arcColor - Inside color of button
     * @param borderColor - Border color of button
     * @param context - Context with global information about the application environment
     */
    public Button(float x, float y, float r, String arcColor, String borderColor, int resourceId, Context context) {
        mBitmap = BitmapFactory.decodeResource(context.getResources(), resourceId);
        float strokeWidth = r/20f;
        mRectF = new RectF(x-r,y-r,x+r,y+r);
        mRect = new Rect((int)(x-0.8f*r),(int)(y-0.8f*r),(int)(x+0.8f*r),(int)(y+0.8f*r));
        mXPos = x;
        mYPos = y;
        mRadii = r;
        mArcLength = 0;
        mArcPaint = new Paint();
        mBorderPaint = new Paint();
        mBitmapPaint = new Paint();
        mArcPaint.setColor(Color.parseColor(arcColor));
        mBorderPaint.setColor(Color.parseColor(borderColor));
        mArcPaint.setStyle(Paint.Style.FILL);
        mBorderPaint.setStyle(Paint.Style.STROKE);
        mBorderPaint.setStrokeWidth(strokeWidth);
    }

//-------------------- Methods ------------------------

    /**
     * Changes the border of the button
     */
    public void buttonPressed() {
        mBorderPaint.setAlpha(100);
    }

    /**
     * Changes the border of the button (to default value)
     */
    public void buttonReleased() {
        mBorderPaint.setAlpha(255);
    }

    /**
     * Draws button to argument canvas
     *
     * @param canvas - The canvas to draw on
     */
    public void drawButton(Canvas canvas) {

        canvas.drawArc(mRectF, 270, mArcLength, true, mArcPaint);
        canvas.drawOval(mRectF, mBorderPaint);
        if(mBitmap != null)
            canvas.drawBitmap(mBitmap,null, mRect, mBitmapPaint);
    }

    /**
     * Returns x-position
     *
     * @return - x-position of button
     */
    public float getX() {return mXPos;}

    /**
     * Returns y-position
     *
     * @return - y-position of button
     */
    public float getY() {return mYPos;}

    /**
     * Check if a position is inside the button
     *
     * @param x - x position to be checked
     * @param y - y position to be checked
     * @return - true if inside, false else
     */
    public boolean isInside(float x, float y) {
        float distance = (float)Math.sqrt((mXPos-x)*(mXPos-x)+(mYPos-y)*(mYPos-y));
        return mRadii > distance;
    }

    public void setAlpha(int alpha) {
        if(mBitmap!=null)
            mArcPaint.setAlpha(alpha);
    }

    /**
     * Sets the amount of the button to be filled
     *
     * @param fillFactor - Factor of arc to be filled
     */
    public void setFillLevel(float fillFactor) {
        mArcLength = (int) (360f * fillFactor);
    }

}

package com.yazka.figureitout_alpha.Objects;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.ColorInt;

/**
 * Represents a dashed line from (mStartX,mStartY) to (mStopX,mStopY)
 * Copyright 2017 Roger Kalliomäki
 *
 * @author Roger Kalliomäki
 */
public class LinePath {

//----------- Instance variables ---------------
    private float mStartX,mStartY,mStopX,mStopY;
    private Path mPath;
    private Paint mPaint;
    private boolean mIsSet;
//----------- Constructors --------------------

    /**
     * Constructs a dashed line from (mStartX,mStartY) to (mStopX,mStopY)
     *
     * @param xStart - The x-position of the beginning of the line
     * @param yStart - The y-position of the beginning of the line
     * @param xStop - The x-position of the end of the line
     * @param yStop- The y-position of the end of the line
     */
    public LinePath(float xStart, float yStart, float xStop, float yStop, float screenWidth) {

        float pixelUnit = screenWidth/1500;
        mIsSet = true;
        mStartX = xStart;
        mStartY = yStart;
        mStopX = xStop;
        mStopY = yStop;
        mPath = new Path();
        mPath.moveTo(xStart,yStart);
        mPath.lineTo(xStop,yStop);
        mPaint = new Paint();
        mPaint.setColor(Color.WHITE);
        mPaint.setStrokeWidth(5f*pixelUnit);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setPathEffect(new DashPathEffect(new float[] {20f*pixelUnit,20f*pixelUnit}, 0));

    }

//-------------------- Methods ---------------------

    /**
     * Draws the region to the specified canvas
     *
     * @param canvas - The canvas to draw on
     */
    public void drawLine(Canvas canvas){ canvas.drawPath(mPath,mPaint); }

    /**
     * Checks if line is set
     *
     * @return - true if line is set, false else
     */
    public boolean isLineSet() {return mIsSet;}

    /**
     * Sets new position
     * @param xStart - start x
     * @param xStop - stop x
     * @param yStart - start y
     * @param yStop - stop y
     */
    public void moveTo(float xStart, float xStop, float yStart, float yStop) {
        mPath.reset();
        mPath.moveTo(xStart,yStart);
        mPath.lineTo(xStop,yStop);
    }

    /**
     * Sets the alpha of the line
     *
     * @param alpha - The alpha
     */
    public void setAlpha(int alpha) {
        mPaint.setAlpha(alpha);
    }

    /**
     * Sets the color of the line
     *
     * @param c - The color
     */
    public void setLineColor(@ColorInt int c) { mPaint.setColor(c); }

    /**
     * Sets the start position of the line (x,y)
     *
     * @param x - The x-position
     * @param y - The y-position
     */
    public void setStart(float x, float y) {

        mPath.reset();
        mPath.moveTo(x,y);
        mStartX = x;
        mStartY = y;
        mPath.lineTo(mStopX,mStopY);
        //check if start and stop is same position (floats) to see if path is set
        float epsilon = 0.001f;
        mIsSet = !(Math.abs(mStartX-mStopX) < epsilon && Math.abs(mStartY-mStopY) < epsilon);
    }

    /**
     * Sets the stop position of the line (x,y)
     *
     * @param x - The x-position
     * @param y - The y-position
     */
    public void setStop(float x, float y) {

        mPath.reset();
        mPath.moveTo(mStartX,mStartY);
        mPath.lineTo(x,y);
        mStopX = x;
        mStopY = y;
        float epsilon = 0.001f;
        mIsSet = !(Math.abs(mStartX-mStopX) < epsilon && Math.abs(mStartY-mStopY) < epsilon);

    }
}

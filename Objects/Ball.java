package com.yazka.figureitout_alpha.Objects;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.ColorInt;

/**
 * Represents a ball with radii, velocity, position and color.
 * Copyright 2017 Roger Kalliomäki
 *
 * @author Roger Kalliomäki
 */
public class Ball {

//----------- Instance variables --------------
    private float mRadii;
    private Vector mVelocity,mPosition,mTemp;
    private @ColorInt int mColor;
    private RectF mEdgeRect = new RectF(0.0f,0.0f,0.0f,0.0f);
    private Rect mInnerRect = new Rect(0,0,0,0);
    private Bitmap mBitmap = null;
    private Paint mPaint = null;
    private Paint mBorderPaint = null;
    private Paint mPaint2;
//----------- Constructors --------------------

    /**
     * Constructs a Ball object with specified radii, velocity, position and color.
     *
     * @param r - The radii
     * @param v - The velocity
     * @param p - The position
     * @param c - The color
     */
    public Ball(float r, Vector v, Vector p, @ColorInt int c) {
        mRadii = r;
        mVelocity = v;
        mPosition = p;
        mTemp = new Vector(0f,0f);
        mPaint = new Paint();
        mPaint.setColor(c);
        mColor = c;
        updateRects();
    }

    /**
     * Constructs a (stationary) Ball object with specified radii, position and color.
     *
     * @param r - The radii
     * @param p - The position
     * @param c - The color
     */
    public Ball(float r, Vector p, @ColorInt int c) {
        mRadii = r;
        mVelocity = new Vector(0f,0f);
        mPosition = p;
        mTemp = new Vector(0f,0f);
        mPaint = new Paint();
        mPaint.setColor(c);
        mColor = c;
        updateRects();
    }

    /**
     * Constructs a (stationary) Ball object with specified radii, position and color.
     *
     * @param r - The radii
     * @param x - x position
     * @param y - y position
     * @param c - The color
     */
    public Ball(float r, float x, float y, @ColorInt int c) {
        mRadii = r;
        mVelocity = new Vector(0f,0f);
        mPosition = new Vector(x,y);
        mTemp = new Vector(0f,0f);
        mPaint = new Paint();
        mPaint.setColor(c);
        mColor = c;
        updateRects();
    }

    /**
     * Constructs a Ball object with specified radii, position, vertical velocity and color which
     * has the appearance of a bubble.
     *
     * @param r - The radii
     * @param x - x position
     * @param y - y position
     * @param dy - change i y-position
     * @param c - The color
     */
    public Ball(float r, float x, float y, float dy, @ColorInt int c) {
        float strokeWidth = r/25f;
        mRadii = r;
        mVelocity = new Vector(0f,dy);
        mPosition = new Vector(x,y);
        mTemp = new Vector(0f,0f);
        mPaint = new Paint();
        mBorderPaint = new Paint();
        mPaint.setColor(c);
        mBorderPaint.setColor(c);
        mPaint.setAlpha(70);
        mBorderPaint.setAlpha(200);
        mBorderPaint.setStyle(Paint.Style.STROKE);
        mBorderPaint.setStrokeWidth(strokeWidth);
        updateRects();
    }

    /**
     * Constructs a Ball object with specified radii and color which has the appearance of a bubble.
     *
     * @param r - The radii
     * @param c - The color
     */
    public Ball(float r, float strokeWidth, @ColorInt int c) {
        mRadii = r;
        mVelocity = new Vector(0f,0f);
        mPosition = new Vector(-100f,-100f);
        mTemp = new Vector(0f,0f);
        mPaint = new Paint();
        mBorderPaint = new Paint();
        mPaint.setColor(c);
        mBorderPaint.setColor(c);
        mPaint.setAlpha(70);
        mBorderPaint.setAlpha(200);
        mBorderPaint.setStyle(Paint.Style.STROKE);
        mBorderPaint.setStrokeWidth(strokeWidth);
        updateRects();
    }

    /**
     * Constructs a (stationary) Ball object at the origin with specified radii.
     *
     * @param r - The radii
     */
    public Ball(float r) {
        mRadii = r;
        mVelocity = new Vector(0f,0f);
        mPosition = new Vector(0f,0f);
        mTemp = new Vector(0f,0f);
        mPaint = new Paint();
        updateRects();
    }

    /**
     * Constructs a (stationary) unfilled gray circle with specified radii and position.
     *
     * @param r - The radii
     * @param p - The position
     */
    public Ball(float r, float strokeWidth, Vector p) {
        mRadii = r;
        mVelocity = new Vector(0f,0f);
        mPosition = p;
        mTemp = new Vector(0f,0f);
        mPaint = new Paint();
        mColor = Color.rgb(50,50,50);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(strokeWidth);
        mPaint.setColor(mColor);
        updateRects();
    }

    /**
     * Constructs a ball object with a bitmap in the middle
     *
     * @param r - Radii of button
     * @param x - x position
     * @param y - y position
     * @param resourceId - Id of drawable (bitmap)
     * @param context - Context with global information about the application environment
     */
    public Ball(float r, float x, float y, int resourceId, Context context) {
        mBitmap = BitmapFactory.decodeResource(context.getResources(), resourceId);
        mRadii = r;
        mPosition = new Vector(x,y);
        mVelocity = new Vector(0f,0f);
        mTemp = new Vector(0f,0f);
        mPaint = new Paint();
        mPaint2 = new Paint();
        updateRects();
    }

    /**
     * Constructs a ball object used as a button with a bitmap in the middle
     *
     * @param r - Radii of button
     * @param x - x position
     * @param y - y position
     * @param edgeWidth - The width of the button edge
     * @param resourceId - Id of drawable (bitmap)
     * @param context - Context with global information about the application environment
     */
    public Ball(float r, float x, float y, float edgeWidth, int resourceId, Context context) {
        mBitmap = BitmapFactory.decodeResource(context.getResources(), resourceId);
        mRadii = r;
        mPosition = new Vector(x,y);
        mVelocity = new Vector(0f,0f);
        mTemp = new Vector(0f,0f);
        mPaint = new Paint();
        mPaint2 = new Paint();
        mPaint.setStrokeWidth(edgeWidth);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(Color.WHITE);
        mPaint.setAlpha(150);
        mBitmap.setHasAlpha(true);
        mPaint2.setAlpha(150);
        updateRects();
    }

//---------------------- Methods ------------------------

    /**
     * Accelerates the ball towards the position defined by x and y within a speed limit
     *
     * @param x - x position
     * @param y - y position
     * @param speedLimit - highest allowed speed (pixels/frame)
     * @param attractScale - used to scale up or down the attractive force
     */
    public void attract(float x, float y, float speedLimit, float attractScale) {

        //set old velocity and position
        mVelocity.setOld(mVelocity.getX(),mVelocity.getY());
        mPosition.setOld(mPosition.getX(),mPosition.getY());
        //set touch position as vector (temp vector in ball)
        mTemp.setX(x);
        mTemp.setY(y);
        //set scale depending on distance between ball and touch position
        float distance = mTemp.distance(mPosition);
        float scale = attractScale/distance;
        //adjust velocity according to speed limit
        if (mVelocity.length() > speedLimit)
            mVelocity.setLength(speedLimit);
        //add velocity
        mTemp.sub(mPosition);
        mTemp.scale(scale);
        mVelocity.add(mTemp);
        //add velocity to position
        mPosition.add(mVelocity);
        //update ball rects
        updateRects();

    }

    /**
     * Change the bitmap
     *
     * @param resourceId - Id of drawable (bitmap)
     * @param context - Context with global information about the application environment
     */
    public void changeBitmap(int resourceId, Context context) {
        mBitmap = BitmapFactory.decodeResource(context.getResources(), resourceId);
    }

    /**
     * Check for collisions with boarders and redirect Ball if a collision occurs
     *
     * @param xMax - Right boundary of the View
     * @param yMax - Lower boundary of the View
     * @param xMin - Left boundary of the View
     * @param yMin - Upper boundary of the View
     *
     * @return - true if collision, else false
     */
    public boolean checkForBorderCollision(float xMax, float yMax, float xMin, float yMin) {

        boolean collision = false;

        //check left and right boundary
        if (mPosition.getX() + mRadii> xMax) {
            mVelocity.flipSignX();
            mPosition.setX(xMax - mRadii);
            collision = true;
        }
        else if (mPosition.getX() - mRadii < xMin) {
            mVelocity.flipSignX();
            mPosition.setX(xMin + mRadii);
            collision = true;
        }

        //check top and bottom boundary
        if (mPosition.getY() + mRadii > yMax) {
            mVelocity.flipSignY();
            mPosition.setY(yMax - mRadii);
            collision = true;
        }
        else if (mPosition.getY() - mRadii < yMin) {
            mVelocity.flipSignY();
            mPosition.setY(yMin + mRadii);
            collision = true;
        }
        updateRects();
        return collision;
    }

    /**
     * Check if ball is merged inside a circle
     *
     * @param b - Ball to check
     * @return - true if merged, false else
     */
    public boolean checkIfInside(Ball b) {
        float distance = this.getPosition().distance(b.getPosition());
        return ( distance + b.getRadii() < this.getRadii() );
    }

    /**
     * Check if a position is inside the ball
     *
     * @param x - x position to be checked
     * @param y - y position to be checked
     * @return - true if inside, false else
     */
    public boolean checkIfInside(float x, float y) {
        float distance = this.getPosition().distance(x,y);
        return ( distance < this.getRadii() );
    }

    /**
     * Changing the velocity of two colliding balls
     *
     * @param b - The second ball in the collision
     */
    public void collideElastic(Ball b) {

        //move balls back (so they don't "touch")
        float merged = (mRadii + b.mRadii) - mPosition.distance(b.mPosition);
        mTemp.copy(mPosition);
        mTemp.setDirection(b.mPosition,mPosition);
        mTemp.normalize();
        mTemp.scale(merged);
        b.mPosition.sub(mTemp);
        mPosition.add(mTemp);
        //collision is a reflection against imaginary line between balls
        //(normal is vector between ball positions) => v_new = v_old - (2*(v_old·ñ))*ñ
        //ball2 collision
        mTemp.normalize();
        float scale2 = 2*b.getVelocity().dot(mTemp);
        b.mTemp.copy(mTemp);
        b.mTemp.scale(scale2);
        b.getVelocity().sub(b.mTemp);
        b.mTemp.copy(mTemp);
        //ball1 collision
        mTemp.flipSignX();
        mTemp.flipSignY();
        float scale1 = 2*getVelocity().dot(mTemp);
        mTemp.scale(scale1);
        getVelocity().sub(mTemp);
        //update ball rects
        updateRects();
        b.updateRects();
    }

    /**
     * Changing the velocity of two colliding balls
     *
     * @param b - The second ball in the collision
     */
    public void collide(Ball b) {

        //move balls back (so they don't "touch")
        float merged = (mRadii + b.mRadii) - mPosition.distance(b.mPosition);
        mTemp.copy(mPosition);
        mTemp.setDirection(b.mPosition,mPosition);
        mTemp.normalize();
        mTemp.scale(merged);
        b.mPosition.sub(mTemp);
        mPosition.add(mTemp);
        //collision is a reflection against imaginary line between balls
        //(normal is vector between ball positions) => v_new = v_old - (2*(v_old·ñ))*ñ
        //ball2 collision
        mTemp.normalize();
        float scale2 = b.getVelocity().dot(mTemp);
        b.mTemp.copy(mTemp);
        b.mTemp.scale(scale2);
        b.getVelocity().sub(b.mTemp);
        b.mTemp.copy(mTemp);
        //ball1 collision
        mTemp.flipSignX();
        mTemp.flipSignY();
        float scale1 = getVelocity().dot(mTemp);
        mTemp.scale(scale1);
        getVelocity().sub(mTemp);
        //if ball is moving towards other ball it looses energy to the other ball, else it gains
        //energy from the other ball ((2*(v_old·ñ)/2 that's why scale1/scale2 only is half)
        b.getVelocity().add(mTemp);
        b.mTemp.scale(scale2);
        getVelocity().add(b.mTemp);
        //update ball rects
        updateRects();
        b.updateRects();
    }

    /**
     * Draws the Ball to the specified canvas
     *
     * @param canvas - The canvas to draw on
     */
    public void drawBall(Canvas canvas){
        //draw ball
        canvas.drawOval(getBallBound(), mPaint);
        //if ball has bitmap draw it
        if(mBitmap != null)
            canvas.drawBitmap(mBitmap,null, mInnerRect, mPaint2);
        //if ball has border paint draw it
        if(mBorderPaint != null)
            canvas.drawOval(getBallBound(),mBorderPaint);
    }

    /**
     * Accelerates the ball towards a position straight down under the ball
     *
     * @param speedLimit - highest allowed speed (pixels/frame)
     * @param attractScale - used to scale up or down the attractive force
     */
    public void drop(float speedLimit, float attractScale) {
        float dropAgainstX = mPosition.getX();
        float dropAgainstY = 2*mPosition.getY();
        attract(dropAgainstX,dropAgainstY,speedLimit,attractScale);
    }

    /**
     * Returns the boundary of the ball
     *
     * @return The boundary of the ball
     */
    private RectF getBallBound() {
        return mEdgeRect;
    }

    /**
     * Returns the color of the ball
     *
     * @return The color of the ball
     */
    public @ColorInt int getColor() {

        return mColor;
    }

    /**
     * Returns the distance to a given ball
     *
     * @param b - the given ball
     * @return The distance to the the ball
     */
    public float getDistance(Ball b) {

        return mPosition.distance(b.mPosition.getX(),b.mPosition.getY());
    }

    /**
     * Returns the distance to the ball from a given position
     *
     * @param x - x position
     * @param y - y position
     * @return The distance to the the ball
     */
    public float getDistance(float x, float y) {

        return mPosition.distance(x,y);
    }

    /**
     * Returns the x-velocity of the ball
     *
     * @return The x-veloctiy of the ball
     */
    public float getDx() {

        return mVelocity.getX();
    }

    /**
     * Returns the y-velocity of the ball
     *
     * @return The y-velocity of the ball
     */
    public float getDy() {

        return mVelocity.getY();
    }

    /**
     * Returns inner Rect
     *
     * @return - Inner Rect
     */
    public Rect getInnerRect() {return mInnerRect;}

    /**
     * Returns the position of the ball
     *
     * @return The position of the ball
     */
    public Vector getPosition() {

        return mPosition;
    }

    /**
     * Returns the radii of the ball
     *
     * @return The radii of the ball
     */
    public float getRadii() {

        return mRadii;
    }

    /**
     * Creates a Ball with a random (from 8 colors) color and random position (within 0<xmax
     * 0<ymax)
     *
     * @return A new (stationary) Ball object with random color and position
     */
    public static Ball getRandomBall(float radii, float xmin, float ymin, float xmax, float ymax) {
        //eight colors to draw randomly from
        @ColorInt int[] colors = new int[8];
        colors[0] = Color.parseColor("#3949AB");
        colors[1] = Color.parseColor("#1E88E5");
        colors[2] = Color.parseColor("#00897B");
        colors[3] = Color.parseColor("#7CB342");
        colors[4] = Color.parseColor("#FDD835");
        colors[5] = Color.parseColor("#FB8C00");
        colors[6] = Color.parseColor("#D81B60");
        colors[7] = Color.parseColor("#8E24AA");
        //random position
        float randomX = (float)Math.random()*(xmax-xmin)+xmin;
        float randomY = (float)Math.random()*(ymax-ymin)+ymin;
        //random color
        int randomColor = (int)(Math.random()*8);
        Vector p = new Vector(randomX,randomY);
        return new Ball(radii,p,colors[randomColor]);
    }

    /**
     * Returns the temp Vector
     *
     * @return - The temp Vector
     */
    public Vector getTemp() {
        return mTemp;
    }

    /**
     * Returns the velocity of the ball
     *
     * @return The velocity of the ball
     */
    public Vector getVelocity() {
        return mVelocity;
    }

    /**
     * Returns the x-position of the ball
     *
     * @return The x-position of the ball
     */
    public float getX() {

        return mPosition.getX();
    }

    /**
     * Returns the y-position of the ball
     *
     * @return The y-position of the ball
     */
    public float getY() {

        return mPosition.getY();
    }

    /**
     * Checking if two balls are colliding
     *
     * @param b - The second ball in the collision
     * @return True if balls are colliding, else false
     */
    public boolean isCollision(Ball b) {

        return mPosition.distance(b.mPosition) < mRadii+b.mRadii;
    }

    /**
     * Checking if ball is inside screen borders
     *
     * @return True if ball is inside screen, else false
     */
    public boolean isInScreen(float xmin, float ymin, float xmax, float ymax) {

        float x = mPosition.getX();
        float y = mPosition.getY();
        float r = mRadii;
        return x-r>xmin && x+r<xmax && y-r>ymin && y+r<ymax;
    }

    /**
     * Moves the ball according to the balls velocity
     */
    public void move() {
        mPosition.setOld(mPosition.getX(),mPosition.getY());
        mPosition.add(mVelocity);
        updateRects();
    }

    /**
     * Moves the ball according to the balls velocity, keeps speed within a speed limit
     * by braking the ball with a brake factor
     *
     */
    public void move(float speedLimit) {

        mPosition.setOld(mPosition.getX(),mPosition.getY());
        if (mVelocity.length() > speedLimit)
            mVelocity.setLength(speedLimit);

        mPosition.add(mVelocity);
        updateRects();
    }

    /**
     * Moves the ball according to the argument velocity
     */
    public void move(Vector velocity) {
        mPosition.setOld(mPosition.getX(),mPosition.getY());
        mPosition.add(velocity);
        updateRects();
    }

    /**
     * Moves the ball according to the balls velocity, the ball looses energy (speed)
     * accordingly to the friction scale (between 0 and 1)
     *
     * @param friction - value between 0 and 1 used as a breaking scale
     */
    public void moveWithFriction(float friction) {

        mPosition.setOld(mPosition.getX(),mPosition.getY());
        if (mVelocity.length() >= 0f)
            mVelocity.scale(friction);
        else
            mVelocity.setLength(0f);
        mPosition.add(mVelocity);
        updateRects();
    }

    /**
     * Repels the ball away from the position defined by x and y within a speed limit
     *
     * @param x - x position
     * @param y - y position
     * @param speedLimit - highest allowed speed
     */
    public void repel(float x, float y, float speedLimit) {

        //save old velocity and position
        mVelocity.setOld(mVelocity.getX(),mVelocity.getY());
        mPosition.setOld(mPosition.getX(),mPosition.getY());
        //set touch position as a vector (ball temp vector)
        mTemp.setX(x);
        mTemp.setY(y);
        //distance between ball and touch position
        float distance = mTemp.distance(mPosition);
        //adjust to speed limit
        if (mVelocity.length() > speedLimit)
            mVelocity.setLength(speedLimit);
        //accelerate ball (more acceleration near the touch position)
        mTemp.sub(mPosition);
        mTemp.scale(1/distance);
        mVelocity.sub(mTemp);
        //add velocity to position
        mPosition.add(mVelocity);
        //update ball rects
        updateRects();
    }

    /**
     * Sets alpha of the ball color
     *
     * @param alpha - The alpha of the ball color
     */
    public void setAlpha(int alpha) {

        mPaint.setAlpha(alpha);
    }

    /**
     * Sets the color of the ball
     *
     * @param c - The color of the ball
     */
    public void setColor(@ColorInt int c) {
        mPaint.setColor(c);
        mColor = c;
    }

    /**
     * Set paint style to fill
     */
    public void setFill() {
        mPaint.setStyle(Paint.Style.FILL);
    }

    /**
     * Sets the position of the ball
     *
     * @param x - The x coordinate
     * @param y - The y coordinate
     */
    public void setPosition(float x, float y) {
        mPosition.setX(x);
        mPosition.setY(y);
        updateRects();
    }

    /**
     * Sets the position of the ball
     *
     * @param v - Vector (x,y) with new position
     */
    public void setPosition(Vector v) {
        mPosition.setX(v.getX());
        mPosition.setY(v.getY());
        updateRects();
    }

    /**
     * Sets the radii of the ball
     *
     * @param r - The radii of the ball
     */
    public void setRadii(float r) {
        mRadii = r;
        updateRects();
    }

    /**
     * Set paint style to stroke
     */
    public void setStroke() {
        mPaint.setStyle(Paint.Style.STROKE);
    }

    /**
     * Sets the velocity of the ball
     *
     * @param x - The x c
     * @param y - The y coordinate
     */
    public void setVelocity(float x, float y) {
        mVelocity.setX(x);
        mVelocity.setY(y);
    }

    /**
     * Sets the velocity of the ball
     *
     * @param v - The vector to set as velocity
     */
    public void setVelocity(Vector v) {
        mVelocity.setX(v.getX());
        mVelocity.setY(v.getY());
    }

    /**
     * Accelerates the ball according to the device tilt defined by x and y and scaled
     * according to the screen size
     *
     * @param x - x tilt (in radians)
     * @param y - y tilt (in radians)
     * @param tiltScale - Factor to affect the tilt
     */
    public void tilt(float x, float y, float tiltScale) {

        //save old velocity
        mVelocity.setOld(mVelocity.getX(),mVelocity.getY());
        //set maximum tilt to 90 degrees
        float maxTilt = (float)Math.PI/2;
        //adjust if tilt > maximum tilt
        if(x>maxTilt)
            x = maxTilt;
        if(x<-maxTilt)
            x = -maxTilt;
        if(y>maxTilt)
            y = maxTilt;
        if(y<-maxTilt)
            y = -maxTilt;
        //add velocity in tilt direction
        mVelocity.setX(mVelocity.getX()+x*tiltScale);
        mVelocity.setY(mVelocity.getY()+y*tiltScale);
    }

    /**
     * Updates recs according to x- and y- positions
     */
    public void updateRects() {
        float innerDist = (float)(mRadii*Math.cos(Math.PI/4));
        float x = mPosition.getX();
        float y = mPosition.getY();
        mInnerRect.set((int)(x-innerDist),(int)(y-innerDist),(int)(x+innerDist),(int)(y+innerDist));
        mEdgeRect.set(x - mRadii, y - mRadii, x + mRadii, y + mRadii);
    }
}
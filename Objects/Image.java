package com.yazka.figureitout_alpha.Objects;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;


/**
 * Represents a image object taken from drawables with a position defined by a Rect
 * Copyright 2017 Roger Kalliomäki
 *
 * @author Roger Kalliomäki
 */
public class Image {

//----------- Instance variables ---------------
    private Bitmap mBitmap;
    private Paint mPaint;
    private Rect mRect;
//----------- Constructors --------------------

    /**
     * Constructs a Image from a drawable with a position defined by a Rect
     *
     * @param rect - The Rect
     * @param resourceId - Id of drawable (bitmap)
     * @param context - Context with global information about the application environment
     */
    public Image(Rect rect, int resourceId, Context context) {
        mBitmap = BitmapFactory.decodeResource(context.getResources(), resourceId);
        mPaint = new Paint();
        mRect = rect;
    }

//-------------------- Methods ------------------------

    /**
     * Draws the image to a canvas
     *
     * @param canvas - The canvas to draw to
     */
    public void drawImage(Canvas canvas) {
        canvas.drawBitmap(mBitmap,null,mRect, mPaint);
    }

    /**
     * Returns the left x-position of the image
     *
     * @return - The left of the Rect
     */
    public float getLeft() {return mRect.left;}

    /**
     * Returns the width of the image
     *
     * @return - The width
     */
    public float getWidth() {return mRect.right-mRect.left;}

    /**
     * Offsets the image with dx in the x-direction
     *
     * @param offset - The offset
     */
    public void offsetX(int offset) {
        int newLeft = mRect.left+offset;
        int newRight = mRect.right+offset;
        mRect.set(newLeft,mRect.top,newRight,mRect.bottom);
    }
}

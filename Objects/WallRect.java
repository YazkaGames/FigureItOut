package com.yazka.figureitout_alpha.Objects;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.preference.PreferenceManager;

/**
 * Represents a rectangular wall
 * Copyright 2017 Roger Kalliomäki
 *
 * @author Roger Kalliomäki
 */
public class WallRect {

//----------- Instance variables ---------------
    private Rect mRect;
    private Paint mPaint;
    private float mXmax,mYmax;
//----------- Constructor --------------------

    /**
     * Constructs a WallRect specified by a Rect
     *
     * @param r - The rectangle
     * @param context - Context with global information about the application environment
     */
    public WallRect(Rect r, Context context) {
        mRect = r;
        mPaint = new Paint();
        mPaint.setColor(Color.WHITE);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        mXmax = sharedPref.getFloat("xMax", 0f);
        mYmax = sharedPref.getFloat("yMax", 0f);
    }

//-------------------- Methods ------------------------

    /**
     * Check for collisions with Ball object and redirect Ball if a collision occurs
     *
     * @param b - The ball to be checked
     */
    public boolean checkForCollision(Ball b){

        float x = b.getX();
        float y = b.getY();
        float oldX = b.getPosition().getOldX();
        float oldY = b.getPosition().getOldY();
        float dx = b.getDx();
        float dy = b.getDy();
        float left = (float)mRect.left;
        float right = (float)mRect.right;
        float top = (float)mRect.top;
        float bottom = (float)mRect.bottom;
        float r = b.getRadii();

        //if ball has moved "inside" the wall
        if(x-r < right && x+r > left && y-r < bottom && y+r > top) {

            //if a (stationary) ball was created inside the wall, move it to a new random position
            if (dx == 0f && dy == 0f) {
                float randomX = (float)Math.random()*mXmax;
                float randomY = (float)Math.random()*mYmax;
                b.setPosition(randomX,randomY);
                return false;
            }


            //if ball crosses the top of the wall
            if (left<x && x<right && oldY + r <= top) {
                //move ball back outside wall
                moveBack(b);
                //direct collision vertically flip sign of y
                b.getVelocity().flipSignY();
                b.updateRects();
                return true;
            }
            //if ball crosses the bottom of the wall
            if (left<x && x<right && oldY - r >= bottom) {
                //move ball back outside wall
                moveBack(b);
                //direct collision vertically flip sign of y
                b.getVelocity().flipSignY();
                b.updateRects();
                return true;
            }
            //if ball crosses the left side of the wall
            if (top<y && y<bottom && oldX + r <= left) {
                //move ball back outside wall
                moveBack(b);
                //direct collision horizontally flip sign of x
                b.getVelocity().flipSignX();
                b.updateRects();
                return true;
            }
            //if ball crosses the right side of the wall
            if (top<y && y<bottom && oldX - r >= right) {
                //move ball back outside wall
                moveBack(b);
                //direct collision horizontally flip sign of x
                b.getVelocity().flipSignX();
                b.updateRects();
                return true;
            }


            //collision with top left corner of the wall
            if(b.getDistance(left,top)<=r) {
                //move ball back outside wall
                moveBack(b);
                //set n to normal vector of the boundary line between ball and corner (pointing towards ball)
                Vector n = b.getTemp();
                n.set(x-left,y-top);
                n.normalize();
                //reflection against (imaginary) line between ball and corner
                cornerCollision(n,b);
                b.updateRects();
                return true;
            }
            //collision with top right corner of the wall
            if(b.getDistance(right,top)<=r){
                //move ball back outside wall
                moveBack(b);
                //set n to normal vector of the boundary line between ball and corner (pointing towards ball)
                Vector n = b.getTemp();
                n.set(x-right,y-top);
                n.normalize();
                //reflection against (imaginary) line between ball and corner
                cornerCollision(n,b);
                b.updateRects();
                return true;
            }
            //collision with bottom right corner of the wall
            if(b.getDistance(right,bottom)<=r) {
                //move ball back outside wall
                moveBack(b);
                //set n to normal vector of the boundary line between ball and corner (pointing towards ball)
                Vector n = b.getTemp();
                n.set(x-right,y-bottom);
                n.normalize();
                //reflection against (imaginary) line between ball and corner
                cornerCollision(n,b);
                b.updateRects();
                return true;
            }
            //collision with bottom left corner of the wall
            if(b.getDistance(left,bottom)<=r) {
                //move ball back outside wall
                moveBack(b);
                //set n to normal vector of the boundary line between ball and corner (pointing towards ball)
                Vector n = b.getTemp();
                n.set(x-left,y-bottom);
                n.normalize();
                //reflection against (imaginary) line between ball and corner
                cornerCollision(n,b);
                b.updateRects();
                return true;
            }
        }
        return false;
    }

    /**
     * Reflection against (imaginary) line between ball and corner
     *
     * @param normal - Normal vector of line pointing towards ball
     * @param b - The ball to collide against the corner
     */
    private void cornerCollision(Vector normal, Ball b) {
        normal.scale(b.getVelocity().length());
        b.setVelocity(normal);
    }

    /**
     * Draws the wall to the specified canvas
     *
     * @param canvas - The canvas to draw on
     */
    public void drawWall(Canvas canvas){
        canvas.drawRect(mRect, mPaint);
    }

    /**
     * Move ball back one time step and set old position to two time steps back
     *
     * @param b - The ball to move back
     */
    private void moveBack(Ball b) {
        //move back ball one time step (to get ball outside wall again)
        b.getPosition().sub(b.getVelocity());
        //set old position "two steps" back (one more step back)
        Vector backStep = b.getTemp();
        backStep.copy(b.getPosition());
        backStep.sub(b.getVelocity());
        b.getPosition().setOld(backStep.getX(), backStep.getY());
    }
}

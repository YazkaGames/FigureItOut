package com.yazka.figureitout_alpha.Objects;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

import com.yazka.figureitout_alpha.R;

/**
 * Represents a (a rectangle) region of the lower part of the screen used
 * to give information about the level to the user.
 * Copyright 2017 Roger Kalliomäki
 *
 * @author Roger Kalliomäki
 */
public class LevelMenu {

//----------- Instance variables ---------------
    private float mBottom,mXmax,mYmax;
    private int mLevel,mScore;
    private Paint mBorderPaint,mLevelPaint,mTextPaint,mBackgroundPaint;
    private Ball mBackButton,mRestartButton;
//----------- Constructors --------------------

    /**
     * Constructs a LevelMenu object at the bottom of the screen with a specified color.
     *
     * @param xMax - Width of screen
     * @param yMax - Height of screen
     * @param level - Level nr
     * @param score - Level score
     * @param color - Color of menu
     * @param context - Context
     */
    public LevelMenu(float xMax, float yMax, int level, int score, String color, Context context) {
        mXmax = xMax;
        mYmax = yMax;
        mBottom = mYmax/15f;
        mLevel = level;
        mScore = score;
        mBackgroundPaint = new Paint();
        mBackgroundPaint.setColor(Color.BLACK);
        mLevelPaint = new Paint();
        mLevelPaint.setColor(Color.parseColor(color));
        mLevelPaint.setAlpha(150);
        mBorderPaint = new Paint();
        mBorderPaint.setColor(Color.parseColor(color));
        mBorderPaint.setStrokeWidth(mYmax / 1200f);
        mTextPaint = new Paint();
        mTextPaint.setColor(Color.BLACK);
        mTextPaint.setAlpha(160);
        Typeface textFont = Typeface.createFromAsset(context.getAssets(), "fonts/jelly_crazies.ttf");
        mTextPaint.setTypeface(textFont);
        mTextPaint.setTextSize(mXmax/28);
        mBackButton = new Ball(mYmax/30f, mXmax/15f, mYmax/30f,
                mXmax/100, R.drawable.back, context);
        mBackButton.setColor(Color.BLACK);
        mBackButton.setAlpha(0);
        mRestartButton = new Ball(mYmax/32f, mXmax-mXmax/15f, mYmax/30f,
                mXmax/100, R.drawable.restart, context);
        mRestartButton.setColor(Color.BLACK);
        mRestartButton.setAlpha(0);

    }

//---------------------- Methods ------------------------

    /**
     * Checks if a button is pressed.
     *
     * @param x - X-position to be checked
     * @param y - Y-position to be checked
     * @return - 1 if back button is pressed, 2 if restart button is pressed, else 0
     */
    public int checkButtons(float x, float y) {
        if(mBackButton.checkIfInside(x,y))
            return 1;
        else if (mRestartButton.checkIfInside(x,y))
            return 2;
        return 0;
    }

    /**
     * Draws the menu to the canvas.
     *
     * @param canvas - The canvas to draw to
     */
    public void draw(Canvas canvas) {
        canvas.drawRect(0f, 0f, mXmax, mBottom, mBackgroundPaint);
        canvas.drawRect(0f, 0f, mXmax, mBottom, mLevelPaint);
        canvas.drawLine(0f, mBottom, 0f, mYmax, mBorderPaint);
        canvas.drawLine(0f, mYmax, mXmax, mYmax, mBorderPaint);
        canvas.drawLine(mXmax, mYmax, mXmax, mBottom, mBorderPaint);
        canvas.drawText("LEVEL  " + String.valueOf(mLevel) +
                " | SCORE  " + String.valueOf(mScore), 0.16f*mXmax, 0.04f*mYmax, mTextPaint);
        mBackButton.drawBall(canvas);
        mRestartButton.drawBall(canvas);
    }

    /**
     * Returns the y-value of the bottom of the menu.
     *
     * @return - Y-value of the bottom of the menu.
     */
    public float getBottom() {return mBottom;}
}

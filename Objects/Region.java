package com.yazka.figureitout_alpha.Objects;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.ColorInt;


/**
 * Represents a (a rectangle) region of the screen with a color and a boolean flag
 * Copyright 2017 Roger Kalliomäki
 *
 * @author Roger Kalliomäki
 */
public class Region {

//----------- Instance variables ---------------
    private Rect mRect;
    private @ColorInt int mColor = Color.BLACK;
    private boolean mFlag = false;
    private Paint mPaint;
//----------- Constructors --------------------

    /**
     * Constructs a Region from coordinates
     *
     * @param x1 - Left position
     * @param y1 - Top position
     * @param x2 - Right position
     * @param y2 - Bottom position
     */
    public Region(int x1, int y1, int x2, int y2) {
        mRect = new Rect(x1,y1,x2,y2);
        mPaint = new Paint();
    }

    /**
     * Constructs a Region from a rect object
     *
     * @param r - A Rect object
     */
    public Region(Rect r) {
        mRect = new Rect(r.left,r.top,r.right,r.bottom);
        mPaint = new Paint();
    }

    /**
     * Constructs a Region from a Rect object with specified color
     *
     * @param r - A Rect object
     * @param color - The color of the region
     */
    public Region(Rect r, String color) {
        mRect = r;
        mPaint = new Paint();
        mPaint.setColor(Color.parseColor(color));
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth((float)(r.right-r.left)/20f);
    }

//-------------------- Methods ------------------------

    /**
     * Checks if a position is in the region
     *
     * @param x - The x-position to be checked
     * @param y - The y-position to be checked
     *@return True if position is in region, false else
     */
    public boolean contains(float x, float y){

        return (int)x>mRect.left && (int)x<mRect.right && (int)y>mRect.top && (int)y<mRect.bottom;}

    /**
     * Used to shrink a region (when the region is used as a bar timer)
     *
     */
    public void decrease(){
        mRect.set(mRect.left,mRect.top+1,mRect.right,mRect.bottom);
    }

    /**
     * Draws the region to the specified canvas
     *
     * @param canvas - The canvas to draw on
     */
    public void drawRegion(Canvas canvas){
        //mPaint.setColor(mColor);
        canvas.drawRect(mRect, mPaint);
    }

    /**
     * Returns the color of the Region
     *
     * @return The color of the Region
     */
    public @ColorInt int getColor() {
        return mColor;
    }

    /**
     * Returns the flag
     *
     *@return the flag
     */
    public boolean getFlag(){return mFlag;}

    /**
     * Returns the Rect which defines the region
     *
     * @return The Region as a Rect
     */
    public Rect getRect() {
        return mRect;
    }

    public boolean touchingBall(Ball b) {
        return (b.getX()+b.getRadii())>mRect.left && (b.getX()-b.getRadii())<mRect.right
                && (b.getY()+b.getRadii())>mRect.top && (b.getY()-b.getRadii())<mRect.bottom;
    }

    /**
     * Offsets the Region with dx in x-direction an dy in y-direction
     *
     * @param dx - Offset in x-direction
     * @param dy - Offset in y-direction
     */
    public void offset(float dx, float dy){
        int left = mRect.left + (int)dx;
        int right = mRect.right + (int)dx;
        int top = mRect.top + (int)dy;
        int bottom = mRect.bottom + (int)dy;
        mRect.set(left,top,right,bottom);
    }

    /**
     * Sets the alpha of the region
     *
     * @param alpha - The alpha
     */
    public void setAlpha(int alpha) {
        mPaint.setAlpha(alpha);
    }

    /**
     * Sets a new value to the bottom of the region
     *
     * @param bottom - New left value
     */
    public void setBottom(float bottom) {mRect.set(mRect.left,mRect.top,mRect.right,(int)bottom);}

    /**
     * Sets the color
     *
     * @param c - The color
     */
    public void setColor(@ColorInt int c) {

        mColor = c;
        mPaint.setColor(c);
    }

    /**
     * Used to set the flag
     *
     */
    public void setFlag(boolean b){mFlag = b;}

    /**
     * Sets a new value to the left of the region
     *
     * @param left - New left value
     */
    public void setLeft(float left) {mRect.set((int)left,mRect.top,mRect.right,mRect.bottom);}

    /**
     * Sets a new value to the right of the region
     *
     * @param right - New right value
     */
    public void setRight(float right) {mRect.set(mRect.left,mRect.top,(int)right,mRect.bottom);}

    /**
     * Sets a new style to the paint
     *
     * @param style - The new style
     */
    public void setStyle(Paint.Style style) {mPaint.setStyle(style);}

    /**
     * Sets a new value to the top of the region
     *
     * @param top - New top value
     */
    public void setTop(float top) {mRect.set(mRect.left,(int)top,mRect.right,mRect.bottom);}

    /**
     * Used to reset a region which is used as at timer-bar
     *
     *@param h - The height of the region (timer-bar)
     */
    public void resetTimer(int h){
        mRect.set(mRect.left,mRect.top-h,mRect.right,mRect.bottom);
    }
}
package com.yazka.figureitout_alpha.Objects;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

import com.yazka.figureitout_alpha.R;

/**
 * Represents a rectangle region of the lower part of the screen used to give
 * information about the level to the user.
 * Copyright 2017 Roger Kalliomäki
 *
 * @author Roger Kalliomäki
 */
public class AreaMenu {

//----------- Instance variables ---------------
    private float mTop,mXmax,mYmax;
    private int mArea,mScore;
    private Paint mTopPaint,mLevelPaint,mTextPaint,mBackgroundPaint;
    private Ball mBackButton,mForwardButton;
//----------- Constructors --------------------

    /**
     * Constructs a AreaMenu object at the bottom of the screen with a specified color.
     *
     * @param xMax - Width of screen
     * @param yMax - Height of screen
     * @param area - Area nr
     * @param score - Level score
     * @param color - Color of menu
     * @param context - Context with global information about the application environment
     */
    public AreaMenu(float xMax, float yMax, int area, int score, String color, Context context) {
        mXmax = xMax;
        mYmax = yMax;
        mTop = mYmax-mYmax/15f;
        mArea = area;
        mScore = score;
        mBackgroundPaint = new Paint();
        mBackgroundPaint.setColor(Color.BLACK);
        mLevelPaint = new Paint();
        mLevelPaint.setColor(Color.parseColor(color));
        mLevelPaint.setAlpha(200);
        mTopPaint = new Paint();
        mTopPaint.setColor(Color.WHITE);
        mTopPaint.setStrokeWidth(mYmax / 1200f);
        mTextPaint = new Paint();
        mTextPaint.setColor(Color.BLACK);
        mTextPaint.setAlpha(160);
        Typeface textFont = Typeface.createFromAsset(context.getAssets(), "fonts/jelly_crazies.ttf");
        mTextPaint.setTypeface(textFont);
        mTextPaint.setTextSize(mXmax/28);
        mBackButton = new Ball(mYmax/30f, mXmax/15f, mTop+mYmax/30f,
                mXmax/100, R.drawable.back, context);
        mBackButton.setColor(Color.BLACK);
        mBackButton.setAlpha(0);
        mForwardButton = new Ball(mYmax/30f, mXmax-mXmax/15f, mTop+mYmax/30f,
                mXmax/100, R.drawable.forward_white, context);
        mForwardButton.setColor(Color.BLACK);
        mForwardButton.setAlpha(0);

    }

//---------------------- Methods ------------------------

    /**
     * Checks if a button is pressed.
     *
     * @param x - X-position to be checked
     * @param y - Y-position to be checked
     * @return - 1 if back button is pressed, 2 if forward_white button is pressed, else 0
     */
    public int checkButtons(float x, float y) {
        if(mBackButton.checkIfInside(x,y))
            return 1;
        else if (mForwardButton.checkIfInside(x,y))
            return 2;
        return 0;
    }

    /**
     * Draws the menu to the canvas.
     *
     * @param canvas - The canvas to draw to
     */
    public void draw(Canvas canvas) {
        canvas.drawRect(0f,mTop,mXmax,mYmax,mBackgroundPaint);
        canvas.drawRect(0f,mTop,mXmax,mYmax,mLevelPaint);
        canvas.drawLine(0f,mTop,mXmax,mTop,mTopPaint);
        canvas.drawText("AREA  " + String.valueOf(mArea) +
                " | SCORE  " + String.valueOf(mScore),mXmax/6f,mYmax-mYmax/38f, mTextPaint);
        mBackButton.drawBall(canvas);
        mForwardButton.drawBall(canvas);
    }
    /**
     * Returns the y-value of the top of the menu.
     *
     * @return - Y-value of the top of the menu.
     */
    public float getTop() {return mTop;}

    /**
     * Sets the total score of the Area.
     *
     * @param score - The total score
     */
    public void setScore(int score) {mScore=score;}

    /**
     * Change forward_white button to white (off)
     *
     * @param c - Context with global information about the application environment
     */
    public void turnOffForwardButton(Context c) {
        mForwardButton.changeBitmap(R.drawable.forward_alpha, c);
    }

    /**
     * Change forward_white button to black (on)
     *
     * @param c - Context with global information about the application environment
     */
    public void turnOnForwardButton(Context c) {
        mForwardButton.changeBitmap(R.drawable.forward_black, c);
    }


}

package com.yazka.figureitout_alpha.Objects;

/**
 * Represents a vector given by x- and y-coordinates
 * Copyright 2017 Roger Kalliomäki
 *
 * @author Roger Kalliomäki
 */
public class Vector {

//----------- Instance variables --------------
    private float mX,mY,mOldX,mOldY;
//----------- Constructors --------------------

    /**
     * Constructs a Vector object with specified coordinates
     *
     * @param x - The x-coordinate
     * @param y - The y-coordinate
     */
    public Vector(float x, float y) {
        mX = x;
        mY = y;
    }

//-------------------- Methods ------------------------

    /**
     * Adds a given vector to the existing one
     *
     * @param v - The vector to be added to this vector
     */
    public void add(Vector v) {
        mX = mX + v.mX;
        mY = mY + v.mY;
    }

    /**
     * Copy function
     *
     * @param copy - The Vector to be copied
     */
    public void copy(Vector copy) {
        mX = copy.mX;
        mY = copy.mY;
        mOldX = copy.mOldX;
        mOldY = copy.mOldY;
    }

    /**
     * Returns the distance from this vector (point) to a x,y position
     *
     * @param x - x-coordinate (point) to compute the distance to
     * @param y - y-coordinate (point) to compute the distance to
     * @return The distance between this vector and the position given by x and y
     */
    public float distance(float x, float y) {
        return (float)Math.sqrt((mX - x) * (mX - x) + (mY - y) * (mY - y));
    }

    /**
     * Returns the distance from this vector (point) to another vector (point)
     *
     * @param v - The vector (point) to compute the distance to
     * @return The distance between this vector and the vector v
     */
    public float distance(Vector v) {
        return (float)Math.sqrt((mX - v.mX)*(mX - v.mX) + (mY - v.mY)*(mY - v.mY));
    }

    /**
     * Computes the scalar product
     *
     * @param v - The vector to be multiplied with this vector
     * @return The scalar product
     */
    public float dot(Vector v) {

        return v.mX*this.mX + v.mY*this.mY;
    }

    /**
     * Flips the x component of the vector
     *
     */
    public void flipSignX() {

        mX = -mX;
    }

    /**
     * Flips the y component of the vector
     */
    public void flipSignY() {

        mY = -mY;
    }

    /**
     * Computes the angle of two vectors
     *
     * @return The angle in radians
     */
    public float getAngle(Vector v) {

        return (float)Math.acos(this.dot(v));
    }

    /**
     * Computes the angle of the vector
     *
     * @return The angle in radians
     */
    public float getAngle() {
        return (float)Math.atan2(mY, mX);
    }

    /**
     * Gets the mOldX value
     */
    public float getOldX() {
        return mOldX;
    }

    /**
     * Gets the mOldY value
     */
    public float getOldY() {
        return mOldY;
    }

    /**
     * Gets the x-coordinate
     *
     * @return The x coordinate
     */
    public float getX() {
        return mX;
    }

    /**
     * Gets the y-coordinate
     *
     * @return The y coordinate
     */
    public float getY() {
        return mY;
    }

    /**
     * Computes the length of the vector
     *
     * @return The length
     */
    public float length() {
        return (float)Math.sqrt(mX * mX + mY * mY);
    }

    /**
     * Normalizes the vector
     */
    public void normalize() {
        float scaleToNormal = (float)Math.sqrt(mX*mX + mY*mY);
        if(scaleToNormal>0f) {
            mX = mX / scaleToNormal;
            mY = mY / scaleToNormal;
        }
    }

    /**
     * Scales the vector
     *
     * @param d - The scaling factor
     */
    public void scale(float d) {
        mX = d*mX;
        mY = d*mY;
    }

    /**
     * Sets the vector to (x,y)
     *
     * @param x - The x-position
     * @param y - The y-position
     */
    public void set(float x, float y) {
        mX = x;
        mY = y;
    }

    /**
     * Sets the vector as v2-v1
     *
     * @param v1 - The vector to subtract from v2
     * @param v2 - The vector to subtract v1 from
     */
    public void setDirection(Vector v1, Vector v2) {

        mX = v2.getX() - v1.getX();
        mY = v2.getY() - v1.getY();
    }

    /**
     * Sets the length of a vector
     *
     * @param newL - The new length
     */
    public void setLength(float newL) {
        float oldL = this.length();
        this.scale(newL/oldL);
    }

    /**
     * Sets the OldX and mOldY value
     *
     * @param y - The y value
     */
    public void setOld(float x, float y) {
        mOldX = x;
        mOldY = y;
    }

    /**
     * Set the vector according to polar coordinates
     *
     * @param length - The length of the vector
     * @param angle - The angle (in radians)
     */
    public void setPolar(float length, float angle) {
        mX = length*(float)Math.cos(angle);
        mY = length*(float)Math.sin(angle);
    }

    /**
     * Sets vector (assuming vector = (0,0)->(this.mX,this,mY)) to corresponding normal vector
     */
    public void setToNormal() {
        float temp,scaleToNormal;
        temp = mX;
        mX = -mY;
        mY = temp;
        scaleToNormal = (float)Math.sqrt(mX*mX + mY*mY);
        if(scaleToNormal!=0) {
            mX = mX / scaleToNormal;
            mY = mY / scaleToNormal;
        }
    }

    /**
     * Sets the mX value
     *
     * @param x - The x value
     */
    public void setX(float x) {
        mX = x;
    }
    /**
     * Sets the mY value
     *
     * @param y - The y value
     */
    public void setY(float y) {
        mY = y;
    }

    /**
     * Subtracts a given vector from the existing one
     *
     * @param v - The vector to be subtracted from this vector
     */
    public void sub(Vector v) {
        mX = mX - v.mX;
        mY = mY - v.mY;
    }
}
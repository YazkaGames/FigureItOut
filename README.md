Small Android puzzle game written as a hobby project.
The goal was to build the game from scratch, without using a game engine/platform like LibGDX or Unity.
The whole game is written in Java using Android Studio.

This repository only contains the main Java code of the game. If you are interested in the whole Android Studio project,
send an email with a request to yazkagames@gmail.com.

Copyright 2017 Roger Kalliomäki

[Link to game on Google Play](https://play.google.com/store/apps/details?id=com.yazka.figureitout_alpha)


package com.yazka.figureitout_alpha;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import com.yazka.figureitout_alpha.Areas.Area1Activity;
import com.yazka.figureitout_alpha.Objects.Ball;
import com.yazka.figureitout_alpha.Objects.Image;
import com.yazka.figureitout_alpha.Objects.Region;
import java.util.ArrayList;
import java.util.Random;

/**
 * Start screen activity with settings and a start game button.
 * Copyright 2017 Roger Kalliomäki
 *
 * @author Roger Kalliomäki
 */
public class StartupActivity extends AppCompatActivity {

//************************************************************************************************//
    /**
     * Inner class providing the view
     */
    private class GameView extends View {

        /**
         * Constructor for the view
         *
         * @param context - Context with global information about the application environment
         */
        public GameView(Context context) {
            super(context);
        }

        /**
         * Calls the update function to update conditions and drawCanvas which draws a new
         * frame to the canvas.
         *
         * @param canvas - The canvas to draw to
         */
        @Override
        protected void onDraw(Canvas canvas) {

            //when user is not about to exit app
            if(!mExit) {
                update();
                drawCanvas(canvas);
            }
            //when user is about to exit app
            else
                drawExit(canvas);
            invalidate();
        }
    }
//************************************************************************************************//

//----------- Instance variables ---------------
    private float mXmin,mXmax,mYmin,mYmax,mSpeedLimit,mTouchX,mTouchY,mReleaseX,mReleaseY;
    private boolean mDrawStartSettings,mDrawMenu,mDrawSkip,mDrawYazka,mReset,
            mDrawPresents,mDrawFigure,mDrawIt,mDrawOut,mStartAnimation,mTouchFlag,mExit;
    private int mFrameCounter,mFallTime;
    private Ball mStartButton,mSettingsButton,mMusicButton,mResetButton,mBackButton,mSkipButton,
            mResetBall,mSoundButton;
    private Region mMenuBackground;
    private ArrayList<Ball> mFIO;
    private SharedPreferences mSharedPref;
    private GameView mGameView;
    private Image mYazkaText,mPresentsText;
    private MediaPlayer mMP;
//-----------------------------------------------

    /**
     * Sets everything up when the activity is created
     *
     * @param savedInstanceState -  Bundle object containing the activity's previously saved state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        //set up app
        setDisplayBounds();
        setInitialConditions();
        setContentView(mGameView);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        //media player for theme music
        mMP = MediaPlayer.create(this,R.raw.startup_music);
        if(mSharedPref.getBoolean("MUSIC_ON", true))
            mMP.setVolume(1f,1f);
        else
            mMP.setVolume(0f,0f);
    }

    /**
     * Sets up the initial conditions for the activity
     */
    private void setInitialConditions(){

        //used to reset all devices (increase nr for each reset)
        boolean reset = mSharedPref.getBoolean("RESET_GAME_PROGRESS_2", true);
        if(reset) {
            resetGame();
            SharedPreferences.Editor editor = mSharedPref.edit();
            editor.putBoolean("RESET_GAME_PROGRESS_2", false);
            editor.apply();
        }
        //initial values to instance variables
        mGameView = new GameView(this);
        mFrameCounter = 0;
        mDrawStartSettings = false;
        mDrawMenu = false;
        mDrawSkip = true;
        mDrawYazka = true;
        mDrawPresents = false;
        mDrawFigure = false;
        mDrawIt = false;
        mDrawOut = false;
        mExit = false;
        mReset = false;
        mStartAnimation = true;
        mSpeedLimit = mXmax/140;
        Random r = new Random();
        mFallTime = r.nextInt(65 - 55) + 55;
        //add objects
        addBalls();
        addButtons();
        addTexts();
    }

    /**
     * Updates conditions in activity
     */
    private void update() {

        //only update when app is not closing
        if(!mExit) {
            //update the frame counter
            if (mFrameCounter != 0)
                mFrameCounter++;

            //start animations
            if (mStartAnimation) {
                startAnimation();
            }

            //check for touch button presses
            checkButtons();

            //update balls
            for (int i = 0; i < mFIO.size(); i++) {
                mFIO.get(i).move(mSpeedLimit);
                for (int j = i + 1; j < mFIO.size(); j++) {
                    if (mFIO.get(i).isCollision(mFIO.get(j)))
                        mFIO.get(i).collide(mFIO.get(j));
                }
                mFIO.get(i).checkForBorderCollision(mXmax, mYmax, mXmin, mYmin);
            }
        }
    }

    /**
     * Adds the texts "YaZka Games" and "Presents" as images
     */
    private void addTexts() {
        //"YaZka" text
        int width = (int)(0.7f*mXmax);
        int height = (int)(mYmax/12);
        int left = (int)(mXmin-1.85f*width);
        int top = (int)mYmin;
        Rect rect = new Rect(left,top,left+width,top+height);
        mYazkaText = new Image(rect,R.drawable.yazkagames,this);
        //"Presents" text
        width = (int)(0.35f*mXmax);
        height = (int)(mYmax/28);
        left = (int)(mXmax/2-width/2);
        top = (int)(0.15f*mYmax);
        Rect rect2 = new Rect(left,top,left+width,top+height);
        mPresentsText = new Image(rect2,R.drawable.presents,this);
    }

    /**
     * Adds the balls with the text "Figure It Out"
     */
    private void addBalls() {

        mFIO = new ArrayList<>();
        float size = mXmax/12f;
        //add balls
        mFIO.add(new Ball(size, size, 0.42f*mYmax, R.drawable.f, this));
        mFIO.add(new Ball(size, mXmax/4, 0.48f*mYmax, R.drawable.i, this));
        mFIO.add(new Ball(size, mXmax/2-size, 0.42f*mYmax, R.drawable.g, this));
        mFIO.add(new Ball(size, mXmax/2+size, 0.48f*mYmax, R.drawable.u, this));
        mFIO.add(new Ball(size, 3*mXmax/4, 0.42f*mYmax, R.drawable.r, this));
        mFIO.add(new Ball(size, mXmax-size, 0.48f*mYmax, R.drawable.e, this));
        mFIO.add(new Ball(size, 0.38f*mXmax, 0.66f*mYmax, R.drawable.i, this));
        mFIO.add(new Ball(size, 0.62f*mXmax, 0.66f*mYmax, R.drawable.t, this));
        mFIO.add(new Ball(size, mXmax/4, 0.84f*mYmax, R.drawable.o, this));
        mFIO.add(new Ball(size, mXmax/2, 0.84f*mYmax, R.drawable.u, this));
        mFIO.add(new Ball(size, 3*mXmax/4, 0.84f*mYmax, R.drawable.t, this));
        //set colors
        mFIO.get(0).setColor(Color.parseColor("#F4511E"));
        mFIO.get(1).setColor(Color.parseColor("#F4511E"));
        mFIO.get(2).setColor(Color.parseColor("#F4511E"));
        mFIO.get(3).setColor(Color.parseColor("#F4511E"));
        mFIO.get(4).setColor(Color.parseColor("#F4511E"));
        mFIO.get(5).setColor(Color.parseColor("#F4511E"));
        mFIO.get(6).setColor(Color.parseColor("#00897B"));
        mFIO.get(7).setColor(Color.parseColor("#00897B"));
        mFIO.get(8).setColor(Color.parseColor("#FB8C00"));
        mFIO.get(9).setColor(Color.parseColor("#FB8C00"));
        mFIO.get(10).setColor(Color.parseColor("#FB8C00"));
    }

    /**
     * Adds all buttons (and the background for the menu)
     */
    private void addButtons() {

        //start and settings button
        mStartButton = new Ball(mYmax/13, 0.5f*mXmax, 0.5f*mYmax, mXmax/70, R.drawable.play, this);
        mSettingsButton = new Ball(mYmax/13, 0.5f*mXmax, 0.7f*mYmax, mXmax/70, R.drawable.settings, this);

        //sound on/off button
        int resource;
        if(mSharedPref.getBoolean("SOUND_ON",true))
            resource = R.drawable.sound_on;
        else
            resource = R.drawable.sound_off;
        mSoundButton = new Ball(mYmax/13, 0.5f*mXmax, 0.3f*mYmax, mXmax/70, resource, this);
        mSoundButton.setColor(Color.BLACK);
        mSoundButton.setAlpha(150);

        //music on/off button
        if(mSharedPref.getBoolean("MUSIC_ON",true))
            resource = R.drawable.music_on;
        else
            resource = R.drawable.music_off;
        mMusicButton = new Ball(mYmax/13, 0.5f*mXmax, 0.5f*mYmax, mXmax/70, resource, this);
        mMusicButton.setColor(Color.BLACK);
        mMusicButton.setAlpha(150);

        //reset button
        mResetButton = new Ball(mYmax/13, 0.5f*mXmax, 0.7f*mYmax, mXmax/70, R.drawable.reset, this);
        mResetButton.setColor(Color.BLACK);
        mResetButton.setAlpha(150);
        mResetBall = new Ball(0f);
        mResetBall.setPosition(0.5f*mXmax,0.7f*mYmax);
        mResetBall.setColor(ContextCompat.getColor(this, R.color.dark_red));
        mResetBall.setAlpha(200);

        //back button
        mBackButton = new Ball(mYmax/18, mXmax/2, 0.85f*mYmax, mXmax/50,R.drawable.back, this);
        mBackButton.setColor(Color.BLACK);
        mBackButton.setAlpha(0);

        //background for menu
        mMenuBackground = new Region(new Rect((int)(0.1*mXmax),(int)(0.2f*mYmax),
                (int)(0.9*mXmax),(int)(0.9*mYmax)));
        mMenuBackground.setColor(Color.WHITE);
        mMenuBackground.setAlpha(150);

        //skip button
        mSkipButton = new Ball(mYmax/18, 0.9f*mXmax, 0.95f*mYmax, mXmax/50,R.drawable.forward_white, this);
        mSkipButton.setAlpha(0);
    }

    /**
     * Checks if buttons are pressed and takes the corresponding actions
     */
    private void checkButtons() {

        //check skip button
        if(mSkipButton.checkIfInside(mReleaseX,mReleaseY)) {
            mDrawSkip = false;
            mDrawStartSettings = true;
        }

        //check start button
        if(mTouchFlag && mStartButton.checkIfInside(mTouchX,mTouchY)) {
            mStartButton.setFill();
        }
        else if(!mExit)
            mStartButton.setStroke();
        if(!mDrawMenu && mStartButton.checkIfInside(mReleaseX,mReleaseY) && mDrawStartSettings) {
            fadeOutMusic();
            Intent intent = new Intent(this, Area1Activity.class);
            startActivity(intent);
            mExit = true;
        }

        //check settings button
        if(mTouchFlag && mSettingsButton.checkIfInside(mTouchX,mTouchY)) {
            mSettingsButton.setFill();
        }
        else
            mSettingsButton.setStroke();
        if(!mDrawMenu && mSettingsButton.checkIfInside(mReleaseX,mReleaseY) && mDrawStartSettings) {
            mDrawStartSettings = false;
            mDrawMenu = true;
        }

        //if the menu is on
        if(mDrawMenu) {

            //check sound on/off button
            if (mTouchFlag && mSoundButton.checkIfInside(mTouchX, mTouchY))
                mSoundButton.setFill();
            else
                mSoundButton.setStroke();
            if(mSoundButton.checkIfInside(mReleaseX,mTouchY)) {
                SharedPreferences.Editor editor = mSharedPref.edit();
                if(mSharedPref.getBoolean("SOUND_ON", true)) {
                    mSoundButton.changeBitmap(R.drawable.sound_off, this);
                    editor.putBoolean("SOUND_ON", false);
                }
                else {
                    mSoundButton.changeBitmap(R.drawable.sound_on, this);
                    editor.putBoolean("SOUND_ON", true);
                }
                editor.apply();
            }

            //check music on/off button
            if (mTouchFlag && mMusicButton.checkIfInside(mTouchX, mTouchY))
                mMusicButton.setFill();
            else
                mMusicButton.setStroke();
            if(mMusicButton.checkIfInside(mReleaseX,mTouchY)) {
                SharedPreferences.Editor editor = mSharedPref.edit();
                if(mSharedPref.getBoolean("MUSIC_ON", true)) {
                    mMP.setVolume(0f,0f);
                    mMusicButton.changeBitmap(R.drawable.music_off, this);
                    editor.putBoolean("MUSIC_ON", false);
                }
                else {
                    mMP.setVolume(1f,1f );
                    mMusicButton.changeBitmap(R.drawable.music_on, this);
                    editor.putBoolean("MUSIC_ON", true);
                }
                editor.apply();
            }

            //check the reset game progress button
            if (!mReset && mTouchFlag && mResetButton.checkIfInside(mTouchX, mTouchY)) {
                float growRate = mXmax/400;
                mResetBall.setRadii(mResetBall.getRadii()+growRate);
            }
            else if(!mReset){
                mResetBall.setRadii(0f);
            }

            //check back button
            if (mBackButton.checkIfInside(mReleaseX, mReleaseY)) {
                mDrawMenu = false;
                mDrawStartSettings = true;
                mReset = false;
                mResetButton.setColor(Color.BLACK);
                mResetButton.setAlpha(150);
            }
        }

        //if the reset button has been completely filled (pressed), reset game progress
        if(mResetBall.getRadii()>mResetButton.getRadii()){
            mResetButton.setColor(ContextCompat.getColor(this, R.color.dark_red));
            mReset = true;
            mResetBall.setRadii(0f);
            resetGame();
        }
        //set release positions (touch events) outside screen
        mReleaseX = -100;
        mReleaseY = -100;
    }

    /**
     * All animations in the start of the activity
     */
    private void startAnimation() {

        //move the yazkagames image to the middle
        if(mYazkaText.getLeft()<(mXmax/2-mYazkaText.getWidth()/2)) {
            if(!mMP.isPlaying() && mYazkaText.getLeft()+1.2f*mYazkaText.getWidth()>=0) {
                mMP.start();
                mMP.setLooping(true);
            }
            int offset = (int)(mXmax/130);
            mYazkaText.offsetX(offset);
            if(mYazkaText.getLeft()>=(mXmax/2-mYazkaText.getWidth()/2)) {
                mFrameCounter = 1;
            }
        }

        //draw presents delay ms after yazkagames has reached it's position
        int delay = 15;
        if(mFrameCounter == delay) {
            mDrawPresents = true;
        }

        //draw the 6 first balls ("figure") delay ms after yazkagames has reached it's position
        delay = 135;
        if(mFrameCounter == delay) {
            mDrawFigure = true;
        }

        //draw balls 7 and 8 ("it") delay ms after yazkagames has reached it's position
        delay = 180;
        if(mFrameCounter == delay) {
            mDrawIt = true;
        }

        //draw balls 8-10 ("out") delay ms after yazkagames has reached it's position
        delay = 220;
        if(mFrameCounter == delay) {
            mDrawOut = true;
        }

        //draw play and settings button delay ms after yazkagames has reached it's position
        delay = 375;
        if(!mDrawStartSettings && mFrameCounter == delay) {
            if(!mDrawMenu)
                mDrawStartSettings = true;
            mDrawSkip = false;
        }

        //balls start to fall delay ms after yazkagames has reached it's position
        delay = 610;
        if(mFrameCounter > delay) {
            mDrawYazka = false;
            mDrawPresents = false;
            dropBalls(delay);
        }
    }

    /**
     * Drops the "Figure It Out" balls for mFallTime nr of frames
     *
     * @param startTime - The frame to start the drop on
     */
    private void dropBalls(long startTime) {

        //nr of frames between each "ball drop"
        int offset = 10;
        //strength of gravity
        float gravity = mXmax/6000;

        //drop balls for mFallTime nr of frames with offset between "drops"
        if(mFrameCounter > startTime && mFrameCounter < startTime+mFallTime)
            mFIO.get(1).drop(mSpeedLimit,gravity);
        startTime += offset;
        if(mFrameCounter > startTime && mFrameCounter < startTime+mFallTime)
            mFIO.get(3).drop(mSpeedLimit,gravity);
        startTime += offset;
        if(mFrameCounter > startTime && mFrameCounter < startTime+mFallTime)
            mFIO.get(2).drop(mSpeedLimit,gravity);
        startTime += offset;
        if(mFrameCounter > startTime && mFrameCounter < startTime+mFallTime)
            mFIO.get(4).drop(mSpeedLimit,gravity);
        startTime += offset;
        if(mFrameCounter > startTime && mFrameCounter < startTime+mFallTime)
            mFIO.get(0).drop(mSpeedLimit,gravity);
        startTime += offset;
        if(mFrameCounter > startTime && mFrameCounter < startTime+mFallTime)
            mFIO.get(5).drop(mSpeedLimit,gravity);
        startTime += offset;
        if(mFrameCounter > startTime && mFrameCounter < startTime+mFallTime)
            mFIO.get(6).drop(mSpeedLimit,gravity);
        startTime += offset;
        if(mFrameCounter > startTime && mFrameCounter < startTime+mFallTime)
            mFIO.get(7).drop(mSpeedLimit,gravity);
        startTime += offset;
        if(mFrameCounter > startTime && mFrameCounter < startTime+mFallTime)
            mFIO.get(8).drop(mSpeedLimit,gravity);
        startTime += offset;
        if(mFrameCounter > startTime && mFrameCounter < startTime+mFallTime)
            mFIO.get(9).drop(mSpeedLimit,gravity);
        startTime += offset;
        if(mFrameCounter > startTime && mFrameCounter < startTime+mFallTime)
            mFIO.get(10).drop(mSpeedLimit,gravity);
        startTime += offset;
        if(mFrameCounter > startTime+mFallTime)
            mStartAnimation = false;
    }

    /**
     * Draws everything on the canvas
     *
     * @param canvas - The canvas
     */
    private void drawCanvas(Canvas canvas) {

        //background color for canvas
        canvas.drawColor(Color.BLACK);

        //draw the 6 first balls ("figure")
        if(mDrawFigure) {
            for (int i = 0; i < 6; i++)
                mFIO.get(i).drawBall(canvas);
            if(mDrawIt) {
                //draw the 7th and 8th balls ("it")
                for (int i = 6; i < 8; i++)
                    mFIO.get(i).drawBall(canvas);
            }
            if(mDrawOut) {
                //draw the 9th-11th balls ("out")
                for (int i = 8; i < 11; i++)
                    mFIO.get(i).drawBall(canvas);
            }
        }

        //draw yazka games text
        if(mDrawYazka)
            mYazkaText.drawImage(canvas);
        //draw presents text
        if(mDrawPresents)
            mPresentsText.drawImage(canvas);
        //draw buttons
        if(mDrawStartSettings) {
            mStartButton.drawBall(canvas);
            mSettingsButton.drawBall(canvas);
        }
        //draw menu
        if(mDrawMenu) {
            mMenuBackground.drawRegion(canvas);
            mSoundButton.drawBall(canvas);
            mMusicButton.drawBall(canvas);
            mResetButton.drawBall(canvas);
            mBackButton.drawBall(canvas);
            mResetBall.drawBall(canvas);
        }
        //draw skip button
        if(mDrawSkip)
            mSkipButton.drawBall(canvas);
    }

    /**
     * Used when new activity is started
     *
     * @param canvas - The canvas to draw to
     */
    private void drawExit(Canvas canvas) {
        //draw whole canvas black
        canvas.drawColor(Color.BLACK);
    }

    /**
     * Resets all game progress
     */
    private void resetGame() {
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putInt("score_1", 0);
        editor.putInt("score_2", 0);
        editor.putInt("score_3", 0);
        editor.putInt("score_4", 0);
        editor.putInt("score_5", 0);
        editor.putInt("score_6", 0);
        editor.putInt("score_7", 0);
        editor.putInt("score_8", 0);
        editor.putInt("score_9", 0);
        editor.putInt("score_total", 0);



        editor.apply();
    }

    /**
     * When the back button is pressed
     */
    @Override
    public void onBackPressed() {
        //if menu is up the back button closes it
        if(mDrawMenu) {
            mDrawMenu = false;
            mDrawStartSettings = true;
            mResetButton.setColor(Color.BLACK);
            mResetButton.setAlpha(150);
        }
        else {
            super.onBackPressed();
        }

    }

    /**
     * Called when the activity is no longer visible to the player
     */
    @Override
    protected void onStop() {
        fadeOutMusic();
        super.onStop();
    }

    /**
     * Called when the activity is destroyed (closed)
     */
    @Override
    protected void onDestroy() {
        //release media player
        if(mMP != null) {
            mMP.release();
            mMP = null;
        }
        super.onDestroy();
    }

    /**
     * Running the game when activity is resumed
     */
    @Override
    protected void onResume(){
        //only start music if resuming app (looping is set to true at first run)
        if(!mMP.isPlaying() && mMP.isLooping()) {
            mMP.start();
        }
        super.onResume();
        mExit = false;
        hideSystemUI();
    }

    /**
     * fades out volume of media player
     */
    private void fadeOutMusic() {
        if(mSharedPref.getBoolean("MUSIC_ON", true)) {
            float volume = 1f;
            while (volume > 0f) {
                mMP.setVolume(volume, volume);
                volume -= 0.007;
            }
            if (mMP.isPlaying())
                mMP.pause();
            mMP.setVolume(1f, 1f);
        }
        else {
            if (mMP.isPlaying())
                mMP.pause();
        }
    }

    /**
     * Event listener for touch events
     *
     * @param event - touch event
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // get pointer index from the event object
        int pointerIndex = event.getActionIndex();

        // get masked (not specific to a pointer) action
        int action = event.getAction();

        switch (action) {

            //regular touch
            case MotionEvent.ACTION_DOWN:
                mTouchX = event.getX(pointerIndex);
                mTouchY = event.getY(pointerIndex);
                mTouchFlag = true;
                break;

            //touch event (finger) moves
            case MotionEvent.ACTION_MOVE:
                mTouchX = event.getX(pointerIndex);
                mTouchY = event.getY(pointerIndex);
                break;

            //touch event ended (finger up)
            case MotionEvent.ACTION_UP:
                mTouchFlag = false;
                mReleaseX = event.getX(pointerIndex);
                mReleaseY = event.getY(pointerIndex);
                break;

            default:
                return false;
        }
        return true;
    }

    /**
     * Checks if device has software keys
     *
     * @return - true if device has software keys, else false
     */
    public boolean hasSoftKeys(){
        //get metrics for whole screen
        WindowManager wm = (WindowManager)getSystemService(Context.WINDOW_SERVICE);
        Display d = wm.getDefaultDisplay();
        DisplayMetrics realDisplayMetrics = new DisplayMetrics();
        d.getRealMetrics(realDisplayMetrics);
        int realHeight = realDisplayMetrics.heightPixels;
        int realWidth = realDisplayMetrics.widthPixels;
        //get metrics for used screen
        DisplayMetrics displayMetrics = new DisplayMetrics();
        d.getMetrics(displayMetrics);
        int displayHeight = displayMetrics.heightPixels;
        int displayWidth = displayMetrics.widthPixels;
        //if whole screen>used screen software keys are used
        return (realWidth - displayWidth) > 0 || (realHeight - displayHeight) > 0;
    }

    /**
     * Getting the bounds of the display and saving in shared preferences
     */
    public void setDisplayBounds() {

        //if device screen already has been saved use shared preferences
        if(mSharedPref.getFloat("xMax",0f) > 1) {
            mXmax = mSharedPref.getFloat("xMax",0f);
            mYmax = mSharedPref.getFloat("yMax",0f);
            mXmin = 0;
            mYmin = 0;
            return;
        }
        //else check screen size and save in shared preferences
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int navBar = 0;
        int resource = getResources().getIdentifier("navigation_bar_height","dimen","android");
        if(resource>0 && hasSoftKeys())
            navBar += getResources().getDimensionPixelSize(resource);
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putFloat("xMax", size.x);
        editor.putFloat("yMax", size.y + navBar);
        mXmax = size.x;
        mYmax = size.y + navBar;
        mXmin = 0;
        mYmin = 0;
        editor.apply();
    }

    /**
     * Hides the system bars
     */
    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        mGameView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }
}

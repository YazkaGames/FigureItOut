package com.yazka.figureitout_alpha.Areas;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import com.yazka.figureitout_alpha.Levels.Level0Activity;
import com.yazka.figureitout_alpha.Levels.Level1Activity;
import com.yazka.figureitout_alpha.Levels.Level2Activity;
import com.yazka.figureitout_alpha.Levels.Level3Activity;
import com.yazka.figureitout_alpha.Levels.Level4Activity;
import com.yazka.figureitout_alpha.Levels.Level5Activity;
import com.yazka.figureitout_alpha.Objects.AreaMenu;
import com.yazka.figureitout_alpha.Objects.Button;
import com.yazka.figureitout_alpha.Objects.Region;
import com.yazka.figureitout_alpha.R;
import com.yazka.figureitout_alpha.Tools.AudioPlay;

import java.util.ArrayList;

/**
 * Game menu for Area 1 in the game with level buttons and score rectangles.
 * At the bottom there is a small area with total score and back/forward_white buttons.
 * Copyright 2017 Roger Kalliomäki
 *
 * @author Roger Kalliomäki
 */
public class Area2Activity extends AppCompatActivity {

//************************************************************************************************//
    /**
     * Inner class providing the view
     */
    private class GameView extends View {

        /**
         * Constructor for the view
         *
         * @param context - Context with global information about the application environment
         */
        public GameView(Context context) {
            super(context);
        }

        /**
         * Calls the update function to update conditions and drawCanvas which draws a new
         * frame to the canvas.
         *
         * @param canvas - The canvas to draw to
         */
        @Override
        protected void onDraw(Canvas canvas) {
            checkButtons();
            drawCanvas(canvas);
            invalidate();
        }
    }
//************************************************************************************************//

//----------- Instance variables ---------------
    private static final int MAX_SCORE  = 6;
    private float mXmin,mXmax,mYmin,mYmax,mTouchX,mTouchY,mReleaseX,mReleaseY;
    private float[] mScoreLimit = new float[9];
    private int[] mScores = new int[9];
    private SharedPreferences mSharedPref;
    private GameView mGameView;
    private ArrayList<Button> mButtons;
    private ArrayList<Region> mScore;
    private boolean mTouchFlag,mDrawBlack,mButtonPressed;
    private AreaMenu mAreaMenu;
//-----------------------------------------------

    /**
     * Sets everything up when the activity is created
     *
     * @param savedInstanceState -  Bundle object containing the activity's previously saved state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mSharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        super.onCreate(savedInstanceState);
        setDisplayBounds();
        setInitialConditions();
        setMenu();
        setContentView(mGameView);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    /**
     * Sets up the initial conditions for the activity
     */
    private void setInitialConditions(){

        //score limits to access a level
        mScoreLimit[0] = 360f;
        mScoreLimit[1] = 360f;
        mScoreLimit[2] = 360f;
        mScoreLimit[3] = 360f;
        mScoreLimit[4] = 360f;
        mScoreLimit[5] = 360f;
        mScoreLimit[6] = 360f;
        mScoreLimit[7] = 360f;
        mScoreLimit[8] = 360f;
        /*mScoreLimit[0] = 0f;
        mScoreLimit[1] = 0f;
        mScoreLimit[2] = 0f;
        mScoreLimit[3] = 0f;
        mScoreLimit[4] = 0f;*/
        //initial values to instance variables
        mButtonPressed = false;
        mDrawBlack = false;
        mGameView = new GameView(this);
        mButtons = new ArrayList<>();
        mScore = new ArrayList<>();
        //add objects
        addButtons();
        addScoreRects();
        setScores();
    }


    /**
     * Adds buttons
     */
    private void addButtons() {

        //position and color of buttons
        mButtons.add(new Button(mXmax/6f, 1.8f*mYmax/7f,mXmax/8f,
                "#9FA8DA","#5C6BC0", R.drawable.one,this));
        mButtons.add(new Button(3f*mXmax/6f, 1.8f*mYmax/7f,mXmax/8f,
                "#FFAB91","#FF7043",R.drawable.two,this));
        mButtons.add(new Button(5f*mXmax/6f, 1.8f*mYmax/7f,mXmax/8f,
                "#E6EE9C","#D4E157",R.drawable.three,this));
        mButtons.add(new Button(mXmax/6f, 3.8f*mYmax/7f,mXmax/8f,
                "#EF5350","#E53935",R.drawable.four,this));
        mButtons.add(new Button(3f*mXmax/6f, 3.8f*mYmax/7f,mXmax/8f,
                "#80DEEA","#26C6DA",R.drawable.five,this));
        mButtons.add(new Button(5f*mXmax/6f, 3.8f*mYmax/7f,mXmax/8f,
                "#BA68C8","#8E24AA",R.drawable.six,this));
        mButtons.add(new Button(mXmax/6f, 5.8f*mYmax/7f,mXmax/8f,
                "#F06292","#D81B60",R.drawable.seven,this));
        mButtons.add(new Button(3f*mXmax/6f, 5.8f*mYmax/7f,mXmax/8f,
                "#80CBC4","#26A69A",R.drawable.eight,this));
        mButtons.add(new Button(5f*mXmax/6f, 5.8f*mYmax/7f,mXmax/8f,
                "#B0BEC5","#78909C",R.drawable.nine,this));
    }

    /**
     * Add rectangles showing the scores for the levels
     */
    private void addScoreRects() {
        //colors of score rectangles (corresponds to color of level buttons)
        String[] colors1 = {"#9FA8DA","#FFAB91","#E6EE9C"};
        String[] colors2 = {"#EF5350","#80DEEA","#BA68C8"};
        String[] colors3 = {"#F06292","#80CBC4","#B0BEC5"};
        //positions on sizes of rectangles
        int y = (int)(mYmax/60f);
        int y1 = 2*y;
        int y2 = 3*y;
        int y3 = 5*y;
        int y4 = 6*y;
        int y5 = 8*y;
        int y6 = 9*y;
        int width = (int)(mXmax/18f);
        int gap = (int)(width/(10f));
        int x = 0;
        //add top rectangles
        int cIndex = 0;
        for (int i=0; i<18; i++) {
            if(i!=0 && i%6==0)
                cIndex++;
            mScore.add(new Region(new Rect(x+gap, y1, x+width-gap, y2), colors1[cIndex]));
            x = x + width;
        }
        //add middle rectangles
        cIndex = 0;
        x = 0;
        for (int i=0; i<18; i++) {
            if(i!=0 && i%6==0)
                cIndex++;
            mScore.add(new Region(new Rect(x+gap, y3, x+width-gap, y4), colors2[cIndex]));
            x = x + width;
        }
        //add bottom rectangles
        cIndex = 0;
        x = 0;
        for (int i=0; i<18; i++) {
            if(i!=0 && i%6==0)
                cIndex++;
            mScore.add(new Region(new Rect(x+gap, y5, x+width-gap, y6), colors3[cIndex]));
            x = x + width;
        }
    }

    /**
     * Sets the scores for each levels using shared preferences
     */
    private void setScores() {
        //load scores
        int totalScore = mSharedPref.getInt("score_total_2",0);
        mScores[0] = mSharedPref.getInt("score_2_1",0);
        mScores[1] = mSharedPref.getInt("score_2_2",0);
        mScores[2] = mSharedPref.getInt("score_2_3",0);
        mScores[3] = mSharedPref.getInt("score_2_4",0);
        mScores[4] = mSharedPref.getInt("score_2_5",0);
        mScores[5] = mSharedPref.getInt("score_2_6",0);
        mScores[6] = mSharedPref.getInt("score_2_7",0);
        mScores[7] = mSharedPref.getInt("score_2_8",0);
        mScores[8] = mSharedPref.getInt("score_2_9",0);

        //fill rectangles and buttons according to the scores
        for (int l=0; l<9; l++) {
            for (int s=0; s<mScores[l]; s++) {
                mScore.get((l*6)+s).setStyle(Paint.Style.FILL_AND_STROKE);
            }


            // TODO when Area1 is finished - change fillLevels
            //fill buttons according to total score
            //if(l>0)
                mButtons.get(l).setFillLevel((float)totalScore/mScoreLimit[l]);

            //else
                //first button should always be filled
                //mButtons.get(l).setFillLevel(1);

            //if score on level is 6, the level is done
            if(mScores[l]==6)
                mButtons.get(l).setAlpha(50);
        }
    }

    /**
     * Check for touch events inside buttons
     */
    private void checkButtons() {

        //check if button1 is pressed
        if(!mButtonPressed && mTouchFlag && mButtons.get(0).isInside(mTouchX,mTouchY))
            mButtons.get(0).buttonPressed();
        else
            mButtons.get(0).buttonReleased();
        if(mScores[0]<MAX_SCORE && !mButtonPressed && mButtons.get(0).isInside(mReleaseX,mReleaseY))
            level_1();

        //check if button2 is pressed
        if(!mButtonPressed && mTouchFlag && mButtons.get(1).isInside(mTouchX,mTouchY))
            mButtons.get(1).buttonPressed();
        else
            mButtons.get(1).buttonReleased();
        if(mScores[1]<MAX_SCORE && mButtons.get(1).isInside(mReleaseX,mReleaseY))
            level_2();

        //check if button3 is pressed
        if(!mButtonPressed && mTouchFlag && mButtons.get(2).isInside(mTouchX,mTouchY))
            mButtons.get(2).buttonPressed();
        else
            mButtons.get(2).buttonReleased();
        if(mScores[2]<MAX_SCORE && mButtons.get(2).isInside(mReleaseX,mReleaseY))
            level_3();

        //check if button4 is pressed
        if(!mButtonPressed && mTouchFlag && mButtons.get(3).isInside(mTouchX,mTouchY))
            mButtons.get(3).buttonPressed();
        else
            mButtons.get(3).buttonReleased();
        if(mScores[3]<MAX_SCORE && mButtons.get(3).isInside(mReleaseX,mReleaseY))
            level_4();

        //check if button5 is pressed
        if(!mButtonPressed && mTouchFlag && mButtons.get(4).isInside(mTouchX,mTouchY))
            mButtons.get(4).buttonPressed();
        else
            mButtons.get(4).buttonReleased();
        if(mScores[4]<MAX_SCORE && mButtons.get(4).isInside(mReleaseX,mReleaseY))
            level_5();

        //check if button6 is pressed
        if(!mButtonPressed && mTouchFlag && mButtons.get(5).isInside(mTouchX,mTouchY))
            mButtons.get(5).buttonPressed();
        else
            mButtons.get(5).buttonReleased();
        if(mScores[5]<MAX_SCORE && mButtons.get(5).isInside(mReleaseX,mReleaseY))
            level_6();

        //check if button7 is pressed
        if(!mButtonPressed && mTouchFlag && mButtons.get(6).isInside(mTouchX,mTouchY))
            mButtons.get(6).buttonPressed();
        else
            mButtons.get(6).buttonReleased();
        if(mScores[6]<MAX_SCORE && mButtons.get(6).isInside(mReleaseX,mReleaseY))
            level_7();

        //check if button8 is pressed
        if(!mButtonPressed && mTouchFlag && mButtons.get(7).isInside(mTouchX,mTouchY))
            mButtons.get(7).buttonPressed();
        else
            mButtons.get(7).buttonReleased();
        if(mScores[7]<MAX_SCORE && mButtons.get(7).isInside(mReleaseX,mReleaseY))
            level_8();

        //check if button9 is pressed
        if(!mButtonPressed && mTouchFlag && mButtons.get(8).isInside(mTouchX,mTouchY))
            mButtons.get(8).buttonPressed();
        else
            mButtons.get(8).buttonReleased();
        if(mScores[8]<MAX_SCORE && mButtons.get(8).isInside(mReleaseX,mReleaseY))
            level_9();

        //update menu
        updateMenu();

        mReleaseX = 0f;
        mReleaseY = 0f;
    }

    /**
     * Called when button1 is pressed, runs level 1
     */
    public void level_1() {
        if(mSharedPref.getInt("score_total_2", 0) >= mScoreLimit[0]) {

            //Intent intent = new Intent(this, Level0Activity.class);
            //startActivity(intent);
            mButtonPressed = true;
            AudioPlay.fadeDownToVolume(0.1f);
        }
    }

    /**
     * Called when button2 is pressed, runs level 2 if score is 2 or more
     */
    public void level_2() {
        if(mSharedPref.getInt("score_total_2", 0) >= mScoreLimit[1]) {

            //Intent intent = new Intent(this, Level0Activity.class);
            //startActivity(intent);
            mButtonPressed = true;
            AudioPlay.fadeDownToVolume(0.1f);
        }
    }

    /**
     * Called when button3 is pressed, runs level 3 if score is 5 or more
     */
    public void level_3() {
        if(mSharedPref.getInt("score_total_2", 0) >= mScoreLimit[2]) {

            //Intent intent = new Intent(this, Level0Activity.class);
            //startActivity(intent);
            mButtonPressed = true;
            AudioPlay.fadeDownToVolume(0.1f);
        }
    }

    /**
     * Called when button4 is pressed, runs level 4 if score is 8 or more
     */
    public void level_4() {
        if(mSharedPref.getInt("score_total_2", 0) >= mScoreLimit[3]) {

            //Intent intent = new Intent(this, Level0Activity.class);
            //startActivity(intent);
            mButtonPressed = true;
            AudioPlay.fadeDownToVolume(0.1f);
        }
    }

    /**
     * Called when button5 is pressed, runs level 5
     */
    public void level_5() {
        if(mSharedPref.getInt("score_total_2", 0) >= mScoreLimit[4]) {

            //Intent intent = new Intent(this, Level0Activity.class);
            //startActivity(intent);
            mButtonPressed = true;
            AudioPlay.fadeDownToVolume(0.1f);
        }
    }

    /**
     * Called when button6 is pressed, runs level 6
     */
    public void level_6() {
        if(mSharedPref.getInt("score_total_2", 0) >= mScoreLimit[5]) {

            //Intent intent = new Intent(this, Level0Activity.class);
            //startActivity(intent);
            mButtonPressed = true;
            AudioPlay.fadeDownToVolume(0.1f);
        }
    }

    /**
     * Called when button7 is pressed, runs level 7
     */
    public void level_7() {
        if(mSharedPref.getInt("score_total_2", 0) >= mScoreLimit[6]) {

            mButtonPressed = true;
            AudioPlay.fadeDownToVolume(0.1f);
        }
    }

    /**
     * Called when button8 is pressed, runs level 8
     */
    public void level_8() {
        if(mSharedPref.getInt("score_total_2", 0) >= mScoreLimit[7]) {

            mButtonPressed = true;
            AudioPlay.fadeDownToVolume(0.1f);
        }
    }

    /**
     * Called when button9 is pressed, runs level 9
     */
    public void level_9() {
        if(mSharedPref.getInt("score_total_2", 0) >= mScoreLimit[8]) {

            mButtonPressed = true;
            AudioPlay.fadeDownToVolume(0.1f);
        }
    }

    /**
     * Draws everything on the canvas
     *
     * @param canvas - The canvas
     */
    private void drawCanvas(Canvas canvas) {

        //draw background color
        canvas.drawColor(Color.BLACK);

        if(!mDrawBlack) {
            //draw buttons
            for (int i = 0; i < mButtons.size(); i++)
                mButtons.get(i).drawButton(canvas);
            //draw score rectangles
            for (int i = 0; i < mScore.size(); i++)
                mScore.get(i).drawRegion(canvas);
            //draw menu
            mAreaMenu.draw(canvas);
        }
    }

    /**
     * Running the game when activity is resumed
     */
    @Override
    protected void onResume(){
        if(mButtonPressed)
            AudioPlay.fadeUpToVolume(0.3f);
        else
            AudioPlay.playAudioAtVolume(this,R.raw.area2_music,0.3f);
        super.onResume();
        hideSystemUI();
        setScores();
        mAreaMenu.setScore(mSharedPref.getInt("score_total_2", 0));
        mButtonPressed = false;
    }

    /**
     * Event listener for touch events
     *
     * @param event - touch event
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // get pointer index from the event object
        int pointerIndex = event.getActionIndex();

        // get masked (not specific to a pointer) action
        int action = event.getAction();

        switch (action) {

            // regular touch
            case MotionEvent.ACTION_DOWN:
                mTouchX = event.getX(pointerIndex);
                mTouchY = event.getY(pointerIndex);
                mTouchFlag = true;
                break;

            case MotionEvent.ACTION_MOVE:
                mTouchX = event.getX(pointerIndex);
                mTouchY = event.getY(pointerIndex);
                break;

            // touch event ended (finger up)
            case MotionEvent.ACTION_UP:
                mTouchFlag = false;
                mReleaseX = event.getX(pointerIndex);
                mReleaseY = event.getY(pointerIndex);
                break;

            default:
                return false;
        }
        return true;
    }

    /**
     * Update the Level menu at the bottom and check for button events
     */
    private void updateMenu() {

        if(!mDrawBlack && !mTouchFlag) {
            int buttonValue = mAreaMenu.checkButtons(mReleaseX, mReleaseY);
            //if back button is pressed
            if(buttonValue == 1) {
                mDrawBlack = true;
                onBackPressed();
            }
            //if forward_white button is pressed (nothing for now)
            else if(buttonValue == 99){
                mDrawBlack = true;
                Intent intent = new Intent(this, Level0Activity.class);
                startActivity(intent);
            }
        }
        mReleaseX = 0f;
        mReleaseY = 0f;
    }

    /**
     * Set up Level Menu
     */
    private void setMenu() {
        int totalScore = mSharedPref.getInt("score_total_2",0);
        mAreaMenu = new AreaMenu(mXmax,mYmax,2,totalScore,"#517E4E",this);
        //turn off forward button until Area3 exists
        mAreaMenu.turnOffForwardButton(this);
        mYmax = mAreaMenu.getTop();
    }

    /**
     * Called when the system is about to start resuming a previous activity. Followed by either
     * onResume() if the activity returns back to the front, or onStop() if it becomes invisible
     * to the user.
     */
    @Override
    protected void onPause() {
        if(!mButtonPressed) {
            AudioPlay.stopAudio();
        }
        super.onPause();
    }

    /**
     * Setting the game up for the device screen size
     */
    public void setDisplayBounds() {
        mXmax = mSharedPref.getFloat("xMax", 0f);
        mYmax = mSharedPref.getFloat("yMax", 0f);
        mYmin = mSharedPref.getFloat("xMin", 0f);
        mXmin = mSharedPref.getFloat("yMin", 0f);
    }

    /**
     * Hides the system bars
     */
    private void hideSystemUI() {
        // Set the IMMERSIVE flag
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show
        mGameView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }
}

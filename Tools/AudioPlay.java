package com.yazka.figureitout_alpha.Tools;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;

/**
 * Used to play music in the game. Uses a static Media Player to be able to use
 * it between different activities.
 * Copyright 2017 Roger Kalliomäki
 *
 * @author Roger Kalliomäki
 */
public class AudioPlay {

//----------- Instance variables ---------------
    private static MediaPlayer mMediaPlayer;
    private static boolean mSoundOn;
    private static SharedPreferences mSharedPref;
    private static float mVolume;
//----------------------------------------------

//---------------------- Methods ------------------------

    /**
     * Plays audio in R.res.raw with resourceID id at full (1.0) volume.
     *
     * @param c - Context
     * @param id - resource ID
     */
    public static void playAudio(Context c,int id){
        mSharedPref = PreferenceManager.getDefaultSharedPreferences(c);
        mSoundOn = mSharedPref.getBoolean("MUSIC_ON", true);
        mMediaPlayer = MediaPlayer.create(c,id);
        mMediaPlayer.setLooping(true);
        if(!mMediaPlayer.isPlaying() && mSoundOn) {
            mMediaPlayer.start();
        }
        mVolume = 1f;
    }

    /**
     * Plays audio in R.res.raw with resourceID id at volume v.
     *
     * @param c - Context
     * @param id - resource ID
     */
    public static void playAudioAtVolume(Context c,int id,float v){
        mVolume = v;
        mSharedPref = PreferenceManager.getDefaultSharedPreferences(c);
        mSoundOn = mSharedPref.getBoolean("MUSIC_ON", true);
        mMediaPlayer = MediaPlayer.create(c,id);
        mMediaPlayer.setLooping(true);
        if(!mMediaPlayer.isPlaying() && mSoundOn) {
            mMediaPlayer.setVolume(v,v);
            mMediaPlayer.start();
        }
    }

    /**
     * Fades out music down to volume 0.0.
     */
    public static void fadeOut() {
        if(mMediaPlayer.isPlaying() && mSoundOn) {
            while (mVolume > 0) {
                mMediaPlayer.setVolume(mVolume, mVolume);
                mVolume -= 0.05;
            }
            mMediaPlayer.setVolume(0f, 0f);
        }
        mVolume = 0f;
    }

    /**
     * Fades up volume to volume v.
     *
     * @param v - New volume
     */
    public static void fadeUpToVolume(float v) {
        if(mMediaPlayer.isPlaying() && mSoundOn) {
            while (mVolume < v) {
                mMediaPlayer.setVolume(mVolume, mVolume);
                mVolume += 0.01f;
            }
            mMediaPlayer.setVolume(v, v);
        }
        mVolume = v;
    }

    /**
     * Fades down volume to volume v.
     *
     * @param v - New volume
     */
    public static void fadeDownToVolume(float v) {
        if(mMediaPlayer.isPlaying() && mSoundOn) {
            while (mVolume > v) {
                mMediaPlayer.setVolume(mVolume, mVolume);
                mVolume -= 0.01f;
            }
        }
        mVolume = v;
    }

    /**
     * Releases Media Player.
     */
    public static void releaseMediaPlayer() {
        if(mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    /**
     * Stop audio and release Media Player.
     */
    public static void stopAudio() {
        mMediaPlayer.stop();
        releaseMediaPlayer();
    }
}
package com.yazka.figureitout_alpha.Tools;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;


/**
 * Draws a value to the canvas
 * Copyright 2017 Roger Kalliomäki
 *
 * @author Roger Kalliomäki
 */
public class ValueDisplayer {

//----------- Instance variables ---------------
    private Paint mPaint;
    private int mX;
    private int mY;
//------------- Constructor --------------------

    /**
     * Constructs a Image from a drawable with a position defined by (x,y)
     *
     * @param x  - The x-position of the text to be displayed
     * @param y  - The y-position of the text to be displayed
     */
    public ValueDisplayer(float x, float y) {
        mPaint = new Paint();
        mPaint.setColor(Color.WHITE);
        mPaint.setTextSize(200);
        mX = (int)x;
        mY = (int)y;
    }

//------------- Methods ------------------------

    /**
     * Draws given int to argument canvas
     *
     * @param  value - The value to be displayed
     * @param canvas - The canvas to draw to
     */
    public void drawValue(int value, Canvas canvas) {
        String stringValue = Integer.toString(value);
        canvas.drawText(stringValue,mX,mY,mPaint);
    }

    /**
     * Draws given float to argument canvas
     *
     * @param  value - The value to be displayed
     * @param canvas - The canvas to draw to
     */
    public void drawValue(float value, Canvas canvas) {
        String stringValue = Float.toString(value);
        canvas.drawText(stringValue,mX,mY,mPaint);
    }

    /**
     * Draws given String to argument canvas
     *
     * @param  value - The value to be displayed
     * @param canvas - The canvas to draw to
     */
    public void drawValue(String value, Canvas canvas) {
        canvas.drawText(value,mX,mY,mPaint);
    }

}
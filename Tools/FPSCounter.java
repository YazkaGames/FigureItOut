package com.yazka.figureitout_alpha.Tools;

/**
 * Computes the current FPS.
 * Copyright 2017 Roger Kalliomäki
 *
 * @author Roger Kalliomäki
 */
public class FPSCounter {

//----------- Instance variables ---------------
    private long mOldTime;
    private float mFrameTime,mFPS;
    private int mCounter,mNrOfFrames;
//------------- Constructor --------------------

    /**
     * Constructs a FPS counter
     */
    public FPSCounter(){
        mOldTime = System.currentTimeMillis();
    }

    /**
     * Constructs a (mean value) FPS counter
     *
     * @param nrOfFrames - The nr of frames to compute the mean FPS over
     */
    public FPSCounter(int nrOfFrames) {
        mOldTime = System.currentTimeMillis();
        mCounter = 0;
        mFrameTime = 0f;
        mFPS = 0f;
        mNrOfFrames = nrOfFrames;
    }

//------------- Methods ------------------------

    /**
     * Computes the current FPS
     *
     * @return - Current FPS
     */
    public float getFPS() {
        mFrameTime = System.currentTimeMillis() - mOldTime;
        mOldTime = System.currentTimeMillis();
        return  1000f/mFrameTime;
    }

    /**
     * Compute highest fps of the latest mNrOfFrames
     *
     * @return - Highest fps of the latest mNrOfFrames
     */
    public float getHighestFPS() {

        mCounter++;
        float frameTime = System.currentTimeMillis() - mOldTime;
        if(frameTime<mFrameTime || mCounter==1)
            mFrameTime = frameTime;

        if(mCounter == mNrOfFrames) {
            mFPS = 1000f/mFrameTime;
            mFrameTime = 0f;
            mCounter = 0;
        }
        mOldTime = System.currentTimeMillis();
        return mFPS;
    }

    /**
     * Compute lowest fps of the latest mNrOfFrames
     *
     * @return - Lowest fps of the latest mNrOfFrames
     */
    public float getLowestFPS() {

        mCounter++;
        float frameTime = System.currentTimeMillis() - mOldTime;
        if(frameTime>mFrameTime || mCounter==1)
            mFrameTime = frameTime;

        if(mCounter == mNrOfFrames) {
            mFPS = 1000f/mFrameTime;
            mFrameTime = 0f;
            mCounter = 0;
        }
        mOldTime = System.currentTimeMillis();
        return mFPS;
    }

    /**
     * Computes mean value (over mNrOfFrames) FPS
     *
     * @return - Mean value FPS
     */
    public float getMeanFPS() {
        mCounter++;
        mFrameTime += System.currentTimeMillis() - mOldTime;
        if(mCounter == mNrOfFrames) {
            mFrameTime /= (float)mNrOfFrames;
            mFPS = 1000f/mFrameTime;
            mFrameTime = 0f;
            mCounter = 0;
        }
        mOldTime = System.currentTimeMillis();
        return mFPS;
    }
}

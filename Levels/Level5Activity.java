package com.yazka.figureitout_alpha.Levels;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener2;
import android.hardware.SensorManager;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.ColorInt;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import com.yazka.figureitout_alpha.Objects.Ball;
import com.yazka.figureitout_alpha.Objects.LevelMenu;
import com.yazka.figureitout_alpha.Objects.LinePath;
import com.yazka.figureitout_alpha.Objects.Region;
import com.yazka.figureitout_alpha.R;
import com.yazka.figureitout_alpha.Tools.AudioPlay;

import java.util.ArrayList;

/**
 * The level consists of 4 rectangular zones with different colors and corresponding colored
 * balls that drops from the top of the screen. The goal of the level is to steer the balls
 * (by tilting the device) in such a way that they end up in the right (same colored) zones.
 * Copyright 2017 Roger Kalliomäki
 *
 * @author Roger Kalliomäki
 */
public class Level5Activity extends AppCompatActivity implements SensorEventListener2 {

//************************************************************************************************//
    /**
     * Inner class providing the view
     */
    private class GameView extends View {

        /**
         * Constructor for the view
         *
         * @param context - Context with global information about the application environment
         */
        public GameView(Context context) {
            super(context);
        }

        /**
         * Calls the update function to update game conditions and drawCanvas which draws a new
         * frame to the canvas.
         *
         * @param canvas - The canvas to draw to
         */
        @Override
        protected void onDraw(Canvas canvas) {
            update();
            drawCanvas(canvas);
            invalidate();
        }
    }
//************************************************************************************************//

//----------- Instance variables ---------------
    private SensorManager mSensorManager;
    private boolean mTouchFlag,mLevelCleared,mStopMusic,mPlayWin,mSoundOn;
    private float mXmin,mXmax,mYmin,mYmax,mSpeedLimit,mXRotation,mTiltScale,mBubblePos,mZoneSize,
            mBallRadii,mTouchX,mTouchY,mReleaseX,mReleaseY,mDefaultY,mBubbleSpeed,mMaxBubble;
    private int mFrameCounter,mMaxNrOfBubbles;
    private boolean mDrawBlack;
    private ArrayList<Ball> mBalls,mBubbles;
    private ArrayList<LinePath> mLinePaths;
    private ArrayList<Region> mZones;
    private SharedPreferences mSharedPref;
    private GameView mGameView;
    private float[] mRotationMatrix,mOrientationValues;
    private boolean[] mBallMoving = {false,false,false,false};
    private boolean[] mZoneCleared = {false,false,false,false};
    private LevelMenu mLevelMenu;
    private SoundPool mSoundPool;
    private int mSoundIDs[] = new int[5];
//-----------------------------------------------

    /**
     * Sets everything up when the level (activity) is created
     *
     * @param savedInstanceState -  Bundle object containing the activity's previously saved state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        setDisplayBounds();
        setMenu();
        setGameInitialCondition();
        setContentView(mGameView);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        hideSystemUI();

    }

    /**
     * Sets up the initial conditions for the level
     */
    public void setGameInitialCondition() {
        //the view
        mGameView = new GameView(this);
        //sound pool for game sounds
        addSounds();
        mSoundOn = (mSharedPref.getBoolean("SOUND_ON", true));
        //variables to change level difficulty
        float score = mSharedPref.getInt("score_5", 0);
        int nrOfStartBubbles = 2*(int)score;
        if(score==0) {
            mBubblePos = 1f/25f;
            mMaxNrOfBubbles = 5;
            nrOfStartBubbles = 1;
        }
        else {
            mBubblePos = 0.1f*score;
            mMaxNrOfBubbles = (int)score * 15;
        }
        //variables setting up the level
        mTiltScale = mXmax/500;
        mZoneSize = mXmax/4;
        mBubbleSpeed = -mYmax/2500f;
        mFrameCounter = 0;
        mMaxBubble = mXmax/12f;
        mLevelCleared = false;
        mDrawBlack = false;
        mStopMusic = true;
        mPlayWin = true;
        mReleaseX = -100f;
        mReleaseY = -100f;
        mSpeedLimit = mXmax*0.01f;
        mBallRadii = mXmax*0.035f;
        mDefaultY = mYmin-mBallRadii;
        //sensor manager for the tilt sensor
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mRotationMatrix = new float[9];
        mOrientationValues = new float[3];
        mXRotation = 0;
        //add game objects
        mBubbles = new ArrayList<>();
        for(int i=0; i<nrOfStartBubbles; i++)
            addBubble();
        addZones();
        addBalls();
    }

    /**
     * Add sounds to sound pool
     */
    private void addSounds() {
        AudioAttributes attrs = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();
        mSoundPool = new SoundPool.Builder()
                .setMaxStreams(10)
                .setAudioAttributes(attrs)
                .build();
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        mSoundIDs[0] = mSoundPool.load(this, R.raw.bubble_pop, 1);
        mSoundIDs[1] = mSoundPool.load(this, R.raw.wall, 2);
        mSoundIDs[2] = mSoundPool.load(this, R.raw.pen_click, 3);
        mSoundIDs[3] = mSoundPool.load(this, R.raw.bubble_pop2, 4);
        mSoundIDs[4] = mSoundPool.load(this, R.raw.win1, 5);
    }

    /**
     * Updates game conditions
     */
    private void update() {

        mFrameCounter++;

        if(!mDrawBlack) {
            //when level is cleared add multiple random bubbles and exit level
            if (mLevelCleared) {
                addRandomBubble();
                addRandomBubble();
                addRandomBubble();
                addRandomBubble();
            } else {
                //check for touch events in zones and set corresponding ball to moving
                for (int i = 0; i < mBalls.size(); i++) {
                    if (!mZoneCleared[i] && mZones.get(i).contains(mReleaseX, mReleaseY)) {
                        mBallMoving[i] = true;
                        if(mSoundOn)
                            playSound(2);
                    }
                    if (mBalls.get(i).getPosition().getY() > mYmax-mBalls.get(i).getRadii()
                            || mBalls.get(i).getPosition().getX() < -mBallRadii
                            || mBalls.get(i).getPosition().getX() > mXmax + mBallRadii) {
                        if(mSoundOn)
                            playSound(3);
                        resetZone(i);
                    }
                }

                //update bubbles
                //randomly add a bubble each 10th frame with possibility mBubblePos
                if (mFrameCounter == 10) {
                    if (mBubbles.size() < mMaxNrOfBubbles && Math.random() < mBubblePos)
                        addBubble();
                    mFrameCounter = 0;
                }
                for (int i = 0; i < mBubbles.size(); i++) {
                    mBubbles.get(i).move();
                    if (mBubbles.get(i).getPosition().getY() < -mBubbles.get(i).getRadii())
                        mBubbles.get(i).setPosition(getRandomXPos(), mYmax + mBubbles.get(i).getRadii());
                }

                //update balls and check for collisions with bubbles and zones
                float dropAcc = mYmax / 10000f;
                for (int i = 0; i < mBalls.size(); i++) {
                    if (mBallMoving[i]) {
                        mBalls.get(i).tilt(mXRotation, 0f, mTiltScale);
                        if (mBalls.get(i).getVelocity().getY() < mSpeedLimit)
                            mBalls.get(i).getVelocity().setY(mBalls.get(i).getVelocity().getY() + dropAcc);
                        if (mBalls.get(i).getVelocity().getX() > mSpeedLimit)
                            mBalls.get(i).getVelocity().setX(mSpeedLimit);
                        if (mBalls.get(i).getVelocity().getX() < -mSpeedLimit)
                            mBalls.get(i).getVelocity().setX(-mSpeedLimit);
                        mBalls.get(i).move();
                        for (int j = 0; j < mBubbles.size(); j++) {
                            if (mBalls.get(i).isCollision(mBubbles.get(j))) {
                                if(mSoundOn)
                                    playSound(0);
                                bubbleCollision(i, j);
                                break;
                            }
                        }
                        for (int j = 0; j < mZones.size(); j++) {
                            if (!mZoneCleared[j] && mZones.get(j).touchingBall(mBalls.get(i))) {
                                //if ball is in right zone
                                if (i == j) {
                                    if(mSoundOn)
                                        playSound(1);
                                    clearZone(i);
                                }
                                //if ball is in wrong zone
                                else {
                                    if(mSoundOn)
                                        playSound(3);
                                    resetZone(i);
                                }
                            }
                        }
                    }
                }
                //check if level is beaten
                if (mZoneCleared[0] && mZoneCleared[1] && mZoneCleared[2] && mZoneCleared[3])
                    mLevelCleared = true;
            }
            if (mBubbles.size() > 500) {
                int newScore = mSharedPref.getInt("score_5", 0);
                int totalScore = mSharedPref.getInt("score_total", 0);
                if (newScore < 6) {
                    newScore++;
                    totalScore++;
                }
                SharedPreferences.Editor editor = mSharedPref.edit();
                editor.putInt("score_5", newScore);
                editor.putInt("score_total", totalScore);
                editor.apply();
                mDrawBlack = true;
                mStopMusic = false;
                onBackPressed();
            }
        }

        //play win sound
        if(mSoundOn && mLevelCleared && mPlayWin) {
            playSound(4);
            mPlayWin = false;
        }

        updateMenu();

        mReleaseX = -100;
        mReleaseY = -100;
    }

    /**
     * Collision between ball and bubble. Bubble resets and ball bounces on bubble.
     *
     * @param i - Index of ball
     * @param j - Index of bubble
     */
    private void bubbleCollision(int i, int j) {

        mBalls.get(i).collideElastic(mBubbles.get(j));
        float collisionScale = mBubbles.get(j).getRadii()/(0.7f*mMaxBubble);
        mBalls.get(i).getVelocity().scale(collisionScale);
        mBubbles.remove(j);
    }

    /**
     * Clears (hides from player) the zone
     *
     * @param i - Index of zone
     */
    private void clearZone(int i) {
        mBallMoving[i] = false;
        mZoneCleared[i] = true;
        mBalls.get(i).setPosition(getRandomXPos(),-mBalls.get(i).getRadii());
        mBalls.get(i).setVelocity(0f, 0f);
        mZones.get(i).setAlpha(0);
    }

    /**
     * Resets one cleared zone to default values
     *
     * @param i - Index of zone
     */
    private void resetZone(int i) {
        mBallMoving[i] = false;
        mBalls.get(i).setPosition(getRandomXPos(),mDefaultY);
        mBalls.get(i).setVelocity(0f, 0f);
        mLinePaths.get(i).setStart(mBalls.get(i).getX(), mBalls.get(i).getY());
        mLinePaths.get(i).setStop(mBalls.get(i).getX(), mBalls.get(i).getY());
        //check if zones are cleared, if so reset the first one found
        for(int j=0; j<mZones.size(); j++) {
            if(mZoneCleared[j]) {
                mZoneCleared[j] = false;
                mZones.get(j).setAlpha(255);
                break;
            }
        }
    }

    /**
     * Draws everything on the canvas
     *
     * @param canvas - The canvas
     */
    public void drawCanvas(Canvas canvas) {

        //drawing a background color for canvas
        canvas.drawColor(Color.BLACK);

        if(!mDrawBlack) {
            //draw balls
            for (int i = 0; i < mBalls.size(); i++) {
                mBalls.get(i).drawBall(canvas);
                //draw help line (if ball is above screen)
                if(mBalls.get(i).getPosition().getY()<mYmin-mBallRadii) {
                    mLinePaths.get(i).moveTo(mBalls.get(i).getX(),mBalls.get(i).getX(),
                            mYmin+(mYmin-mBalls.get(i).getY()),mYmin);
                    mLinePaths.get(i).drawLine(canvas);
                }
            }

            //draw buttons
            for (int i = 0; i < mBubbles.size(); i++) {
                mBubbles.get(i).drawBall(canvas);
            }

            //draw zones
            for (int i = 0; i < mZones.size(); i++) {
                mZones.get(i).drawRegion(canvas);
            }
        }
        mLevelMenu.draw(canvas);

    }

    /**
     * Add zones
     */
    private void addZones() {

        mZones = new ArrayList<>();

        //add zones
        Rect rect1 = new Rect(0,(int)(mYmax-mZoneSize/3f),(int) mZoneSize,(int) mYmax);
        mZones.add(new Region(rect1));
        Rect rect2 = new Rect((int)mZoneSize,(int)(mYmax-mZoneSize/3f),(int)(2*mZoneSize),(int) mYmax);
        mZones.add(new Region(rect2));
        Rect rect3 = new Rect((int)(2*mZoneSize),(int)(mYmax-mZoneSize/3f),(int)(3*mZoneSize),(int) mYmax);
        mZones.add(new Region(rect3));
        Rect rect4 = new Rect((int) (3*mZoneSize),(int)(mYmax-mZoneSize/3f),(int)mXmax,(int)mYmax);
        mZones.add(new Region(rect4));
        //set zone colors
        mZones.get(0).setColor(ContextCompat.getColor(this, R.color.purple));
        mZones.get(1).setColor(ContextCompat.getColor(this, R.color.orange));
        mZones.get(2).setColor(ContextCompat.getColor(this, R.color.lime));
        mZones.get(3).setColor(ContextCompat.getColor(this, R.color.pink));

    }

    /**
     * Add random bubble
     */
    private void addBubble() {

        //declare 8 colors to choose randomly from
        @ColorInt int[] colors = new int[8];
        colors[0] = Color.parseColor("#3949AB");
        colors[1] = Color.parseColor("#1E88E5");
        colors[2] = Color.parseColor("#00897B");
        colors[3] = Color.parseColor("#7CB342");
        colors[4] = Color.parseColor("#B71C1C");
        colors[5] = Color.parseColor("#9FA8DA");
        colors[6] = Color.parseColor("#B0BEC5");
        colors[7] = Color.parseColor("#2E7D32");
        //random color
        int randomColor = (int)(Math.random()*colors.length);
        //random radii
        float randomRadii = (float)Math.random()*(mMaxBubble-mXmax/40f)+mXmax/40f;
        //random "float" speed
        float bubbleSpeed = (float)Math.random()*(mBubbleSpeed)+mBubbleSpeed;
        //add bubble
        mBubbles.add(new Ball(randomRadii,getRandomXPos(),mYmax+randomRadii,
                bubbleSpeed, colors[randomColor]));
    }

    /**
     * Adds a bubble with a random x-position on the screen
     */
    private void addRandomBubble(){
        addBubble();
        mBubbles.get(mBubbles.size()-1).setPosition((float)Math.random()*mXmax,(float)Math.random()*(mYmax-mYmin)+mYmin);
    }

    /**
     * Add balls
     */
    private void addBalls() {

        mBalls = new ArrayList<>();
        mLinePaths = new ArrayList<>();

        //add balls
        mBalls.add(new Ball(mBallRadii, getRandomXPos(), mDefaultY,
                ContextCompat.getColor(this, R.color.purple)));
        mBalls.add(new Ball(mBallRadii, getRandomXPos(), mDefaultY,
                ContextCompat.getColor(this, R.color.orange)));
        mBalls.add(new Ball(mBallRadii, getRandomXPos(), mDefaultY,
                ContextCompat.getColor(this, R.color.lime)));
        mBalls.add(new Ball(mBallRadii, getRandomXPos(), mDefaultY,
                ContextCompat.getColor(this, R.color.pink)));
        //add dotted lines (to show ball position when ball goes over screen)
        mLinePaths.add(new LinePath(0f,0f,0f,0f,mXmax));
        mLinePaths.get(0).setLineColor(ContextCompat.getColor(this, R.color.purple));
        mLinePaths.add(new LinePath(0f,0f,0f,0f,mXmax));
        mLinePaths.get(1).setLineColor(ContextCompat.getColor(this, R.color.orange));
        mLinePaths.add(new LinePath(0f,0f,0f,0f,mXmax));
        mLinePaths.get(2).setLineColor(ContextCompat.getColor(this, R.color.lime));
        mLinePaths.add(new LinePath(0f,0f,0f,0f,mXmax));
        mLinePaths.get(3).setLineColor(ContextCompat.getColor(this, R.color.pink));

    }

    /**
     * Returns a random x-position in the range ballRadii<x<xMax-ballRadii
     *
     * @return - Random x-position
     */
    private float getRandomXPos() {
        return (float)Math.random()*(mXmax-4*mBallRadii)+2*mBallRadii;
    }

    /**
     * Event listener for touch events
     *
     * @param event - touch event
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // get pointer index from the event object
        int pointerIndex = event.getActionIndex();

        // get masked (not specific to a pointer) action
        int action = event.getAction();

        switch (action) {

            // regular touch
            case MotionEvent.ACTION_DOWN:
                mTouchX = event.getX(pointerIndex);
                mTouchY = event.getY(pointerIndex);
                mTouchFlag = true;
                break;

            //touch event (finger) moves
            case MotionEvent.ACTION_MOVE:
                mTouchX = event.getX(pointerIndex);
                mTouchY = event.getY(pointerIndex);
                break;

            // touch event ended (finger up)
            case MotionEvent.ACTION_UP:
                mReleaseX = event.getX(pointerIndex);
                mReleaseY = event.getY(pointerIndex);
                mTouchFlag = false;
                break;

            default:
                return false;
        }
        return true;
    }

    /**
     * Set up Level Menu
     */
    private void setMenu() {
        int score = mSharedPref.getInt("score_5", 0);
        mLevelMenu = new LevelMenu(mXmax,mYmax,5,score,"#80DEEA",this);
        mYmin = mLevelMenu.getBottom();
    }

    /**
     * Update the Level menu at the bottom and check for button events
     */
    private void updateMenu() {

        if(!mDrawBlack && !mTouchFlag) {
            int buttonValue = mLevelMenu.checkButtons(mReleaseX, mReleaseY);
            //if back button is pressed
            if(buttonValue == 1) {
                mDrawBlack = true;
                onBackPressed();
            }
            //if restart button is pressed
            else if(buttonValue == 2){
                Intent intent = getIntent();
                mStopMusic = false;
                mDrawBlack = true;
                finish();
                startActivity(intent);
            }
        }
    }

    /**
     * When the back button is pressed
     */
    @Override
    public void onBackPressed() {
        mStopMusic = false;
        super.onBackPressed();
    }

    @Override
    protected void onRestart() {
        AudioPlay.playAudioAtVolume(this,R.raw.area1_music,0.1f);
        super.onRestart();
    }

    /**
     * Called when the system is about to start resuming a previous activity. Followed by either
     * onResume() if the activity returns back to the front, or onStop() if it becomes invisible
     * to the user.
     */
    @Override
    protected void onPause() {
        if(mStopMusic) {
            AudioPlay.stopAudio();
        }
        //don't receive any more updates from sensor
        mSensorManager.unregisterListener(this);
        mSoundPool.release();
        mSoundPool = null;
        super.onPause();
    }

    /**
     * Running the game when activity is resumed
     */
    @Override
    protected void onResume(){
        super.onResume();
        hideSystemUI();
        //register listener and set delay for updates to make batch operations more efficient and
        //reduce power consumption
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR),
                SensorManager.SENSOR_DELAY_GAME);
    }

    /**
     * Called after flush() is completed. All the events in the batch at the point when the flush
     * was called have been delivered to the applications registered for those sensor events.
     *
     * @param sensor - The {@link Sensor Sensor} on which flush was called
     */
    @Override
    public void onFlushCompleted(Sensor sensor) {

    }

    /**
     * Called when the accuracy of the registered sensor has changed.  Unlike
     * onSensorChanged(), this is only called when this accuracy value changes.
     *
     * @param accuracy - The new accuracy of this sensor
     */
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    /**
     * Called when there is a new sensor event.
     *
     * @param sensorEvent - The {@link SensorEvent SensorEvent}
     */
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_GAME_ROTATION_VECTOR)  {
            SensorManager.getRotationMatrixFromVector(mRotationMatrix,sensorEvent.values);
            SensorManager.remapCoordinateSystem(mRotationMatrix,SensorManager.AXIS_X,
                    SensorManager.AXIS_Y, mRotationMatrix);
            SensorManager.getOrientation(mRotationMatrix, mOrientationValues);

            mXRotation = mOrientationValues[2];
        }
    }

    /**
     * Plays game sound on background thread
     *
     * @param soundIndex - Index of game sound in array mSoundIDs
     */
    private void playSound(final int soundIndex) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                mSoundPool.play(mSoundIDs[soundIndex],1,1,1,0,1.0f);
            }
        });
    }

    /**
     * Setting the game up for the device screen size
     */
    public void setDisplayBounds() {

        mXmax = mSharedPref.getFloat("xMax", 0f);
        mYmax = mSharedPref.getFloat("yMax", 0f);
        mYmin = 0;
        mXmin = 0;
    }

    /**
     * Hides the system bars
     */
    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        mGameView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }
}
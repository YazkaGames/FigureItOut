package com.yazka.figureitout_alpha.Levels;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import com.yazka.figureitout_alpha.Objects.Ball;
import com.yazka.figureitout_alpha.Objects.LevelMenu;
import com.yazka.figureitout_alpha.Objects.Vector;
import com.yazka.figureitout_alpha.Objects.WallRect;
import com.yazka.figureitout_alpha.R;
import com.yazka.figureitout_alpha.Tools.AudioPlay;

import java.util.ArrayList;

/**
 * The level consists of 100 balls which are attracted by touch events.
 * The goal is to collect all balls inside a circle in the middle of the screen.
 * Copyright 2017 Roger Kalliomäki
 *
 * @author Roger Kalliomäki
 */
public class Level4Activity extends AppCompatActivity {

//************************************************************************************************//
    /**
     * Inner class providing the view
     */
    private class GameView extends View {

        /**
         * Constructor for the view
         *
         * @param context - Context with global information about the application environment
         */
        public GameView(Context context) {
            super(context);
        }

        /**
         * Calls the update function to update game conditions and drawCanvas which draws a new
         * frame to the canvas.
         *
         * @param canvas - The canvas to draw to
         */
        @Override
        protected void onDraw(Canvas canvas) {
            update();
            drawCanvas(canvas);
            invalidate();
        }
    }
//************************************************************************************************//

//----------- Instance variables ---------------
    private boolean mTouchFlag,mAllInsideCircle,mDrawBlack,mStopMusic,mPlayWin,mSoundOn;
    private float mXmin,mXmax,mYmin,mYmax,mTouchX,mTouchY,mReleaseX,mReleaseY,mSpeedLimit;
    private int mNrOfBalls,mEndCounter;
    private Ball mCircle,mBlinkingBall;
    private ArrayList<Ball> mBalls;
    private ArrayList<WallRect> mWalls;
    private SharedPreferences mSharedPref;
    private GameView mGameView;
    private LevelMenu mLevelMenu;
    private SoundPool mSoundPool;
    private int mSoundIDs[] = new int[3];
//-----------------------------------------------

    /**
     * Sets everything up when the level (activity) is created
     *
     * @param savedInstanceState -  Bundle object containing the activity's previously saved state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        setDisplayBounds();
        setMenu();
        setGameInitialCondition();
        setContentView(mGameView);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        hideSystemUI();
    }

    /**
     * Sets up the initial conditions for the level
     */
    public void setGameInitialCondition(){
        //the view
        mGameView = new GameView(this);
        //sound pool for game sounds
        addSounds();
        mSoundOn = (mSharedPref.getBoolean("SOUND_ON", true));
        //variables from intent to change level difficulty
        Intent intent = getIntent();
        float circleFactor = intent.getFloatExtra("CIRCLE_FACTOR",0.30f);
        int nrOfWalls = intent.getIntExtra("NR_OF_WALLS", 0);
        //variables setting up the level
        mDrawBlack = false;
        mEndCounter = 0;
        mNrOfBalls = 100;
        mSpeedLimit = mXmax*0.014f;
        mStopMusic = true;
        mPlayWin = true;
        mReleaseX = -100f;
        mReleaseY = -100f;
        float ballRadii = mXmax*0.015f;
        float circleRadii = mXmax*circleFactor;
        //wall objects
        mWalls = new ArrayList<>();
        addWalls(nrOfWalls);
        //middle circle
        mCircle = new Ball(circleRadii,mXmax/150f,new Vector(mXmax/2,mYmin+(mYmax-mYmin)/2));
        mBlinkingBall = new Ball(circleRadii,new Vector(0,0),new Vector(mXmax/2,mYmin+(mYmax-mYmin)/2),0);
        //the balls
        mBalls = new ArrayList<>();
        for(int i=0; i<mNrOfBalls; i++)
            mBalls.add(Ball.getRandomBall(ballRadii, mXmin, mYmin, mXmax, mYmax));
    }

    /**
     * Add sounds to sound pool
     */
    private void addSounds() {
        AudioAttributes attrs = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();
        mSoundPool = new SoundPool.Builder()
                .setMaxStreams(10)
                .setAudioAttributes(attrs)
                .build();
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        mSoundIDs[0] = mSoundPool.load(this, R.raw.pingpong, 1);
        mSoundIDs[1] = mSoundPool.load(this, R.raw.pingpong, 2);
        mSoundIDs[2] = mSoundPool.load(this, R.raw.win1, 3);
    }

    /**
     * Updates game conditions
     */
    private void update() {

        float attractScale = mXmax/4000;
        mAllInsideCircle = true;

        if(!mDrawBlack) {
            //update balls
            for (int i = 0; i < mNrOfBalls; i++) {
                if (mTouchFlag && mTouchY>mYmin) {
                    mBalls.get(i).attract(mTouchX, mTouchY, mSpeedLimit, attractScale);
                } else {
                    mBalls.get(i).move(mSpeedLimit);
                }
                mBalls.get(i).checkForBorderCollision(mXmax, mYmax, mXmin, mYmin);
                for (int j = 0; j < mWalls.size(); j++)
                    mWalls.get(j).checkForCollision(mBalls.get(i));
                mAllInsideCircle = mAllInsideCircle && mCircle.checkIfInside(mBalls.get(i));
            }
            if (mAllInsideCircle) {
                mBalls.clear();
                mNrOfBalls = 0;
            }

            //if end animation has run for 700 iterations, exit activity and increase level score
            if (mEndCounter > 700) {
                mEndCounter = 0;
                int newScore = mSharedPref.getInt("score_4", 0);
                int totalScore = mSharedPref.getInt("score_total", 0);
                if (newScore < 6) {
                    newScore++;
                    totalScore++;
                }
                SharedPreferences.Editor editor = mSharedPref.edit();
                editor.putInt("score_4", newScore);
                editor.putInt("score_total", totalScore);
                editor.apply();
                mDrawBlack = true;
                mStopMusic = false;
                onBackPressed();
            }

            //when all balls are inside circle play winning sound
            if(mSoundOn && mAllInsideCircle && mPlayWin) {
                playSound(2);
                mPlayWin = false;
            }
            //update menu
            updateMenu();
        }

        mReleaseX = -100f;
        mReleaseY = -100f;
    }

    /**
     * Draws everything on the canvas
     *
     * @param canvas - The canvas
     */
    private void drawCanvas(Canvas canvas) {

        //background color for canvas
        canvas.drawColor(Color.BLACK);

        if(!mDrawBlack) {
            //brighter circle when touching the screen
            if (mTouchFlag && mTouchY>mYmin)
                mCircle.setColor(Color.rgb(100, 100, 100));
            else
                mCircle.setColor(Color.rgb(20, 20, 20));
            //draw the circle
            mCircle.drawBall(canvas);

            //draw the balls
            for (int i = 0; i < mNrOfBalls; i++)
                mBalls.get(i).drawBall(canvas);

            //draw walls
            for (int i = 0; i < mWalls.size(); i++)
                mWalls.get(i).drawWall(canvas);

            //if all balls are inside circle the stage is done - draw blinking animation
            if (mAllInsideCircle) {
                canvas.drawColor(Color.BLACK);
                mEndCounter = mEndCounter + 4;
                mBlinkingBall.setColor(Color.rgb(mEndCounter % 150, mEndCounter % 150, mEndCounter % 150));
                mBlinkingBall.drawBall(canvas);
            }
        }
        //draw menu
        mLevelMenu.draw(canvas);
    }

    /**
     * Add walls (positions depend on the score of the level)
     */
    private void addWalls(int nrOfWalls) {

        int wallWidth = (int)(mXmax/20f);

        //add different amount of walls (depending on the score)
        if(nrOfWalls == 2) {
            mWalls.add(new WallRect(new Rect((int)(mXmax/2)-wallWidth, (int)(mYmin+(mYmax-mYmin)/4)-wallWidth,
                    (int)(mXmax/2)+wallWidth, (int)(mYmin+(mYmax-mYmin)/4)+wallWidth),this));
            mWalls.add(new WallRect(new Rect((int)(mXmax/2)-wallWidth, (int)(3*(mYmin+(mYmax-mYmin))/4)-wallWidth,
                    (int)(mXmax/2)+wallWidth, (int)(3*(mYmin+(mYmax-mYmin))/4)+wallWidth),this));
        }
        if (nrOfWalls == 4) {
            mWalls.add(new WallRect(new Rect((int)(mXmax/6)-wallWidth, (int)(mYmin+(mYmax-mYmin)/2)-wallWidth,
                    (int)(mXmax/6)+wallWidth, (int)(mYmin+(mYmax-mYmin)/2)+wallWidth),this));
            mWalls.add(new WallRect(new Rect((int)(5*mXmax/6)-wallWidth, (int)(mYmin+(mYmax-mYmin)/2)-wallWidth,
                    (int)(5*mXmax/6)+wallWidth, (int)(mYmin+(mYmax-mYmin)/2)+wallWidth),this));
            mWalls.add(new WallRect(new Rect((int)(mXmax/2)-wallWidth, (int)(mYmin+(mYmax-mYmin)/4)-wallWidth,
                    (int)(mXmax/2)+wallWidth, (int)(mYmin+(mYmax-mYmin)/4)+wallWidth),this));
            mWalls.add(new WallRect(new Rect((int)(mXmax/2)-wallWidth, (int)(3*(mYmin+(mYmax-mYmin))/4)-wallWidth,
                    (int)(mXmax/2)+wallWidth, (int)(3*(mYmin+(mYmax-mYmin))/4)+wallWidth),this));
        }
    }

    /**
     * Event listener for touch events
     *
     * @param event - touch event
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // get pointer index from the event object
        int pointerIndex = event.getActionIndex();

        // get masked (not specific to a pointer) action
        int action = event.getAction();

        switch (action) {

            // regular touch
            case MotionEvent.ACTION_DOWN:
                mTouchX = event.getX(pointerIndex);
                mTouchY = event.getY(pointerIndex);
                mTouchFlag = true;
                break;

            //touch event (finger) moves
            case MotionEvent.ACTION_MOVE:
                mTouchX = event.getX(pointerIndex);
                mTouchY = event.getY(pointerIndex);
                break;

            // touch event ended (finger up)
            case MotionEvent.ACTION_UP:
                mTouchFlag = false;
                mReleaseX = event.getX(pointerIndex);
                mReleaseY = event.getY(pointerIndex);
                break;

            default:
                return false;
        }
        return true;
    }

    /**
     * Set up Level Menu
     */
    private void setMenu() {
        int score = mSharedPref.getInt("score_4", 0);
        mLevelMenu = new LevelMenu(mXmax,mYmax,4,score,"#EF5350",this);
        mYmin = mLevelMenu.getBottom();
    }

    /**
     * Update the Level menu at the bottom and check for button events
     */
    private void updateMenu() {

        if(!mDrawBlack && !mTouchFlag) {
            int buttonValue = mLevelMenu.checkButtons(mReleaseX, mReleaseY);
            //if back button is pressed
            if(buttonValue == 1) {
                mDrawBlack = true;
                onBackPressed();
            }
            //if restart button is pressed
            else if(buttonValue == 2){
                Intent intent = getIntent();
                mStopMusic = false;
                mDrawBlack = true;
                finish();
                startActivity(intent);
            }
        }
    }

    /**
     * When the back button is pressed
     */
    @Override
    public void onBackPressed() {
        mStopMusic = false;
        super.onBackPressed();
    }

    @Override
    protected void onRestart() {
        AudioPlay.playAudioAtVolume(this,R.raw.area1_music,0.1f);
        super.onRestart();
    }

    /**
     * Called when the system is about to start resuming a previous activity. Followed by either
     * onResume() if the activity returns back to the front, or onStop() if it becomes invisible
     * to the user.
     */
    @Override
    protected void onPause() {
        if(mStopMusic) {
            AudioPlay.stopAudio();
        }
        mSoundPool.release();
        mSoundPool = null;
        super.onPause();
    }

    /**
     * Running the game when activity is resumed
     */
    @Override
    protected void onResume(){
        super.onResume();
        hideSystemUI();
    }

    /**
     * Plays game sound on background thread
     *
     * @param soundIndex - Index of game sound in array mSoundIDs
     */
    private void playSound(final int soundIndex) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                mSoundPool.play(mSoundIDs[soundIndex],1,1,1,0,1.0f);
            }
        });
    }

    /**
     * Setting the game up for the device screen size
     */
    public void setDisplayBounds() {

        mXmax = mSharedPref.getFloat("xMax", 0f);
        mYmax = mSharedPref.getFloat("yMax", 0f);
        mYmin = 0;
        mXmin = 0;
    }

    /**
     * Hides the system bars
     */
    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        mGameView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }
}

package com.yazka.figureitout_alpha.Levels;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.ColorInt;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import com.yazka.figureitout_alpha.Objects.Ball;
import com.yazka.figureitout_alpha.Objects.LevelMenu;
import com.yazka.figureitout_alpha.Objects.Vector;
import com.yazka.figureitout_alpha.R;
import com.yazka.figureitout_alpha.Tools.AudioPlay;

import java.util.ArrayList;

/**
 * The level consists of 4 "holes" in each corner of the screen. The goal is to get all balls
 * of the same color as the holes into the holes. This is done by placing your finger on the
 * screen which will create a growing circle that will knock away all balls inside it when the
 * finger is released.
 * Copyright 2017 Roger Kalliomäki
 *
 * @author Roger Kalliomäki
 */
public class Level6Activity extends AppCompatActivity {

//************************************************************************************************//
    /**
     * Inner class providing the view
     */
    private class GameView extends View {

        /**
         * Constructor for the view
         *
         * @param context - Context with global information about the application environment
         */
        public GameView(Context context) {
            super(context);
        }

        /**
         * Calls the update function to update game conditions and drawCanvas which draws a new
         * frame to the canvas.
         *
         * @param canvas - The canvas to draw to
         */
        @Override
        protected void onDraw(Canvas canvas) {
            update();
            drawCanvas(canvas);
            invalidate();
        }
    }
//************************************************************************************************//

//----------- Instance variables ---------------
    private boolean mTouchFlag,mLevelCleared,mStopMusic,mPlayWin,mSoundOn;
    private float mXmin,mXmax,mYmin,mYmax,mFrictionLow,mFrictionMed,mFrictionHigh,mBallRadii,mTouchX,mTouchY,
            mReleaseX,mReleaseY,mCollisionScale,mGrowRate,mZoneRadii, mDecreaseScale;
    private boolean mDrawBlack,mGrowCircle,mWallHit;
    private int mBallNr,mNrOfLiveBalls,mLevelScore;
    private ArrayList<Ball> mBalls,mZones,mTouchCircles;
    private SharedPreferences mSharedPref;
    private GameView mGameView;
    private LevelMenu mLevelMenu;
    private @ColorInt int[] mColors = new int[4];
    private SoundPool mSoundPool;
    private int mSoundIDs[] = new int[6];
//-----------------------------------------------

    /**
     * Sets everything up when the level (activity) is created
     *
     * @param savedInstanceState -  Bundle object containing the activity's previously saved state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        setDisplayBounds();
        setMenu();
        setGameInitialCondition();
        setContentView(mGameView);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        hideSystemUI();

    }

    /**
     * Sets up the initial conditions for the level
     */
    public void setGameInitialCondition() {
        //the view
        mGameView = new GameView(this);
        //sound pool for game sounds
        addSounds();
        mSoundOn = (mSharedPref.getBoolean("SOUND_ON", true));
        //variables to change level difficulty
        mLevelScore = mSharedPref.getInt("score_6", 0);
        //variables setting up the level
        mLevelCleared = false;
        mDrawBlack = false;
        mStopMusic = true;
        mPlayWin = true;
        mGrowCircle = false;
        mWallHit = false;
        mReleaseX = -100f;
        mReleaseY = -100f;
        mFrictionLow = 0.999f;
        mFrictionMed = 0.995f;
        mFrictionHigh = 0.98f;
        mBallRadii = 0.035f*mXmax;
        mZoneRadii = 5f*mBallRadii;
        mDecreaseScale = 10f/(10f+(float)mLevelScore);
        if(mLevelScore==5) {
            mDecreaseScale = 10f / 14f;
            mZoneRadii = 4f*mBallRadii;
        }
        mCollisionScale = 0.05f;
        mGrowRate = 0.003f*mXmax;
        mBallNr = 0;
        mNrOfLiveBalls = 4;
        //sensor manager for the tilt sensor
        mColors[0] = ContextCompat.getColor(this, R.color.pink);
        mColors[1] = ContextCompat.getColor(this, R.color.yellow);
        mColors[2] = ContextCompat.getColor(this, R.color.green);
        mColors[3] = ContextCompat.getColor(this, R.color.dark_blue);
        addBalls();
        addZones();
        addTouchCircles();
    }

    /**
     * Add sounds to sound pool
     */
    private void addSounds() {
        AudioAttributes attrs = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();
        mSoundPool = new SoundPool.Builder()
                .setMaxStreams(10)
                .setAudioAttributes(attrs)
                .build();
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        mSoundIDs[0] = mSoundPool.load(this, R.raw.laser_1, 1);
        mSoundIDs[1] = mSoundPool.load(this, R.raw.wall, 2);
        mSoundIDs[2] = mSoundPool.load(this, R.raw.pen_click, 3);
        mSoundIDs[3] = mSoundPool.load(this, R.raw.bubble_pop2, 4);
        mSoundIDs[4] = mSoundPool.load(this, R.raw.bubble_pop, 5);
        mSoundIDs[5] = mSoundPool.load(this, R.raw.win1, 6);
    }

    /**
     * Updates game conditions
     */
    private void update() {

        //if level is cleared draw black screen
        if(!mDrawBlack) {
            //move balls and check for collisions
            for(int i=0; i<mBalls.size(); i++) {
                //high speed => low friction
                if(mBalls.get(i).getVelocity().length() > mXmax/100f)
                    mBalls.get(i).moveWithFriction(mFrictionLow);
                //medium speed => medium friction
                else if(mBalls.get(i).getVelocity().length() > mXmax/300f)
                    mBalls.get(i).moveWithFriction(mFrictionMed);
                //low speed => high friction
                else
                    mBalls.get(i).moveWithFriction(mFrictionHigh);
                //check for collisions with borders
                if(mBalls.get(i).checkForBorderCollision(mXmax,mYmax,mXmin,mYmin) && mSoundOn)
                    playSound(1);
                //check for collisions with other balls
                for (int j = i + 1; j < mBalls.size(); j++) {
                    if (mBalls.get(i).isCollision(mBalls.get(j))) {
                        if(mSoundOn)
                            playSound(2);
                        mBalls.get(i).collide(mBalls.get(j));
                    }
                }
            }

            //if touch event on play area (and circle has not hit the wall), grow touchCircle
            if(mTouchFlag && mTouchY>mYmin && !mWallHit) {
                mTouchCircles.get(mBallNr).setPosition(mTouchX,mTouchY);
                mTouchCircles.get(mBallNr).setRadii(mTouchCircles.get(mBallNr).getRadii() + mGrowRate);
                mGrowCircle = true;
            }
            //else if circle is still growing
            else if(mGrowCircle){
                //if circle touches ball knock the ball away
                if(mTouchCircles.get(mBallNr).isCollision(mBalls.get(mBallNr))) {
                    if(mSoundOn && mBallNr!=mNrOfLiveBalls-1)
                        playSound(0);
                    mBalls.get(mBallNr).getTemp().copy(mBalls.get(mBallNr).getPosition());
                    mBalls.get(mBallNr).getTemp().sub(mTouchCircles.get(mBallNr).getPosition());
                    mBalls.get(mBallNr).getTemp().normalize();
                    mBalls.get(mBallNr).getTemp().scale(mTouchCircles.get(mBallNr).getRadii()*mCollisionScale);
                    mBalls.get(mBallNr).setVelocity(mBalls.get(mBallNr).getTemp());
                }
                //remove circle and go to next ball
                mGrowCircle = false;
                mTouchCircles.get(mBallNr).setPosition(-100f,-100f);
                mTouchCircles.get(mBallNr).setRadii(0f);
                mBallNr++;
                if(mBallNr>mNrOfLiveBalls-1) {
                    if(mSoundOn)
                        playSound(3);
                    mBallNr = 0;
                    for(int i=0; i<mZones.size(); i++)
                        mZones.get(i).setRadii(mDecreaseScale*mZones.get(i).getRadii());
                }
            }
            //check if circle hits wall
            if(mGrowCircle && !mTouchCircles.get(mBallNr).isInScreen(mXmin,mYmin,mXmax,mYmax)){
                mWallHit = true;
            }
            //if no touch event (finger lifted), reset the wall hit flag
            if(!mTouchFlag && mWallHit)
                mWallHit = false;

            //check if ball is inside corresponding (same color) hole (zone)
            //if so remove ball
            for(int i=0; i<mBalls.size(); i++) {
                if (mZones.get(i).checkIfInside(mBalls.get(i))) {
                    if(mSoundOn && mNrOfLiveBalls!=1)
                        playSound(4);
                    mWallHit = true;
                    mBalls.remove(i);
                    mTouchCircles.remove(i);
                    mZones.remove(i);
                    mNrOfLiveBalls--;
                    mBallNr--;
                    if (mBallNr < 0)
                        mBallNr = 0;
                    break;
                }
            }
            //if all balls are gone the level is cleared
            if(mNrOfLiveBalls == 0)
                mLevelCleared = true;
        }
        //if the level is cleared increase scores
        if (mLevelCleared && !mDrawBlack) {
            int newScore = mSharedPref.getInt("score_6", 0);
            int totalScore = mSharedPref.getInt("score_total", 0);
            if (newScore < 6) {
                newScore++;
                totalScore++;
            }
            SharedPreferences.Editor editor = mSharedPref.edit();
            editor.putInt("score_6", newScore);
            editor.putInt("score_total", totalScore);
            editor.apply();
            mDrawBlack = true;
            mStopMusic = false;
            onBackPressed();
        }

        //play win sound
        if(mSoundOn && mLevelCleared && mPlayWin) {
            playSound(5);
            mPlayWin = false;
        }
        //update level menu
        updateMenu();
        //set release point outside screen
        mReleaseX = -100;
        mReleaseY = -100;
    }

    /**
     * Draws everything on the canvas
     *
     * @param canvas - The canvas
     */
    public void drawCanvas(Canvas canvas) {

        //drawing a background color for canvas
        canvas.drawColor(Color.BLACK);
        //draw holes, balls and circles
        if(!mDrawBlack) {
            for(int i=0; i<mZones.size(); i++)
                mZones.get(i).drawBall(canvas);
            for(int i=0; i<mBalls.size(); i++)
                mBalls.get(i).drawBall(canvas);
            for(int i=0; i<mTouchCircles.size(); i++)
                mTouchCircles.get(i).drawBall(canvas);
        }
        mLevelMenu.draw(canvas);
    }

    /**
     * Add balls
     */
    private void addBalls() {

        mBalls = new ArrayList<>();

        //add balls if score<5 use default positions
        if(mLevelScore<5) {
            mBalls.add(new Ball(mBallRadii, new Vector(mXmax / 2f, mYmin + 4 * (mYmax - mYmin) / 5f), mColors[0]));
            mBalls.add(new Ball(mBallRadii, new Vector(mXmax / 2f, mYmin + 3 * (mYmax - mYmin) / 5f), mColors[1]));
            mBalls.add(new Ball(mBallRadii, new Vector(mXmax / 2f, mYmin + 2 * (mYmax - mYmin) / 5f), mColors[2]));
            mBalls.add(new Ball(mBallRadii, new Vector(mXmax / 2f, mYmin + (mYmax - mYmin) / 5f), mColors[3]));
        }
        //if score is 5 set balls on harder positions
        else
        {
            mBalls.add(new Ball(mBallRadii, new Vector(mXmax / 2f,   mYmin + 4 * (mYmax - mYmin) / 7f), mColors[0]));
            mBalls.add(new Ball(mBallRadii, new Vector(2*mXmax / 5f,   mYmin + 3 * (mYmax - mYmin) / 6f), mColors[1]));
            mBalls.add(new Ball(mBallRadii, new Vector(3*mXmax / 5f, mYmin + 3 * (mYmax - mYmin) / 6f), mColors[2]));
            mBalls.add(new Ball(mBallRadii, new Vector(mXmax / 2f,   mYmin + 3 * (mYmax - mYmin) / 7f), mColors[3]));
        }
    }

    /**
     * Add zones (transparent stationary ball objects)
     */
    private void addZones() {

        mZones = new ArrayList<>();

        int alpha = 150;
        //add zones
        mZones.add(new Ball(mZoneRadii,new Vector(mXmin, mYmin), mColors[0]));
        mZones.add(new Ball(mZoneRadii,new Vector(mXmax,mYmin),mColors[1]));
        mZones.add(new Ball(mZoneRadii,new Vector(mXmin,mYmax),mColors[2]));
        mZones.add(new Ball(mZoneRadii,new Vector(mXmax,mYmax),mColors[3]));
        //set alpha
        mZones.get(0).setAlpha(alpha);
        mZones.get(1).setAlpha(alpha);
        mZones.get(2).setAlpha(alpha);
        mZones.get(3).setAlpha(alpha);
    }

    /**
     * Add holes represented by ball objects
     */
    private void addTouchCircles() {

        mTouchCircles = new ArrayList<>();
        float strokeWidth = 0.002f*mXmax;
        //add holes
        mTouchCircles.add(new Ball(0f, strokeWidth, mColors[0]));
        mTouchCircles.add(new Ball(0f, strokeWidth, mColors[1]));
        mTouchCircles.add(new Ball(0f, strokeWidth, mColors[2]));
        mTouchCircles.add(new Ball(0f, strokeWidth, mColors[3]));
    }

    /**
     * Event listener for touch events
     *
     * @param event - touch event
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // get pointer index from the event object
        int pointerIndex = event.getActionIndex();

        // get masked (not specific to a pointer) action
        int action = event.getAction();

        switch (action) {

            // regular touch
            case MotionEvent.ACTION_DOWN:
                mTouchX = event.getX(pointerIndex);
                mTouchY = event.getY(pointerIndex);
                mTouchFlag = true;
                break;

            //touch event (finger) moves
            case MotionEvent.ACTION_MOVE:
                mTouchX = event.getX(pointerIndex);
                mTouchY = event.getY(pointerIndex);
                break;

            // touch event ended (finger up)
            case MotionEvent.ACTION_UP:
                mReleaseX = event.getX(pointerIndex);
                mReleaseY = event.getY(pointerIndex);
                mTouchFlag = false;
                break;

            default:
                return false;
        }
        return true;
    }

    /**
     * Set up Level Menu
     */
    private void setMenu() {
        int score = mSharedPref.getInt("score_6", 0);
        mLevelMenu = new LevelMenu(mXmax,mYmax,6,score,"#BA68C8",this);
        mYmin = mLevelMenu.getBottom();
    }

    /**
     * Update the Level menu at the bottom and check for button events
     */
    private void updateMenu() {

        if(!mDrawBlack && !mTouchFlag) {
            int buttonValue = mLevelMenu.checkButtons(mReleaseX, mReleaseY);
            //if back button is pressed
            if(buttonValue == 1) {
                mDrawBlack = true;
                onBackPressed();
            }
            //if restart button is pressed
            else if(buttonValue == 2){
                Intent intent = getIntent();
                mStopMusic = false;
                mDrawBlack = true;
                finish();
                startActivity(intent);
            }
        }
    }

    /**
     * When the back button is pressed
     */
    @Override
    public void onBackPressed() {
        mStopMusic = false;
        super.onBackPressed();
    }

    @Override
    protected void onRestart() {
        AudioPlay.playAudioAtVolume(this,R.raw.area1_music,0.1f);
        mSoundOn = (mSharedPref.getBoolean("SOUND_ON", true));
        super.onRestart();
    }

    /**
     * Called when the system is about to start resuming a previous activity. Followed by either
     * onResume() if the activity returns back to the front, or onStop() if it becomes invisible
     * to the user.
     */
    @Override
    protected void onPause() {
        if(mStopMusic) {
            AudioPlay.stopAudio();
        }
        mSoundPool.release();
        mSoundPool = null;
        super.onPause();
    }

    /**
     * Running the game when activity is resumed
     */
    @Override
    protected void onResume(){
        super.onResume();
        mSoundOn = (mSharedPref.getBoolean("SOUND_ON", true));
        hideSystemUI();
    }


    /**
     * Plays game sound on background thread
     *
     * @param soundIndex - Index of game sound in array mSoundIDs
     */
    private void playSound(final int soundIndex) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                mSoundPool.play(mSoundIDs[soundIndex],1,1,1,0,1.0f);
            }
        });
    }

    /**
     * Setting the game up for the device screen size
     */
    public void setDisplayBounds() {

        mXmax = mSharedPref.getFloat("xMax", 0f);
        mYmax = mSharedPref.getFloat("yMax", 0f);
        mYmin = 0;
        mXmin = 0;
    }

    /**
     * Hides the system bars
     */
    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        mGameView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }
}
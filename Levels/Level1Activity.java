package com.yazka.figureitout_alpha.Levels;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import com.yazka.figureitout_alpha.Objects.Ball;
import com.yazka.figureitout_alpha.Objects.LevelMenu;
import com.yazka.figureitout_alpha.Objects.LinePath;
import com.yazka.figureitout_alpha.Objects.Region;
import com.yazka.figureitout_alpha.Objects.Vector;
import com.yazka.figureitout_alpha.Objects.WallRect;
import com.yazka.figureitout_alpha.Objects.WallTriangle;
import com.yazka.figureitout_alpha.R;
import com.yazka.figureitout_alpha.Tools.AudioPlay;
import java.util.ArrayList;

/**
 * The level consists of 4 rectangular zones with different colors and corresponding colored
 * balls. The goal of the level is to shoot the balls in such a way that they end up in
 * the right (same colored) zones.
 * Copyright 2017 Roger Kalliomäki
 *
 * @author Roger Kalliomäki
 */
public class Level1Activity extends AppCompatActivity {

//************************************************************************************************//
    /**
     * Inner class providing the view
     */
    private class GameView extends View {

        /**
         * Constructor for the view
         *
         * @param context - Context with global information about the application environment
         */
        public GameView(Context context) {
            super(context);
        }

         /**
         * Calls the update function to update game conditions and drawCanvas which draws a new
         * frame to the canvas.
         *
         * @param canvas - The canvas to draw to
         */
        @Override
        protected void onDraw(Canvas canvas) {
            update();
            drawCanvas(canvas);
            invalidate();
        }
    }
//************************************************************************************************//

//----------- Instance variables ---------------
    private boolean mPlayEndAnimation,mDrawBlack,mTouchFlag,mStopMusic,mPlayWin,mSoundOn;
    private float mXmin,mXmax,mYmin,mYmax,mTouchX,mTouchY,mReleaseX,mReleaseY,mZoneSize;
    private int mScore;
    private boolean[] mBallReady = {false,false,false,false};
    private boolean[] mBallMoving = {false,false,false,false};
    private boolean[] mZoneCleared = {false,false,false,false};
    private Vector[] mDefaultBallPosition = new Vector[4];
    private ArrayList<Ball> mBalls;
    private ArrayList<LinePath> mLinePaths;
    private ArrayList<Region> mZones;
    private ArrayList<WallTriangle> mTWalls;
    private ArrayList<WallRect> mRWalls;
    private SharedPreferences mSharedPref;
    private GameView mGameView;
    private LevelMenu mLevelMenu;
    private SoundPool mSoundPool;
    private int mSoundIDs[] = new int[8];
//-----------------------------------------------

    /**
     * Sets everything up when the level (activity) is created
     *
     * @param savedInstanceState -  Bundle object containing the activity's previously saved state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        setDisplayBounds();
        setMenu();
        setGameInitialCondition();
        setContentView(mGameView);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    /**
     * Sets up the initial conditions for the level
     */
    public void setGameInitialCondition(){

        //the view
        mGameView = new GameView(this);
        //sound pool for game sounds
        addSounds();
        mSoundOn = (mSharedPref.getBoolean("SOUND_ON", true));
        //variables setting up the level
        mReleaseX = -100;
        mReleaseY = -100;
        mZoneSize = mXmax/4;
        mPlayEndAnimation = false;
        mDrawBlack = false;
        mStopMusic = true;
        mPlayWin = true;
        //the balls
        mBalls = new ArrayList<>();
        addBalls();
        mLinePaths = new ArrayList<>();
        addLines();
        //the zones
        mZones = new ArrayList<>();
        addZones();
        //the walls
        mTWalls = new ArrayList<>();
        mRWalls = new ArrayList<>();
        addWalls();
    }

    /**
     * Add sounds to sound pool
     */
    private void addSounds() {
        AudioAttributes attrs = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();
        mSoundPool = new SoundPool.Builder()
                .setMaxStreams(10)
                .setAudioAttributes(attrs)
                .build();
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        mSoundIDs[0] = mSoundPool.load(this, R.raw.load_laser,1);
        mSoundIDs[1] = mSoundPool.load(this, R.raw.laser_1,2);
        mSoundIDs[2] = mSoundPool.load(this, R.raw.wall, 3);
        mSoundIDs[3] = mSoundPool.load(this, R.raw.bord_bounce, 4);
        mSoundIDs[4] = mSoundPool.load(this, R.raw.collision, 5);
        mSoundIDs[5] = mSoundPool.load(this, R.raw.bubble_pop, 6);
        mSoundIDs[6] = mSoundPool.load(this, R.raw.bubble_pop2, 7);
        mSoundIDs[7] = mSoundPool.load(this, R.raw.win1, 8);
    }

    /**
     * Add balls to their starting (default) positions depending on the score of the level
     */
    private void addBalls() {

        //different default ball positions for different scores on the level
        float ballRadii = 0.04f*mXmax;
        if(mScore<3) {
            mDefaultBallPosition[0] = new Vector(mZoneSize / 2, mYmax - mZoneSize - ballRadii);
            mDefaultBallPosition[1] = new Vector(3 * mZoneSize / 2, mYmax - mZoneSize - ballRadii);
            mDefaultBallPosition[2] = new Vector(5 * mZoneSize / 2, mYmax - mZoneSize - ballRadii);
            mDefaultBallPosition[3] = new Vector(7 * mZoneSize / 2, mYmax - mZoneSize - ballRadii);
        }
        else if(mScore<6){
            float sin45R = 1.3f * ballRadii * (float) Math.sqrt(2) / 2;
            mDefaultBallPosition[0] = new Vector(mZoneSize + sin45R, mYmin + mZoneSize + sin45R);
            mDefaultBallPosition[1] = new Vector(mXmax - mZoneSize - sin45R, mYmin + mZoneSize + sin45R);
            mDefaultBallPosition[2] = new Vector(mXmax - mZoneSize - sin45R, mYmax - mZoneSize - sin45R);
            mDefaultBallPosition[3] = new Vector(mZoneSize + sin45R, mYmax - mZoneSize - sin45R);
        }
        else {
            float sin45R = 1.3f * ballRadii * (float) Math.sqrt(2) / 2;
            float zoneScale = mScore-5;
            mDefaultBallPosition[0] = new Vector((zoneScale*mZoneSize) + sin45R, mYmin + (zoneScale*mZoneSize) + sin45R);
            mDefaultBallPosition[1] = new Vector(mXmax - (zoneScale*mZoneSize) - sin45R, mYmin + (zoneScale*mZoneSize) + sin45R);
            mDefaultBallPosition[2] = new Vector(mXmax - (zoneScale*mZoneSize) - sin45R, mYmax - (zoneScale*mZoneSize) - sin45R);
            mDefaultBallPosition[3] = new Vector((zoneScale*mZoneSize) + sin45R, mYmax - (zoneScale*mZoneSize) - sin45R);
        }
        //add balls to defined default positions
        mBalls.add(new Ball(ballRadii, mDefaultBallPosition[0].getX(), mDefaultBallPosition[0].getY(),
                ContextCompat.getColor(this, R.color.orange)));
        mBalls.add(new Ball(ballRadii, mDefaultBallPosition[1].getX(), mDefaultBallPosition[1].getY(),
                ContextCompat.getColor(this, R.color.cyan)));
        mBalls.add(new Ball(ballRadii, mDefaultBallPosition[2].getX(), mDefaultBallPosition[2].getY(),
                ContextCompat.getColor(this, R.color.lime)));
        mBalls.add(new Ball(ballRadii, mDefaultBallPosition[3].getX(), mDefaultBallPosition[3].getY(),
                ContextCompat.getColor(this, R.color.blue_gray)));
    }

    /**
     * Add lines (paths) for the dotted lines used to shoot the balls
     */
    private void addLines() {
        //add lines according to default ball positions
        mLinePaths.add(new LinePath(mDefaultBallPosition[0].getX(), mDefaultBallPosition[0].getY(),
                mDefaultBallPosition[0].getX(), mDefaultBallPosition[0].getY(),mXmax));
        mLinePaths.add(new LinePath(mDefaultBallPosition[1].getX(), mDefaultBallPosition[1].getY(),
                mDefaultBallPosition[1].getX(), mDefaultBallPosition[1].getY(),mXmax));
        mLinePaths.add(new LinePath(mDefaultBallPosition[2].getX(), mDefaultBallPosition[2].getY(),
                mDefaultBallPosition[2].getX(), mDefaultBallPosition[2].getY(),mXmax));
        mLinePaths.add(new LinePath(mDefaultBallPosition[3].getX(), mDefaultBallPosition[3].getY(),
                mDefaultBallPosition[3].getX(), mDefaultBallPosition[3].getY(),mXmax));
        //set line colors
        mLinePaths.get(0).setLineColor(ContextCompat.getColor(this, R.color.orange));
        mLinePaths.get(1).setLineColor(ContextCompat.getColor(this, R.color.cyan));
        mLinePaths.get(2).setLineColor(ContextCompat.getColor(this, R.color.light_lime));
        mLinePaths.get(3).setLineColor(ContextCompat.getColor(this, R.color.blue_gray));
    }

    /**
     * Add zones (positions depend on the score of the level)
     */
    private void addZones() {

        //different zone positions for different scores on the level
        if(mScore<3) {
            Rect rect1 = new Rect((int)mXmin, (int) (mYmax - mZoneSize), (int) mZoneSize, (int) mYmax);
            mZones.add(new Region(rect1));
            Rect rect2 = new Rect((int) mZoneSize, (int) (mYmax - mZoneSize), (int) (2 * mZoneSize), (int) mYmax);
            mZones.add(new Region(rect2));
            Rect rect3 = new Rect((int) (2 * mZoneSize), (int) (mYmax - mZoneSize), (int) (3 * mZoneSize), (int) mYmax);
            mZones.add(new Region(rect3));
            Rect rect4 = new Rect((int) (3 * mZoneSize), (int) (mYmax - mZoneSize), (int) mXmax, (int) mYmax);
            mZones.add(new Region(rect4));
        }
        else {
            Rect rect1 = new Rect((int)mXmin, (int)mYmin, (int)mZoneSize, (int)(mYmin + mZoneSize));
            mZones.add(new Region(rect1));
            Rect rect2 = new Rect((int)(mXmax - mZoneSize), (int)mYmin, (int)mXmax, (int)(mYmin + mZoneSize));
            mZones.add(new Region(rect2));
            Rect rect3 = new Rect((int) (mXmax - mZoneSize), (int) (mYmax - mZoneSize), (int) mXmax, (int) mYmax);
            mZones.add(new Region(rect3));
            Rect rect4 = new Rect((int)mXmin, (int) (mYmax - mZoneSize), (int) mZoneSize, (int) mYmax);
            mZones.add(new Region(rect4));
        }
        //set zone colors
        mZones.get(0).setColor(ContextCompat.getColor(this, R.color.light_orange));
        mZones.get(1).setColor(ContextCompat.getColor(this, R.color.light_cyan));
        mZones.get(2).setColor(ContextCompat.getColor(this, R.color.light_lime));
        mZones.get(3).setColor(ContextCompat.getColor(this, R.color.light_blue_gray));
    }

    /**
     * Add walls (positions depend on the score of the level)
     */
    private void addWalls() {

        //different versions of walls depending on the score
        if(mScore==1) {
            mTWalls.add(new WallTriangle(mXmax / 2f + 0.01f * mXmax, mYmin + mYmax / 3f,
                    4f * mXmax / 5f, mYmin + 3f * mYmax / 5f, this));
            mTWalls.add(new WallTriangle(mXmax / 2f - 0.01f * mXmax, mYmin + mYmax / 3f,
                    mXmax / 5f, mYmin + 3f * mYmax / 5f, this));
        }
        else if (mScore==2) {
            mTWalls.add(new WallTriangle(mXmax / 2f + 0.01f * mXmax, mYmin + mYmax / 3f,
                    4f * mXmax / 5f, mYmin + 3f * mYmax / 5f, this));
            mTWalls.add(new WallTriangle(mXmax / 2f - 0.01f * mXmax, mYmin + mYmax / 3f,
                    mXmax / 5f, mYmin + 3f * mYmax / 5f, this));
            mTWalls.add(new WallTriangle(mXmin, mYmin,mXmax/3f,mYmin + mYmax/5f,this));
            mTWalls.add(new WallTriangle(mXmax, mYmin,2f*mXmax/3f,mYmin + mYmax/5f,this));
        }
        else if(mScore>3) {
            float halfWidth = 4f*mZoneSize/5f;
            Rect rect = new Rect((int)(mXmax/2f-halfWidth),(int)(mYmin + (mYmax-mYmin)/2f-halfWidth),
                    (int)(mXmax/2f+halfWidth),(int)(mYmin + (mYmax-mYmin)/2f+halfWidth));
            mRWalls.add(new WallRect(rect,this));
        }
    }

    /**
     * Updates game conditions
     */
    private void update() {

        //play win sound
        if(mPlayEndAnimation && mPlayWin) {
            if(mSoundOn)
                playSound(7);
            mPlayWin = false;
        }
        //if level is beaten
        if(mPlayEndAnimation)
            endAnimation();
        else {

            //use initialization from last frame to check if a ball should be fired
            fireBall();

            //set position of a touch event inside a zone
            InitializeBall();

            //set lines
            setLines();

            //check for ball collisions
            for (int i = 0; i < mBalls.size(); i++) {
                for (int j = i + 1; j < mBalls.size(); j++) {
                    if (mBallMoving[i] && mBallMoving[j] && mBalls.get(i).isCollision(mBalls.get(j))) {
                        if(mSoundOn)
                            playSound(4);
                        mBalls.get(i).collide(mBalls.get(j));
                    }
                }
            }

            //check for wall and border collisions
            for (int i = 0; i < mBalls.size(); i++) {
                //Borders
                if(mBallMoving[i]) {
                    mBalls.get(i).move();
                    if(mBalls.get(i).checkForBorderCollision(mXmax, mYmax, mXmin, mYmin))
                        if(mSoundOn)
                            playSound(3);
                }
                //Triangle walls
                for (int j = 0; j < mTWalls.size(); j++) {
                    if (mTWalls.get(j).checkForCollision(mBalls.get(i)))
                        if(mSoundOn)
                            playSound(2);
                }
                //Rectangular walls
                for (int j = 0; j < mRWalls.size(); j++) {
                    if (mRWalls.get(j).checkForCollision(mBalls.get(i)))
                        if(mSoundOn)
                            playSound(2);
                }
            }

            //check if ball is inside zone
            for (int i = 0; i < mBalls.size(); i++) {
                for (int j = 0; j < mZones.size(); j++) {
                    if (!mZoneCleared[j]) {
                        if (mBallMoving[i] && mZones.get(j).touchingBall(mBalls.get(i))) {
                            //if ball is in right zone
                            if (i == j)
                                clearZone(i);
                            //if ball is in wrong zone
                            else
                                resetZone(i);
                        }
                    }
                }
            }

            //update menu
            updateMenu();

            //check if level is beaten
            if(mZoneCleared[0] && mZoneCleared[1] && mZoneCleared[2] && mZoneCleared[3])
                mPlayEndAnimation = true;
        }

        mReleaseX = -100f;
        mReleaseY = -100f;
    }

    /**
     * Clears (hides from player) the zone
     *
     * @param i - index of zone
     */
    private void clearZone(int i) {
        if(mSoundOn)
            playSound(5);
        mBallMoving[i] = false;
        mBallReady[i] = false;
        mZoneCleared[i] = true;
        mBalls.get(i).setPosition(mDefaultBallPosition[i]);
        mBalls.get(i).setVelocity(0,0);
        mBalls.get(i).setAlpha(0);
        mZones.get(i).setAlpha(0);
    }

    /**
     * Resets zone to default values
     *
     * @param i - index of ball
     */
    private void resetZone(int i) {

        mBallMoving[i] = false;
        mBallReady[i] = false;
        mBalls.get(i).setPosition(mDefaultBallPosition[i]);
        mBalls.get(i).setVelocity(0,0);
        //check if any zone(s) are cleared and if so reset the first one found
        for(int j=0; j<mZones.size(); j++) {
            if(mZoneCleared[j]) {
                if(mSoundOn)
                    playSound(5);
                mZoneCleared[j] = false;
                mBalls.get(j).setAlpha(255);
                mZones.get(j).setAlpha(255);
                return;
            }
        }
        if(mSoundOn)
            playSound(5);
    }

    /**
     * Tests if any ball is ready to be fired and fires a ball if it is
     */
    private void fireBall() {

        if(!mTouchFlag) {
            //if touch is released inside zone 0 and ball is ready to fire
            if (mBallReady[0] && !mZoneCleared[0] && mZones.get(0).contains(mReleaseX, mReleaseY)) {
                if(mSoundOn)
                    playSound(1);
                setVelocity(0);
                mBallReady[0] = false;
                mBallMoving[0] = true;
            }
            //if touch is released inside zone 1 and ball is ready to fire
            if (mBallReady[1] && !mZoneCleared[1] && mZones.get(1).contains(mReleaseX, mReleaseY)) {
                if(mSoundOn)
                    playSound(1);
                setVelocity(1);
                mBallReady[1] = false;
                mBallMoving[1] = true;
            }
            //if touch is released inside zone 2 and ball is ready to fire
            if (mBallReady[2] && !mZoneCleared[2] && mZones.get(2).contains(mReleaseX, mReleaseY)) {
                if(mSoundOn)
                    playSound(1);
                setVelocity(2);
                mBallReady[2] = false;
                mBallMoving[2] = true;
            }
            //if touch is released inside zone 3 and ball is ready to fire
            if (mBallReady[3] && !mZoneCleared[3] && mZones.get(3).contains(mReleaseX, mReleaseY)) {
                if(mSoundOn)
                    playSound(1);
                setVelocity(3);
                mBallReady[3] = false;
                mBallMoving[3] = true;
            }
        }
    }

    /**
     * Sets velocity of ball depending of the position of the release touch event (finger lifted)
     */
    private void setVelocity(int i) {
        float fireScale = 0.15f;
        float dx = fireScale*(mDefaultBallPosition[i].getX()-mReleaseX);
        float dy = fireScale*(mDefaultBallPosition[i].getY()-mReleaseY);
        mBalls.get(i).setVelocity(dx,dy);
    }

    /**
     * Initialize balls (check if ready to fire)
     */
    private void InitializeBall() {

        //if score>4 only one ball at a time can be fired
        boolean noGo = false;
        if (mScore > 4) {
            for (int i = 0; i < mBallMoving.length; i++) {
                if (mBallMoving[i])
                    noGo = true;
            }
        }
        //check if balls are ready to be fired, set to true if they are
        mBallReady[0] = (!noGo && !mBallMoving[0] && mTouchFlag
                && mZones.get(0).contains(mTouchX, mTouchY));
        mBallReady[1] = (!noGo && !mBallMoving[1] && mTouchFlag
                && mZones.get(1).contains(mTouchX, mTouchY));
        mBallReady[2] = (!noGo && !mBallMoving[2] && mTouchFlag
                && mZones.get(2).contains(mTouchX, mTouchY));
        mBallReady[3] = (!noGo && !mBallMoving[3] && mTouchFlag
                && mZones.get(3).contains(mTouchX, mTouchY));
    }

    /**
     * Draws dotted line in the direction of the ball to be fired
     */
    private void setLines() {

        //if ball 0 is ready to be fired draw a dotted line in the fire direction
        if(mBallReady[0] && !mZoneCleared[0]) {
            if (mSoundOn && !mLinePaths.get(0).isLineSet())
                playSound(0);
            setLine(0);
        }
        //if ball is not ready set length of dotted line to zero
        else mLinePaths.get(0).setStop(mDefaultBallPosition[0].getX(), mDefaultBallPosition[0].getY());


        //if ball 1 is ready to be fired draw a dotted line in the fire direction
        if(mBallReady[1] && !mZoneCleared[1]) {
            if (mSoundOn && !mLinePaths.get(1).isLineSet())
                playSound(0);
            setLine(1);
        }
        //if ball is not ready set length of dotted line to zero
        else mLinePaths.get(1).setStop(mDefaultBallPosition[1].getX(), mDefaultBallPosition[1].getY());


        //if ball 2 is ready to be fired draw a dotted line in the fire direction
        if(mBallReady[2] && !mZoneCleared[2]) {
            if (mSoundOn && !mLinePaths.get(2).isLineSet())
                playSound(0);
            setLine(2);
        }
        //if ball is not ready set length of dotted line to zero
        else mLinePaths.get(2).setStop(mDefaultBallPosition[2].getX(), mDefaultBallPosition[2].getY());


        //if ball 3 is ready to be fired draw a dotted line in the fire direction
        if(mBallReady[3] && !mZoneCleared[3]) {
            if (mSoundOn && !mLinePaths.get(3).isLineSet())
                playSound(0);
            setLine(3);
        }
        //if ball is not ready set length of dotted line to zero
        else mLinePaths.get(3).setStop(mDefaultBallPosition[3].getX(), mDefaultBallPosition[3].getY());
    }

    /**
     * Defines the end position of the dotted line to be drawn
     */
    private void setLine(int i) {
        float lengthScale = 3f;
        float dx = lengthScale*(mDefaultBallPosition[i].getX()-mTouchX);
        float dy = lengthScale*(mDefaultBallPosition[i].getY()-mTouchY);
        mLinePaths.get(i).setStop(mDefaultBallPosition[i].getX()+dx, mDefaultBallPosition[i].getY()+dy);
    }

    /**
     * Function called when level is beaten. Increases the zones so they fill up the hole screen.
     * Then updates the scores and exits the activity.
     */
    private void endAnimation() {

        if(!mDrawBlack) {
            //make all zones visible
            mZones.get(0).setAlpha(255);
            mZones.get(1).setAlpha(255);
            mZones.get(2).setAlpha(255);
            mZones.get(3).setAlpha(255);

            //if score<3 increase zones upwards
            float offset;
            if(mScore<3) {
                offset  = mYmax / 100;
                mZones.get(0).setTop(mZones.get(0).getRect().top - offset);
                mZones.get(1).setTop(mZones.get(1).getRect().top - offset);
                mZones.get(2).setTop(mZones.get(2).getRect().top - offset);
                mZones.get(3).setTop(mZones.get(3).getRect().top - offset);
            }
            //if score>2 increase zones inwards the screen
            else {
                offset = mYmax / 40;
                if(mZones.get(0).getRect().bottom < mYmax) {
                    mZones.get(0).setBottom(mZones.get(0).getRect().bottom + offset);
                    mZones.get(0).setRight(mZones.get(0).getRect().right + offset);
                }
                else if(mZones.get(1).getRect().bottom < mYmax) {
                    mZones.get(1).setBottom(mZones.get(1).getRect().bottom + offset);
                    mZones.get(1).setLeft(mZones.get(1).getRect().left - offset);
                }
                else if(mZones.get(2).getRect().top > mYmin ) {
                    mZones.get(2).setTop(mZones.get(2).getRect().top - offset);
                    mZones.get(2).setLeft(mZones.get(2).getRect().left - offset);
                }
                else {
                    mZones.get(3).setTop(mZones.get(3).getRect().top - offset);
                    mZones.get(3).setRight(mZones.get(3).getRect().right + offset);
                }
            }

            //when end animation is over exit activity and increase level score
            if ( (mScore<2 && mZones.get(0).getRect().top < -mXmax)
                    || (mScore>=2 && mZones.get(3).getRect().top < -mXmax)) {
                int newScore = mSharedPref.getInt("score_1", 0);
                int totalScore = mSharedPref.getInt("score_total", 0);
                if(newScore<6) {
                    newScore++;
                    totalScore++;
                }
                SharedPreferences.Editor editor = mSharedPref.edit();
                editor.putInt("score_1", newScore);
                editor.putInt("score_total", totalScore);
                editor.apply();
                mStopMusic = false;
                mDrawBlack = true;
                onBackPressed();

            }
        }
    }

    /**
     * Draws everything on the canvas
     *
     * @param canvas - The canvas
     */
    private void drawCanvas(Canvas canvas) {

        //background color for canvas
        canvas.drawColor(Color.BLACK);

        if(!mDrawBlack) {
            //draw the lines
            for (int i = 0; i < mLinePaths.size(); i++) {
                mLinePaths.get(i).drawLine(canvas);
            }

            //draw the balls
            for (int i = 0; i < mBalls.size(); i++)
                mBalls.get(i).drawBall(canvas);

            //draw the walls
            for (int i = 0; i < mTWalls.size(); i++) {
                mTWalls.get(i).drawWall(canvas);
            }
            for (int i = 0; i < mRWalls.size(); i++) {
                mRWalls.get(i).drawWall(canvas);
            }

            //draw the zones
            for (int i = 0; i < mZones.size(); i++) {
                mZones.get(i).drawRegion(canvas);
            }

            //draw menu
            mLevelMenu.draw(canvas);
        }
    }


    /**
     * Event listener for touch events
     *
     * @param event - touch event
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // get pointer index from the event object
        int pointerIndex = event.getActionIndex();

        // get masked (not specific to a pointer) action
        int action = event.getAction();

        switch (action) {

            // regular touch
            case MotionEvent.ACTION_DOWN:
                mTouchX = event.getX(pointerIndex);
                mTouchY = event.getY(pointerIndex);
                mTouchFlag = true;
                break;

            //touch event (finger) moves
            case MotionEvent.ACTION_MOVE:
                mTouchX = event.getX(pointerIndex);
                mTouchY = event.getY(pointerIndex);
                break;

            // touch event ended (finger up)
            case MotionEvent.ACTION_UP:
                mReleaseX = event.getX(pointerIndex);
                mReleaseY = event.getY(pointerIndex);
                mTouchFlag = false;
                break;

            default:
                return false;
        }
        return true;
    }

    /**
     * Set up Level Menu
     */
    private void setMenu() {
        mScore = mSharedPref.getInt("score_1", 0);
        mLevelMenu = new LevelMenu(mXmax,mYmax,1,mScore,"#9FA8DA",this);
        mYmin = mLevelMenu.getBottom();
    }

    /**
     * Update the Level menu at the bottom and check for button events
     */
    private void updateMenu() {

        if(!mDrawBlack && !mTouchFlag) {
            int buttonValue = mLevelMenu.checkButtons(mReleaseX, mReleaseY);
            //if back button is pressed
            if(buttonValue == 1) {
                mDrawBlack = true;
                onBackPressed();
            }
            //if restart button is pressed
            else if(buttonValue == 2){
                Intent intent = getIntent();
                mStopMusic = false;
                mDrawBlack = true;
                finish();
                startActivity(intent);
            }
        }
    }

    /**
     * When the back button is pressed
     */
    @Override
    public void onBackPressed() {
        mStopMusic = false;
        super.onBackPressed();
    }

    /**
     * Activity is restarted after being stopped
     */
    @Override
    protected void onRestart() {
        AudioPlay.playAudioAtVolume(this,R.raw.area1_music,0.1f);
        super.onRestart();
    }

    /**
     * Plays game sound on background thread
     *
     * @param soundIndex - Index of game sound in array mSoundIDs
     */
    private void playSound(final int soundIndex) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                mSoundPool.play(mSoundIDs[soundIndex],1,1,1,0,1.0f);
            }
        });
    }

    /**
     * Called when the system is about to start resuming a previous activity. Followed by either
     * onResume() if the activity returns back to the front, or onStop() if it becomes invisible
     * to the user.
     */
    @Override
    protected void onPause() {
        if(mStopMusic) {
            AudioPlay.stopAudio();
        }
        mSoundPool.release();
        mSoundPool = null;
        super.onPause();
    }

    /**
     * Running the game when activity is resumed
     */
    @Override
    protected void onResume(){
        super.onResume();
        hideSystemUI();
    }

    /**
     * Setting the game up for the device screen size
     */
    public void setDisplayBounds() {

        mXmax = mSharedPref.getFloat("xMax", 0f);
        mYmax = mSharedPref.getFloat("yMax", 0f);
        mYmin = 0;
        mXmin = 0;
    }

    /**
     * Hides the system bars
     */
    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        mGameView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }
}
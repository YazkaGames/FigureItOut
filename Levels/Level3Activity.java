package com.yazka.figureitout_alpha.Levels;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener2;
import android.hardware.SensorManager;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import com.yazka.figureitout_alpha.Objects.Ball;
import com.yazka.figureitout_alpha.Objects.LevelMenu;
import com.yazka.figureitout_alpha.Objects.Vector;
import com.yazka.figureitout_alpha.R;
import com.yazka.figureitout_alpha.Tools.AudioPlay;
import java.util.ArrayList;

/**
 * The level consists of 4 balls which are affected by tilting the device. There are 4 circular
 * zones with corresponding colors to the color of the balls.
 * The goal of the level is to get a ball of a certain color inside the zone with the same
 * color. The ball will increase in size when inside the zone. If the ball covers the hole
 * zone tha ball will "swallow" the zone. When all 4 zones are gone the level is beaten.
 * Copyright 2017 Roger Kalliomäki
 *
 * @author Roger Kalliomäki
 */
public class Level3Activity extends AppCompatActivity implements SensorEventListener2 {

//************************************************************************************************//
    /**
     * Inner class providing the view
     */
    private class GameView extends View {

        /**
         * Constructor for the view
         *
         * @param context - Context with global information about the application environment
         */
        public GameView(Context context) {
            super(context);
        }

        /**
         * Calls the update function to update game conditions and drawCanvas which draws a new
         * frame to the canvas.
         *
         * @param canvas - The canvas to draw to
         */
        @Override
        protected void onDraw(Canvas canvas) {
            update();
            drawCanvas(canvas);
            invalidate();
        }
    }
//************************************************************************************************//

//----------- Instance variables ---------------
    private SensorManager mSensorManager;
    private float mXmin,mXmax,mYmin,mYmax,mTouchX,mTouchY,mReleaseX,mReleaseY,mSpeedLimit,
            mXRotation,mYRotation,mTiltScale,mZoneSpeed,mBallRadii;
    private int mNrOfObstacleBalls;
    private long mTime,mTimeBarrier1,mGrowCounter;
    private ArrayList<Ball> mBalls,mObstacleBalls,mZones;
    private SharedPreferences mSharedPref;
    private GameView mGameView;
    private boolean mEndBarrier,mDrawBlack,mTouchFlag,mStopMusic,mPlayWin,mSoundOn;
    private float[] mRotationMatrix,mOrientationValues;
    private LevelMenu mLevelMenu;
    private SoundPool mSoundPool;
    private int mSoundIDs[] = new int[6];
//-----------------------------------------------

    /**
     * Sets everything up when the level (activity) is created
     *
     * @param savedInstanceState -  Bundle object containing the activity's previously saved state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        setDisplayBounds();
        setMenu();
        setGameInitialCondition();
        setContentView(mGameView);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        hideSystemUI();

    }

    /**
     * Sets up the initial conditions for the level
     */
    public void setGameInitialCondition() {
        //the view
        mGameView = new GameView(this);
        //sound pool for game sounds
        addSounds();
        mSoundOn = (mSharedPref.getBoolean("SOUND_ON", true));
        //variables from intent to change level difficulty
        Intent intent = getIntent();
        mNrOfObstacleBalls = intent.getIntExtra("NR_OF_OBSTACLES", 0);
        mTiltScale = mXmax/500;
        mZoneSpeed = 0;
        if(mNrOfObstacleBalls > 1)
            mZoneSpeed = (mNrOfObstacleBalls-1) * mXmax/1100;
        //variables setting up the level
        mSpeedLimit = mXmax*0.012f;
        mBallRadii = (mXmax*0.035f);
        mXRotation = 0;
        mYRotation = 0;
        mReleaseX = -100f;
        mReleaseY = -100f;
        mTime = 0;
        mTimeBarrier1 = 10;
        mGrowCounter = 0;
        mEndBarrier = true;
        mStopMusic = true;
        mPlayWin = true;
        //sensor manager and arrays for the tilt sensor
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mRotationMatrix = new float[9];
        mOrientationValues = new float[3];
        //add game objects
        addBalls();
        addZones();
    }

    /**
     * Add sounds to sound pool
     */
    private void addSounds() {
        AudioAttributes attrs = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();
        mSoundPool = new SoundPool.Builder()
                .setMaxStreams(10)
                .setAudioAttributes(attrs)
                .build();
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        mSoundIDs[0] = mSoundPool.load(this, R.raw.pingpong, 2);
        mSoundIDs[1] = mSoundPool.load(this, R.raw.bord_bounce, 3);
        mSoundIDs[2] = mSoundPool.load(this, R.raw.pen_click2, 1);
        mSoundIDs[3] = mSoundPool.load(this, R.raw.bubble_pop2, 4);
        mSoundIDs[4] = mSoundPool.load(this, R.raw.bubble_pop, 5);
        mSoundIDs[5] = mSoundPool.load(this, R.raw.win1, 6);
    }

    /**
     * Updates game conditions
     */
    private void update() {

        if(!mDrawBlack) {

            //check if balls are inside zones and if so increase size of ball
            if (System.currentTimeMillis() - mTime > mTimeBarrier1) {
                float growRate = mXmax / 1500f;
                for (int i = 0; i < mBalls.size(); i++) {
                    for (int j = 0; j < mZones.size(); j++) {
                        if (mBalls.get(i).getColor() == mZones.get(j).getColor() &&
                                mBalls.get(i).isCollision(mZones.get(j)) &&
                                mBalls.get(i).getRadii() < mXmax / 5) {
                            if (mGrowCounter > 8) {
                                if(mSoundOn)
                                    playSound(2);
                                mGrowCounter = 0;
                            }
                            mGrowCounter++;
                            mBalls.get(i).setRadii(mBalls.get(i).getRadii() + growRate);
                            break;
                        }
                    }
                }
                mTime = System.currentTimeMillis();
            }

            //check for collisions between obstacles balls
            for (int i = 0; i < mObstacleBalls.size(); i++) {
                for (int j = i + 1; j < mObstacleBalls.size(); j++) {
                    if (mObstacleBalls.get(i).isCollision(mObstacleBalls.get(j))) {
                        //if(mSoundOn)
                            //playSound(0);
                        mObstacleBalls.get(i).collide(mObstacleBalls.get(j));
                    }
                }
                mObstacleBalls.get(i).move(0.5f * mSpeedLimit);
                mObstacleBalls.get(i).checkForBorderCollision(mXmax, mYmax, mXmin, mYmin);
            }
            //check for collisions between balls
            for (int i = 0; i < mBalls.size(); i++) {
                for (int j = i + 1; j < mBalls.size(); j++) {
                    if (mBalls.get(i).isCollision(mBalls.get(j))) {
                        //if(mSoundOn)
                            //playSound(0);
                        mBalls.get(i).collide(mBalls.get(j));
                    }
                }
                //check for collisions between balls and obstacles
                for (int j = 0; j < mObstacleBalls.size(); j++) {
                    if (mBalls.get(i).isCollision(mObstacleBalls.get(j))) {
                        //if(mSoundOn)
                            //playSound(0);
                        mBalls.get(i).collide(mObstacleBalls.get(j));
                    }
                }
                //adjust speed (speed limit and tilt of device) and check for border collisions
                mBalls.get(i).move(mSpeedLimit);
                mBalls.get(i).checkForBorderCollision(mXmax, mYmax, mXmin, mYmin);
                mBalls.get(i).tilt(mXRotation, mYRotation, mTiltScale);
            }

            //update zones
            for (int i = 0; i < mZones.size(); i++) {
                mZones.get(i).move(mSpeedLimit);
                mZones.get(i).checkForBorderCollision(mXmax, mYmax, mXmin, mYmin);
            }

            //check if ball covers hole zone, if so remove zone
            for (int i = 0; i < mBalls.size(); i++) {
                for (int j = 0; j < mZones.size(); j++) {
                    if (mBalls.get(i).getColor() == mZones.get(j).getColor() &&
                            mBalls.get(i).getDistance(mZones.get(j)) + mZones.get(j).getRadii()
                                    < mBalls.get(i).getRadii()) {
                        if(mSoundOn)
                            playSound(3);
                        mZones.remove(j);
                        mBalls.get(i).setColor(Color.RED);
                        mObstacleBalls.add(mBalls.get(i));
                        mBalls.remove(i);
                        break;
                    }
                    //if ball goes outside zone reset original ball radii
                    float tempR = mBalls.get(i).getRadii();
                    if (mBalls.get(i).getColor() == mZones.get(j).getColor() &&
                            !mBalls.get(i).isCollision(mZones.get(j))) {
                        mBalls.get(i).setRadii(mBallRadii);
                        if(mSoundOn && tempR>mBalls.get(i).getRadii())
                            playSound(4);
                        break;
                    }
                }
            }
        }

        //update menu
        updateMenu();

        //play winning sound
        if(mSoundOn && mZones.size() < 1 && mPlayWin) {
            playSound(5);
            mPlayWin = false;
        }

        //when all zones are gone the level is beaten
        if (mZones.size() < 1)
            endShrink();

        mReleaseX = -100f;
        mReleaseY = -100f;
    }

    /**
     * Draws everything on the canvas
     *
     * @param canvas - The canvas
     */
    public void drawCanvas(Canvas canvas) {

        //drawing a background color for canvas
        canvas.drawColor(Color.BLACK);

        if(!mDrawBlack) {
            //draw the zones
            for (int i = 0; i < mZones.size(); i++)
                mZones.get(i).drawBall(canvas);

            //draw the balls
            for (int i = 0; i < mBalls.size(); i++)
                mBalls.get(i).drawBall(canvas);

            //draw obstacle balls
            for (int i = 0; i < mObstacleBalls.size(); i++)
                mObstacleBalls.get(i).drawBall(canvas);
        }
        //draw menu
        mLevelMenu.draw(canvas);
    }

    /**
     * Add zones (transparent stationary ball objects)
     */
    private void addZones() {

        mZones = new ArrayList<>();

        float zoneRadii = 4f*mBallRadii;
        int alpha = 150;
        //add zones
        mZones.add(new Ball(zoneRadii,new Vector(mXmax/2-3*zoneRadii/2,
                mYmin+(mYmax-mYmin)/2-3*zoneRadii/2), ContextCompat.getColor(this, R.color.pink)));
        mZones.add(new Ball(zoneRadii,new Vector(mXmax/2+3*zoneRadii/2,
                mYmin+(mYmax-mYmin)/2-3*zoneRadii/2),ContextCompat.getColor(this, R.color.yellow)));
        mZones.add(new Ball(zoneRadii,new Vector(mXmax/2-3*zoneRadii/2,
                mYmin+(mYmax-mYmin)/2+3*zoneRadii/2),ContextCompat.getColor(this, R.color.green)));
        mZones.add(new Ball(zoneRadii,new Vector(mXmax/2+3*zoneRadii/2,
                mYmin+(mYmax-mYmin)/2+3*zoneRadii/2),ContextCompat.getColor(this, R.color.dark_blue)));
        //set alpha
        mZones.get(0).setAlpha(alpha);
        mZones.get(1).setAlpha(alpha);
        mZones.get(2).setAlpha(alpha);
        mZones.get(3).setAlpha(alpha);
        //set velocities
        mZones.get(0).setVelocity(-mZoneSpeed,-mZoneSpeed);
        mZones.get(1).setVelocity(mZoneSpeed,-mZoneSpeed);
        mZones.get(2).setVelocity(-mZoneSpeed,mZoneSpeed);
        mZones.get(3).setVelocity(mZoneSpeed,mZoneSpeed);
    }

    /**
     * Add balls
     */
    private void addBalls() {

        mBalls = new ArrayList<>();
        mObstacleBalls = new ArrayList<>();

        //add regular balls
        mBalls.add(new Ball(mBallRadii,new Vector(mXmax/5,mYmax-mBallRadii),
                ContextCompat.getColor(this, R.color.pink)));
        mBalls.add(new Ball(mBallRadii,new Vector(2*mXmax/5,mYmax-mBallRadii),
                ContextCompat.getColor(this, R.color.yellow)));
        mBalls.add(new Ball(mBallRadii,new Vector(3*mXmax/5,mYmax-mBallRadii),
                ContextCompat.getColor(this, R.color.green)));
        mBalls.add(new Ball(mBallRadii,new Vector(4*mXmax/5,mYmax-mBallRadii),
                ContextCompat.getColor(this, R.color.dark_blue)));
        float radii = 2f*mBallRadii + mNrOfObstacleBalls*(mXmax/120);
        //add obstacle balls
        for(int i = 0; i < mNrOfObstacleBalls; i++) {
            mObstacleBalls.add(new Ball(radii,new Vector((float)Math.random()*mSpeedLimit/2f,(float)Math.random()*mSpeedLimit/2f),
                    new Vector((float)Math.random()*mYmax,(float)Math.random()*(mYmax-mYmin)+mYmin),Color.RED));
        }
    }

    /**
     * Function called when level is beaten. Shrinks all balls until they have disappeared. Then
     * updates the scores and exits the activity.
     */
    private void endShrink() {

        boolean finished = true;
        float shrinkRate = mXmax/700;
        //shrink obstacle balls
        for (int i = 0; i < mObstacleBalls.size(); i++) {
            if (mObstacleBalls.get(i).getRadii() > 0) {
                mObstacleBalls.get(i).setRadii(mObstacleBalls.get(i).getRadii() - shrinkRate);
                finished = false;
            }
        }
        //shrink regular balls
        for (int i = 0; i < mBalls.size(); i++) {
            if (mBalls.get(i).getRadii() > 0) {
                mBalls.get(i).setRadii(mBalls.get(i).getRadii() - shrinkRate);
                finished = false;
            }
        }
        //when all balls are gone update scores end exit level
        if (finished && mEndBarrier) {
            int newScore = mSharedPref.getInt("score_3", 0);
            int totalScore = mSharedPref.getInt("score_total", 0);
            if(newScore<6) {
                newScore++;
                totalScore++;
            }
            SharedPreferences.Editor editor = mSharedPref.edit();
            editor.putInt("score_3", newScore);
            editor.putInt("score_total", totalScore);
            editor.apply();
            mDrawBlack = true;
            mEndBarrier = false;
            mStopMusic = false;
            onBackPressed();

        }
    }

    /**
     * Called after flush() is completed. All the events in the batch at the point when the flush
     * was called have been delivered to the applications registered for those sensor events.
     *
     * @param sensor - The {@link android.hardware.Sensor Sensor} on which flush was called
     */
    @Override
    public void onFlushCompleted(Sensor sensor) {

    }

    /**
     * Called when the accuracy of the registered sensor has changed.  Unlike
     * onSensorChanged(), this is only called when this accuracy value changes.
     *
     * @param accuracy - The new accuracy of this sensor
     */
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    /**
     * Called when there is a new sensor event.
     *
     * @param sensorEvent - The {@link android.hardware.SensorEvent SensorEvent}
     */
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_GAME_ROTATION_VECTOR)  {
            SensorManager.getRotationMatrixFromVector(mRotationMatrix,sensorEvent.values);
            SensorManager.remapCoordinateSystem(mRotationMatrix,SensorManager.AXIS_X,
                    SensorManager.AXIS_Y, mRotationMatrix);
            SensorManager.getOrientation(mRotationMatrix, mOrientationValues);

            mXRotation = mOrientationValues[2];
            mYRotation = -mOrientationValues[1];
        }
    }

    /**
     * Event listener for touch events
     *
     * @param event - touch event
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // get pointer index from the event object
        int pointerIndex = event.getActionIndex();

        // get masked (not specific to a pointer) action
        int action = event.getAction();

        switch (action) {

            // regular touch
            case MotionEvent.ACTION_DOWN:
                mTouchX = event.getX(pointerIndex);
                mTouchY = event.getY(pointerIndex);
                mTouchFlag = true;
                break;

            //touch event (finger) moves
            case MotionEvent.ACTION_MOVE:
                mTouchX = event.getX(pointerIndex);
                mTouchY = event.getY(pointerIndex);
                break;

            // touch event ended (finger up)
            case MotionEvent.ACTION_UP:
                mReleaseX = event.getX(pointerIndex);
                mReleaseY = event.getY(pointerIndex);
                mTouchFlag = false;
                break;

            default:
                return false;
        }
        return true;
    }

    /**
     * Set up Level Menu
     */
    private void setMenu() {
        int score = mSharedPref.getInt("score_3", 0);
        mLevelMenu = new LevelMenu(mXmax,mYmax,3,score,"#E6EE9C",this);
        mYmin = mLevelMenu.getBottom();
    }

    /**
     * Update the Level menu at the bottom and check for button events
     */
    private void updateMenu() {

        if(!mDrawBlack && !mTouchFlag) {
            int buttonValue = mLevelMenu.checkButtons(mReleaseX, mReleaseY);
            //if back button is pressed
            if(buttonValue == 1) {
                mDrawBlack = true;
                onBackPressed();
            }
            //if restart button is pressed
            else if(buttonValue == 2){
                Intent intent = getIntent();
                mStopMusic = false;
                mDrawBlack = true;
                finish();
                startActivity(intent);

            }
        }
    }

    /**
     * When the back button is pressed
     */
    @Override
    public void onBackPressed() {
        mStopMusic = false;
        super.onBackPressed();
    }

    /**
     * When restarting the level
     */
    @Override
    protected void onRestart() {
        AudioPlay.playAudioAtVolume(this,R.raw.area1_music,0.1f);
        super.onRestart();
    }

    /**
     * Called when the system is about to start resuming a previous activity. Followed by either
     * onResume() if the activity returns back to the front, or onStop() if it becomes invisible
     * to the user.
     */
    @Override
    protected void onPause(){
        if(mStopMusic) {
            AudioPlay.stopAudio();
        }
        //don't receive any more updates from sensor
        mSensorManager.unregisterListener(this);
        mSoundPool.release();
        mSoundPool = null;
        super.onPause();
    }

    /**
     * Running the game when activity is resumed
     */
    @Override
    protected void onResume(){

        super.onResume();
        hideSystemUI();
        //register listener and set delay for updates to make batch operations more efficient and
        //reduce power consumption
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR),
                SensorManager.SENSOR_DELAY_GAME);
    }

    /**
     * Plays game sound on background thread
     *
     * @param soundIndex - Index of game sound in array mSoundIDs
     */
    private void playSound(final int soundIndex) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                mSoundPool.play(mSoundIDs[soundIndex],1,1,1,0,1.0f);
            }
        });
    }

    /**
     * Setting the game up for the device screen size
     */
    public void setDisplayBounds() {

        mXmax = mSharedPref.getFloat("xMax", 0f);
        mYmax = mSharedPref.getFloat("yMax", 0f);
        mYmin = 0;
        mXmin = 0;
    }

    /**
     * Hides the system bars
     */
    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        mGameView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }
}
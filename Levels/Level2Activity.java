package com.yazka.figureitout_alpha.Levels;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.annotation.ColorInt;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import com.yazka.figureitout_alpha.Objects.Ball;
import com.yazka.figureitout_alpha.Objects.LevelMenu;
import com.yazka.figureitout_alpha.Objects.Region;
import com.yazka.figureitout_alpha.Objects.Vector;
import com.yazka.figureitout_alpha.Objects.WallRect;
import com.yazka.figureitout_alpha.R;
import com.yazka.figureitout_alpha.Tools.AudioPlay;
import java.util.ArrayList;

/**
 * The level consists of 4 regions and 16 balls with 4 different colors. The balls
 * are repelled by nearby touch events.
 * The goal is to collect 4 balls of equal color in the same region until all regions
 * are filled. When 4 balls of the same color are in a region, the region changes color
 * (for a set time period).
 * Copyright 2017 Roger Kalliomäki
 *
 * @author Roger Kalliomäki
 */
public class Level2Activity extends AppCompatActivity {

//************************************************************************************************//
    /**
     * Inner class providing the view
     */
    private class GameView extends View {

        /**
         * Constructor for the view
         *
         * @param context - Context with global information about the application environment
         */
        public GameView(Context context) {
            super(context);
        }

        /**
         /**
         * Calls the update function to update game conditions and drawCanvas which draws a new
         * frame to the canvas.
         *
         * @param canvas - The canvas to draw to
         */
        @Override
        protected void onDraw(Canvas canvas) {
            update();
            drawCanvas(canvas);
            invalidate();
        }
    }
//************************************************************************************************//

//----------- Instance variables ---------------
    private boolean mTouchFlag,mAllSorted,mUseTimer,mDrawBlack,mStopMusic,mPlayWin,mSoundOn,mPlayRing;
    private float mXmin,mXmax,mYmin,mYmax,mTouchX,mTouchY,mReleaseX,mReleaseY,mBallRadii,mSpeedLimit,
            mSlitWidth,mWallWidth,mTimerWidth;
    private int mTimerDelay,mEndCounter;
    private long mEndTime,mBlinkDelay;
    private Ball mTouchCircle;
    private ArrayList<WallRect> mWalls;
    private ArrayList<Region> mRegions,mTimeBars;
    private ArrayList<Ball> mYellowBalls,mCyanBalls,mGreenBalls,mMagentaBalls;
    private SharedPreferences mSharedPref;
    private GameView mGameView;
    private LevelMenu mLevelMenu;
    private SoundPool mSoundPool;
    private int mSoundIDs[] = new int[5];
//-----------------------------------------------

    /**
     * Sets everything up when the level (activity) is created
     *
     * @param savedInstanceState -  Bundle object containing the activity's previously saved state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        setDisplayBounds();
        setMenu();
        setGameInitialCondition();
        setContentView(mGameView);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        hideSystemUI();

    }

    /**
     * Sets up the initial conditions for the level
     */
    public void setGameInitialCondition() {
        //the view
        mGameView = new GameView(this);
        //sound pool for game sounds
        addSounds();
        mSoundOn = (mSharedPref.getBoolean("SOUND_ON", true));
        //variables from intent to change level difficulty
        Intent intent = getIntent();
        float timerScale = intent.getFloatExtra("TIMER_DELAY",20f);
        mUseTimer = (timerScale < 22);
        mTimerDelay = (int)( (1500f/mXmax)*timerScale );
        float speedLimitFactor = intent.getFloatExtra("SPEED_LIMIT_FACTOR", 1f);
        //variables setting up the level
        mAllSorted = false;
        mPlayRing = true;
        mEndTime = 0;
        mBlinkDelay = 600;
        mEndCounter = 0;
        mSpeedLimit = speedLimitFactor*mXmax/130;
        mBallRadii = mXmax*0.03f;
        mSlitWidth = mXmax/6;
        mWallWidth = mXmax*0.014f;
        mTimerWidth = mXmax*0.03f;
        mReleaseX = -100f;
        mReleaseY = -100f;
        mStopMusic = true;
        mPlayWin = true;
        mTouchCircle = new Ball(mXmax/4f-mBallRadii, mXmax/200f, new Vector(0,0));
        //mTouchCircle.setColor(Color.WHITE);
        //add game objects
        addWalls();
        addRegions();
        addBalls();
    }

    /**
     * Add sounds to sound pool
     */
    private void addSounds() {
        AudioAttributes attrs = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();
        mSoundPool = new SoundPool.Builder()
                .setMaxStreams(10)
                .setAudioAttributes(attrs)
                .build();
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        mSoundIDs[0] = mSoundPool.load(this, R.raw.laser_1, 1);
        mSoundIDs[1] = mSoundPool.load(this, R.raw.collision, 2);
        mSoundIDs[2] = mSoundPool.load(this, R.raw.win1, 3);
        mSoundIDs[3] = mSoundPool.load(this, R.raw.bubble_pop3, 4);
        mSoundIDs[4] = mSoundPool.load(this, R.raw.load_laser, 4);
    }

    /**
     * Updates game conditions
     */
    private void update() {

        //update balls
        mAllSorted = true;
        if(mCyanBalls.size()>0) {
            updateBalls(mCyanBalls);
            mAllSorted = false;
        }
        if(mGreenBalls.size()>0) {
            updateBalls(mGreenBalls);
            mAllSorted = false;
        }
        if(mMagentaBalls.size()>0) {
            updateBalls(mMagentaBalls);
            mAllSorted = false;
        }
        if(mYellowBalls.size()>0) {
            updateBalls(mYellowBalls);
            mAllSorted = false;
        }

        //update timers
        if(!mAllSorted && mUseTimer)
            updateTimers();

        //play sound when touch event (finger on screen) occurs
        if(mSoundOn && mPlayRing && mTouchFlag && mTouchY>mYmin) {
            playSound(0);
            mPlayRing = false;
        }

        //update menu
        updateMenu();

        mReleaseX = -100f;
        mReleaseY = -100f;
    }

    /**
     * Draws everything on the canvas
     *
     * @param canvas - The canvas to draw to
     */
    private void drawCanvas(Canvas canvas) {

        //if game is beaten, play winning sound
        if(mAllSorted && mPlayWin) {
            if(mSoundOn)
                playSound(2);
            mPlayWin = false;
        }

        //if game is not over
        if(!mAllSorted) {
            //draw background color
            canvas.drawColor(Color.BLACK);
            if(!mDrawBlack) {
                //if touch event draw touch ring after first region, else draw regions
                if(mTouchFlag && mTouchY>mYmin) {
                    mTouchCircle.setPosition(mTouchX,mTouchY);
                    drawRingRegions(canvas);
                }
                else {
                    for (int i = 0; i < mRegions.size(); i++)
                        mRegions.get(i).drawRegion(canvas);
                }
                //draw timers
                if (mUseTimer) {
                    for (int i = 0; i < mTimeBars.size(); i++) {
                        if(mTimeBars.get(i).getColor()!=Color.BLACK)
                            mTimeBars.get(i).drawRegion(canvas);
                    }
                }
                //draw walls
                for (int i = 0; i < mWalls.size(); i++)
                    mWalls.get(i).drawWall(canvas);
                //draw balls
                for (int i = 0; i < mYellowBalls.size(); i++)
                    mYellowBalls.get(i).drawBall(canvas);
                for (int i = 0; i < mMagentaBalls.size(); i++)
                    mMagentaBalls.get(i).drawBall(canvas);
                for (int i = 0; i < mGreenBalls.size(); i++)
                    mGreenBalls.get(i).drawBall(canvas);
                for (int i = 0; i < mCyanBalls.size(); i++)
                    mCyanBalls.get(i).drawBall(canvas);
            }
        }

        //if game is over draw the end animation
        else
            endBlink(canvas);

        //when end animation is over exit activity and increase level score
        if(mAllSorted && mEndCounter >30) {
            mEndCounter = 0;
            int newScore = mSharedPref.getInt("score_2", 0);
            int totalScore = mSharedPref.getInt("score_total", 0);
            if(newScore<6) {
                newScore++;
                totalScore++;
            }
            mSharedPref = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = mSharedPref.edit();
            editor.putInt("score_2", newScore);
            editor.putInt("score_total", totalScore);
            editor.apply();
            mStopMusic = false;
            mDrawBlack = true;
            onBackPressed();
        }
        //draw menu
        mLevelMenu.draw(canvas);
    }

    /**
     * Draws a ring around touch event (finger on screen). The draw order of the
     * regions depend on which region the ring is in (so that the ring is only drawn
     * in one region).
     *
     * @param canvas - The canvas to draw to
     */
    private void drawRingRegions(Canvas canvas) {
        //get region of touch event
        int region = getRegion(mTouchX,mTouchY);
        //draw ring only on the region the touch event occurs on
        switch (region) {
            case (0):
                mRegions.get(0).drawRegion(canvas);
                mTouchCircle.drawBall(canvas);
                mRegions.get(1).drawRegion(canvas);
                mRegions.get(2).drawRegion(canvas);
                mRegions.get(3).drawRegion(canvas);
                break;

            case (1):
                mRegions.get(1).drawRegion(canvas);
                mTouchCircle.drawBall(canvas);
                mRegions.get(0).drawRegion(canvas);
                mRegions.get(2).drawRegion(canvas);
                mRegions.get(3).drawRegion(canvas);
                break;

            case (2):
                mRegions.get(2).drawRegion(canvas);
                mTouchCircle.drawBall(canvas);
                mRegions.get(0).drawRegion(canvas);
                mRegions.get(1).drawRegion(canvas);
                mRegions.get(3).drawRegion(canvas);
                break;

            case (3):
                mRegions.get(3).drawRegion(canvas);
                mTouchCircle.drawBall(canvas);
                mRegions.get(0).drawRegion(canvas);
                mRegions.get(1).drawRegion(canvas);
                mRegions.get(2).drawRegion(canvas);
                break;
        }
    }

    /**
     * Event listener for touch events
     *
     * @param event - touch event
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // get pointer index from the event object
        int pointerIndex = event.getActionIndex();

        // get masked (not specific to a pointer) action
        int action = event.getAction();

        switch (action) {

            // regular touch
            case MotionEvent.ACTION_DOWN:
                mTouchX = event.getX(pointerIndex);
                mTouchY = event.getY(pointerIndex);
                mTouchFlag = true;
                break;

            //touch event (finger) moves
            case MotionEvent.ACTION_MOVE:
                mTouchX = event.getX(pointerIndex);
                mTouchY = event.getY(pointerIndex);
                break;

            // touch event ended (finger up)
            case MotionEvent.ACTION_UP:
                mTouchFlag = false;
                mPlayRing = true;
                mReleaseX = event.getX(pointerIndex);
                mReleaseY = event.getY(pointerIndex);
                break;

            default:
                return false;
        }
        return true;
    }

    /**
     * Add balls
     */
    public void addBalls() {

        mYellowBalls = new ArrayList<>();
        mGreenBalls = new ArrayList<>();
        mCyanBalls = new ArrayList<>();
        mMagentaBalls = new ArrayList<>();

        // Add Magenta balls
        newBalls(ContextCompat.getColor(this, R.color.purple));
        // Add Cyan balls
        newBalls(ContextCompat.getColor(this, R.color.blue));
        // Add Green Balls
        newBalls(ContextCompat.getColor(this, R.color.green));
        // Add Yellow balls
        newBalls(ContextCompat.getColor(this, R.color.yellow));
    }

    /**
     * Help function to update an ArrayList with balls
     *
     * @param balls - ArrayList with the balls to be updated
     */
    public void updateBalls(ArrayList<Ball> balls) {

        //go through the ArrayList with the balls and check for touch events and collisions
        for (int i=0; i<balls.size(); i++){
            if(mTouchFlag && getRegion(balls.get(i).getX(),balls.get(i).getY()) == getRegion(mTouchX,mTouchY) &&
                    balls.get(i).getDistance(mTouchX,mTouchY) < mXmax/4)
                balls.get(i).repel(mTouchX,mTouchY,mSpeedLimit);
            else
                balls.get(i).move();
            //check for border collisions
            balls.get(i).checkForBorderCollision(mXmax,mYmax,mXmin,mYmin);
            //check for wall collisions
            for (int j=0; j<mWalls.size(); j++)
                mWalls.get(j).checkForCollision(balls.get(i));
        }

        //check if the balls are in the same region
        boolean inSameRegion = true;
        int region = getRegion(balls.get(0).getX(),balls.get(0).getY());
        for (int i=1; i<balls.size(); i++) {
            region = getRegion(balls.get(i).getX(),balls.get(i).getY());
            inSameRegion = inSameRegion && (region == getRegion(balls.get(i-1).getX(),balls.get(i-1).getY()));
        }

        //if balls are in same region and the region is not full (filled with another color)
        if(inSameRegion && mRegions.get(region).getColor() == Color.BLACK) {
            if(mSoundOn)
                playSound(3);
            mRegions.get(region).setColor(balls.get(0).getColor());
            mTimeBars.get(region).setFlag(true);
            mTimeBars.get(region).setColor(Color.RED);
            balls.clear();
        }
    }

    /**
     * Update the time bars
     */
    public void updateTimers() {

        //update the timers if the last update occurred more then mTimerDelay (ms) ago
        if(System.currentTimeMillis()-mEndTime > mTimerDelay) {
            for (int i = 0; i < mTimeBars.size(); i++) {
                if (mTimeBars.get(i).getFlag())
                    mTimeBars.get(i).decrease();
                if (mTimeBars.get(i).getRect().height() < 1) {
                    if(mSoundOn)
                        playSound(4);
                    mTimeBars.get(i).setColor(Color.BLACK);
                    mTimeBars.get(i).resetTimer((int) ((mYmin+(mYmax-mYmin)/2 - mWallWidth / 2) - (mYmin + mSlitWidth)));
                    mTimeBars.get(i).setFlag(false);
                    newBalls(mRegions.get(i).getColor());
                    mRegions.get(i).setColor(Color.BLACK);
                }
            }
            mEndTime = System.currentTimeMillis();
        }
    }

    /**
     * Adds balls (of a certain color) to their start position (one in each region).
     *
     * @param c - The color of the balls to be added
     */
    public void newBalls(@ColorInt int c){

        //setting the x and y positions of the start positions
        float xPos1, xPos2, xPos3, xPos4, yPos1, yPos2, yPos3, yPos4;
        xPos1 = mXmin+mSlitWidth+mBallRadii;
        xPos2 = mXmax/2-(4*mBallRadii);
        xPos3 = mXmax/2+(4*mBallRadii);
        xPos4 = mXmax-mSlitWidth-mBallRadii;
        yPos1 = mYmin+mSlitWidth+(4*mBallRadii);
        yPos2 = mYmin+(mYmax-mYmin)/2-(4*mBallRadii);
        yPos3 = mYmin+(mYmax-mYmin)/2+(4*mBallRadii);
        yPos4 = mYmax-mSlitWidth-(4*mBallRadii);

        //adding magenta balls
        if(c == ContextCompat.getColor(this, R.color.purple)){
            // Add Magenta balls
            mMagentaBalls.add(new Ball(mBallRadii, new Vector(xPos1, yPos1), c));
            mMagentaBalls.add(new Ball(mBallRadii, new Vector(xPos4, yPos1), c));
            mMagentaBalls.add(new Ball(mBallRadii, new Vector(xPos1, yPos4), c));
            mMagentaBalls.add(new Ball(mBallRadii, new Vector(xPos4, yPos4), c));
        }
        //adding yellow balls
        if(c == ContextCompat.getColor(this, R.color.yellow)){
            // Add Yellow balls
            mYellowBalls.add(new Ball(mBallRadii, new Vector(xPos2, yPos1), c));
            mYellowBalls.add(new Ball(mBallRadii, new Vector(xPos3, yPos1), c));
            mYellowBalls.add(new Ball(mBallRadii, new Vector(xPos2, yPos4), c));
            mYellowBalls.add(new Ball(mBallRadii, new Vector(xPos3, yPos4), c));
        }
        //adding cyan balls
        if(c == ContextCompat.getColor(this, R.color.blue)){
            // Add Cyan balls
            mCyanBalls.add(new Ball(mBallRadii, new Vector(xPos2, yPos2), c));
            mCyanBalls.add(new Ball(mBallRadii, new Vector(xPos3, yPos2), c));
            mCyanBalls.add(new Ball(mBallRadii, new Vector(xPos2, yPos3), c));
            mCyanBalls.add(new Ball(mBallRadii, new Vector(xPos3, yPos3), c));
        }
        //adding green balls
        if(c == ContextCompat.getColor(this, R.color.green)){
            // Add Green Balls
            mGreenBalls.add(new Ball(mBallRadii, new Vector(xPos1, yPos2), c));
            mGreenBalls.add(new Ball(mBallRadii, new Vector(xPos4, yPos2), c));
            mGreenBalls.add(new Ball(mBallRadii, new Vector(xPos1, yPos3), c));
            mGreenBalls.add(new Ball(mBallRadii, new Vector(xPos4, yPos3), c));
        }
    }

    /**
     * Add walls
     */
    public void addWalls() {

        mWalls = new ArrayList<>();

        //set walls
        int vLength = (int)(mYmax-mYmin-mSlitWidth*2);
        int hLength = (int)(mXmax-mSlitWidth*2);
        int VerticalXPosition = (int)(mXmax/2-mWallWidth/2);
        int VerticalYPosition = (int)(mYmin+mSlitWidth);
        int HorizontalXPosition = (int)(mXmin+mSlitWidth);
        int HorizontalYPosition = (int)(mYmin+(mYmax-mYmin)/2-mWallWidth/2);
        Rect r1 = new Rect(VerticalXPosition,VerticalYPosition,
                VerticalXPosition+(int)mWallWidth, VerticalYPosition+vLength);
        Rect r2 = new Rect(HorizontalXPosition,HorizontalYPosition,
               HorizontalXPosition+hLength, HorizontalYPosition+(int)mWallWidth);
        //add walls
        mWalls.add(new WallRect(r1,this));
        mWalls.add(new WallRect(r2,this));
    }

    /**
     * Add regions
     */
    public void addRegions() {

        mRegions = new ArrayList<>();
        mTimeBars = new ArrayList<>();

        // Add regions and timers (timers are regions used as shrinking bars)
        mRegions.add(new Region((int)mXmin,(int)mYmin,(int)(mXmax/2),(int)(mYmin+(mYmax-mYmin)/2)));
        mRegions.add(new Region((int)(mXmax/2),(int)mYmin,(int)mXmax,(int)(mYmin+(mYmax-mYmin)/2)));
        mRegions.add(new Region((int)mXmin,(int)(mYmin+(mYmax-mYmin)/2),(int)(mXmax/2),(int)mYmax));
        mRegions.add(new Region((int)(mXmax/2),(int)(mYmin+(mYmax-mYmin)/2),(int)mXmax,(int)mYmax));
        mTimeBars.add(new Region((int)(mXmax/2-(mWallWidth/2+mTimerWidth)),(int)(mYmin+mSlitWidth),
                (int)(mXmax/2-mWallWidth/2),(int)(mYmin+(mYmax-mYmin)/2-mWallWidth/2)));
        mTimeBars.add(new Region((int)(mXmax/2+mWallWidth/2),(int)(mYmin+mSlitWidth),
                (int)(mXmax/2+(mWallWidth/2+mTimerWidth)),(int)(mYmin+(mYmax-mYmin)/2-mWallWidth/2)));
        mTimeBars.add(new Region((int)(mXmax/2-(mWallWidth/2+mTimerWidth)),(int)(mYmin+(mYmax-mYmin)/2+mWallWidth/2),
                (int)(mXmax/2-mWallWidth/2),(int)(mYmax-mSlitWidth)));
        mTimeBars.add(new Region((int)(mXmax/2+mWallWidth/2),(int)(mYmin+(mYmax-mYmin)/2+mWallWidth/2),
                (int)(mXmax/2+(mWallWidth/2+mTimerWidth)),(int)(mYmax-mSlitWidth)));

    }

    /**
     * Function called when level is beaten. Colors and circles around the zones.
     * Then updates the scores and exits the activity.
     *
     * @param canvas - The canvas to draw to
     */
    public void endBlink(Canvas canvas) {

        //setting values the first iteration
        if(mEndCounter==0) {
            mEndTime = System.currentTimeMillis();
            mEndCounter++;
        }
        //draw background color
        canvas.drawColor(Color.BLACK);
        //draw regions
        for (int i=0; i<mRegions.size(); i++)
            mRegions.get(i).drawRegion(canvas);
        //draw walls
        for (int i=0; i<mWalls.size(); i++)
            mWalls.get(i).drawWall(canvas);
        //changing the color of the regions every mBlinkDelay (ms)
        if(System.currentTimeMillis()-mEndTime > mBlinkDelay) {
            @ColorInt int c = mRegions.get(0).getColor();
            for (int i = 0; i < mRegions.size() - 1; i++)
                mRegions.get(i).setColor(mRegions.get(i + 1).getColor());
            mRegions.get(mRegions.size() - 1).setColor(c);
            mEndTime = System.currentTimeMillis() + mBlinkDelay;
            if(mBlinkDelay>20)
                mBlinkDelay = mBlinkDelay/2;
            mEndCounter++;
        }
    }

    /**
     * Returns the region of the given position
     *
     * @param x - x-position of region
     * @param y - y-position of region
     * @return The region
     */
    public int getRegion(float x, float y) {
        if(x<mXmax/2 && y<mYmin+(mYmax-mYmin)/2)
            return 0;
        if(x>=mXmax/2 && y<=mYmin+(mYmax-mYmin)/2)
            return 1;
        if(x<=mXmax/2 && y>=mYmin+(mYmax-mYmin)/2)
            return 2;
        return 3;
    }

    /**
     * Update the Level menu at the bottom and check for button events
     */
    private void updateMenu() {

        if(!mDrawBlack && !mTouchFlag) {
            int buttonValue = mLevelMenu.checkButtons(mReleaseX, mReleaseY);
            //if back button is pressed
            if(buttonValue == 1) {
                mDrawBlack = true;
                onBackPressed();
            }
            //if restart button is pressed
            else if(buttonValue == 2){
                Intent intent = getIntent();
                mStopMusic = false;
                mDrawBlack = true;
                finish();
                startActivity(intent);
            }
        }
    }

    /**
     * When the back button is pressed
     */
    @Override
    public void onBackPressed() {
        mStopMusic = false;
        super.onBackPressed();
    }

    @Override
    protected void onRestart() {
        AudioPlay.playAudioAtVolume(this,R.raw.area1_music,0.1f);
        super.onRestart();
    }

    /**
     * Called when the system is about to start resuming a previous activity. Followed by either
     * onResume() if the activity returns back to the front, or onStop() if it becomes invisible
     * to the user.
     */
    @Override
    protected void onPause() {
        if(mStopMusic) {
            AudioPlay.stopAudio();
        }
        mSoundPool.release();
        mSoundPool = null;
        super.onPause();
    }

    /**
     * Running the game when activity is resumed
     */
    @Override
    protected void onResume(){

        super.onResume();
        hideSystemUI();
    }

    /**
     * Set up Level Menu
     */
    private void setMenu() {
        int score = mSharedPref.getInt("score_2", 0);
        mLevelMenu = new LevelMenu(mXmax,mYmax,2,score,"#FFAB91",this);
        mYmin = mLevelMenu.getBottom();
    }

    /**
     * Plays game sound on background thread
     *
     * @param soundIndex - Index of game sound in array mSoundIDs
     */
    private void playSound(final int soundIndex) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                mSoundPool.play(mSoundIDs[soundIndex],1,1,1,0,1.0f);
            }
        });
    }

    /**
     * Setting the game up for the device screen size
     */
    public void setDisplayBounds() {

        mXmax = mSharedPref.getFloat("xMax", 0f);
        mYmax = mSharedPref.getFloat("yMax", 0f);
        mYmin = 0;
        mXmin = 0;
    }

    /**
     * Hides the system bars
     */
    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        mGameView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }
}

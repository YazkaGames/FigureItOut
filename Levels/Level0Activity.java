package com.yazka.figureitout_alpha.Levels;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.hardware.SensorManager;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import com.yazka.figureitout_alpha.Objects.Ball;
import com.yazka.figureitout_alpha.Objects.LevelMenu;
import com.yazka.figureitout_alpha.R;
import com.yazka.figureitout_alpha.Tools.AudioPlay;

import java.util.ArrayList;

/**
 * Template activity for a level in the game.
 * Copyright 2017 Roger Kalliomäki
 *
 * @author Roger Kalliomäki
 */
public class Level0Activity extends AppCompatActivity {

//************************************************************************************************//

    /**
     * Inner class providing the view
     */
    private class GameView extends View {

        /**
         * Constructor for the view
         *
         * @param context - Context with global information about the application environment
         */
        public GameView(Context context) {
            super(context);
        }

        /**
         * Calls the update function to update game conditions and drawCanvas which draws a new
         * frame to the canvas.
         *
         * @param canvas - The canvas to draw to
         */
        @Override
        protected void onDraw(Canvas canvas) {
            if (!mDrawBlack)
                update();
            drawCanvas(canvas);
            invalidate();
        }
    }
//************************************************************************************************//

    //----------- Instance variables ---------------
    private SensorManager mSensorManager;
    private boolean mTouchFlag, mLevelCleared, mStopMusic, mPlayWin, mSoundOn;
    private float mXmin, mXmax, mYmin, mYmax, mSpeedLimit, mBallRadii, mTouchX, mTouchY,
            mReleaseX, mReleaseY;
    private boolean mDrawBlack;
    private ArrayList<Ball> mBalls;
    private SharedPreferences mSharedPref;
    private Level0Activity.GameView mGameView;
    private LevelMenu mLevelMenu;
    private SoundPool mSoundPool;
    private int mSoundIDs[] = new int[5];
//-----------------------------------------------

    /**
     * Sets everything up when the level (activity) is created
     *
     * @param savedInstanceState -  Bundle object containing the activity's previously saved state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        setDisplayBounds();
        setMenu();
        setGameInitialCondition();
        setContentView(mGameView);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        hideSystemUI();

    }

    /**
     * Sets up the initial conditions for the level
     */
    public void setGameInitialCondition() {
        //the view
        mGameView = new Level0Activity.GameView(this);
        //sound pool for game sounds
        addSounds();
        mSoundOn = (mSharedPref.getBoolean("SOUND_ON", true));
        //variables to change level difficulty
        float score = mSharedPref.getInt("score_6", 0);
        //variables setting up the level
        mLevelCleared = false;
        mDrawBlack = false;
        mStopMusic = true;
        mPlayWin = true;
        mReleaseX = -100f;
        mReleaseY = -100f;
        mSpeedLimit = mXmax * 0.01f;
        mBallRadii = mXmax * 0.035f;
        //sensor manager for the tilt sensor
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        addBalls();
    }

    /**
     * Add sounds to sound pool
     */
    private void addSounds() {
        AudioAttributes attrs = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();
        mSoundPool = new SoundPool.Builder()
                .setMaxStreams(10)
                .setAudioAttributes(attrs)
                .build();
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        mSoundIDs[0] = mSoundPool.load(this, R.raw.bubble_pop, 1);
        mSoundIDs[1] = mSoundPool.load(this, R.raw.wall, 2);
        mSoundIDs[2] = mSoundPool.load(this, R.raw.pen_click, 3);
        mSoundIDs[3] = mSoundPool.load(this, R.raw.bubble_pop2, 4);
        mSoundIDs[4] = mSoundPool.load(this, R.raw.win1, 5);
    }

    /**
     * Updates game conditions
     */
    private void update() {

        //if(!mDrawBlack)

        if (mLevelCleared) {
            int newScore = mSharedPref.getInt("score_6", 0);
            int totalScore = mSharedPref.getInt("score_total", 0);
            if (newScore < 6) {
                newScore++;
                totalScore++;
            }
            SharedPreferences.Editor editor = mSharedPref.edit();
            editor.putInt("score_6", newScore);
            editor.putInt("score_total", totalScore);
            editor.apply();
            mDrawBlack = true;
            mStopMusic = false;
            onBackPressed();
        }

        //play win sound
        if (mSoundOn && mLevelCleared && mPlayWin) {
            playSound(4);
            mPlayWin = false;
        }

        updateMenu();

        mReleaseX = -100;
        mReleaseY = -100;
    }


    /**
     * Draws everything on the canvas
     *
     * @param canvas - The canvas
     */
    public void drawCanvas(Canvas canvas) {

        //drawing a background color for canvas
        canvas.drawColor(Color.BLACK);

        //if(!mDrawBlack)

        mLevelMenu.draw(canvas);

    }


    /**
     * Add balls
     */
    private void addBalls() {

        mBalls = new ArrayList<>();

        //add balls

    }


    /**
     * Event listener for touch events
     *
     * @param event - touch event
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // get pointer index from the event object
        int pointerIndex = event.getActionIndex();

        // get masked (not specific to a pointer) action
        int action = event.getAction();

        switch (action) {

            // regular touch
            case MotionEvent.ACTION_DOWN:
                mTouchX = event.getX(pointerIndex);
                mTouchY = event.getY(pointerIndex);
                mTouchFlag = true;
                break;

            //touch event (finger) moves
            case MotionEvent.ACTION_MOVE:
                mTouchX = event.getX(pointerIndex);
                mTouchY = event.getY(pointerIndex);
                break;

            // touch event ended (finger up)
            case MotionEvent.ACTION_UP:
                mReleaseX = event.getX(pointerIndex);
                mReleaseY = event.getY(pointerIndex);
                mTouchFlag = false;
                break;

            default:
                return false;
        }
        return true;
    }

    /**
     * Set up Level Menu
     */
    private void setMenu() {
        int score = mSharedPref.getInt("score_5", 0);
        mLevelMenu = new LevelMenu(mXmax, mYmax, 5, score, "#80DEEA", this);
        mYmax = mLevelMenu.getBottom();
    }

    /**
     * Update the Level menu at the bottom and check for button events
     */
    private void updateMenu() {

        if (!mDrawBlack && !mTouchFlag) {
            int buttonValue = mLevelMenu.checkButtons(mReleaseX, mReleaseY);
            //if back button is pressed
            if (buttonValue == 1) {
                mDrawBlack = true;
                onBackPressed();
            }
            //if restart button is pressed
            else if (buttonValue == 2) {
                Intent intent = getIntent();
                mStopMusic = false;
                mDrawBlack = true;
                finish();
                startActivity(intent);
            }
        }
    }

    /**
     * When the back button is pressed
     */
    @Override
    public void onBackPressed() {
        mStopMusic = false;
        super.onBackPressed();
    }

    @Override
    protected void onRestart() {
        AudioPlay.playAudioAtVolume(this, R.raw.area1_music, 0.1f);
        super.onRestart();
    }

    /**
     * Called when the system is about to start resuming a previous activity. Followed by either
     * onResume() if the activity returns back to the front, or onStop() if it becomes invisible
     * to the user.
     */
    @Override
    protected void onPause() {
        if (mStopMusic) {
            AudioPlay.stopAudio();
        }
        mSoundPool.release();
        mSoundPool = null;
        super.onPause();
    }

    /**
     * Running the game when activity is resumed
     */
    @Override
    protected void onResume() {
        super.onResume();
        hideSystemUI();
    }


    /**
     * Plays game sound on background thread
     *
     * @param soundIndex - Index of game sound in array mSoundIDs
     */
    private void playSound(final int soundIndex) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                mSoundPool.play(mSoundIDs[soundIndex], 1, 1, 1, 0, 1.0f);
            }
        });
    }

    /**
     * Setting the game up for the device screen size
     */
    public void setDisplayBounds() {

        mXmax = mSharedPref.getFloat("xMax", 0f);
        mYmax = mSharedPref.getFloat("yMax", 0f);
        mYmin = 0;
        mXmin = 0;
    }

    /**
     * Hides the system bars
     */
    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        mGameView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }
}
